<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();




Route::get('/', 'HomeController@index');
Route::post('/create', 'HomeController@store');

Route::get('/confirmation', 'HomeController@confirmation');
Route::post('contact',['as' => 'contact_store', 'uses' => 'ContactController@store']);
Route::post('/contact_call_aswat',['as' => 'contact_call_aswat', 'uses' => 'CallCenterController@contact_call_aswat']);


/*-- PAGES --*/

Route::get('/qui-sommes-nous', 'PageController@quisommesnous');
Route::get('/notre-mission', 'PageController@mission');
Route::get('/contact', 'PageController@contact');
Route::get('/mentions-legales', 'PageController@ml');
Route::get('/politique-de-confidentialite', 'PageController@pdc');
Route::get('/cgu-cgv', 'PageController@cgu');

Route::get('/pdf2', 'HomeController@pdf');


Route::group(['prefix' => 'admin'], function () {

    Route::get('/demandes/creation','Admin\DemandeController@create');
    Route::post('/demandes','Admin\DemandeController@store');

    Route::get('/faq','FaqController@liste');
    Route::delete('/faq/{faq}/delete','FaqController@delete');
    Route::get('/faq/search','FaqController@search');
    Route::post('/faq/search', 'FaqController@search');
    Route::get('/faq/creation','FaqController@create');
    Route::post('/faq/creation','FaqController@store');
    Route::put('/faq/creation/{id}','FaqController@update');


    Route::get('/ehpad/creation','EhpadController@create');
    Route::post('/ehpad/creation','EhpadController@store');
    Route::get('/ehpad','EhpadController@index');
    Route::get('/ehpad/recherche','EhpadController@search');
    Route::get('/ehpad/{id}','EhpadController@show');


    Route::post('/ehpad/recherche', 'EhpadController@search');
    Route::put('/ehpad/{id}/commentaire', 'EhpadController@updatecomment');
    Route::put('/ehpad/{id}', 'EhpadController@update');
    Route::delete('/ehpad/{commentaire}/delete', 'EhpadController@deletecomment')->middleware('administrator');

    Route::get('/utilisateurs','Admin\UserController@utilisateurslist');
    Route::put('/utilisateurs/{id}','Admin\UserController@editrole');
    Route::get('/utilisateurs/creation','Admin\UserController@usercreate');
    Route::get('/utilisateurs/{id}/modifier','Admin\UserController@edit');
    Route::put('/utilisateurs/{id}/modifier','Admin\UserController@update');
    Route::put('/utilisateurs/{id}/modifier/regions','Admin\UserController@createregions');
    Route::delete('/utilisateurs/{id}/modifier/regions/{region}','Admin\UserController@deleteregions');
    Route::delete('/utilisateurs/{id}','Admin\UserController@delete');


    Route::get('/','Admin\AdminController@index');
    Route::get('/encours','Admin\AdminController@encours');
    Route::put('/{id}', 'Admin\AdminController@priseencharge');
    Route::delete('/{id}','Admin\AdminController@deleteAsker');
    Route::get('fiche/{id}/onglet-{onglet}', ['uses' => 'Admin\AdminController@show','as' => 'ficheview']);
    Route::put('fiche/{id}/onglet-{onglet}/statut', 'Admin\AdminController@updatestatut');
    Route::put('fiche/{id}/onglet-{onglet}/role', 'Admin\AdminController@updateuser')->middleware('administrator');
    Route::put('fiche/{id}/onglet-{onglet}', 'Admin\AdminController@update');

    Route::put('fiche/{id}/commentaire', 'Admin\AdminController@updatecommentaire');

    Route::get('fiche/{id}/pdf', 'Admin\AdminController@pdf');

    Route::get('fiche/{id}/download', 'Admin\AdminController@download');
    Route::post('fiche/{id}/sendemail', 'Admin\AdminController@sendemail');

    Route::get('/demandes/en-cours','Admin\AdminController@encours');
    Route::get('/demandes/archive','Admin\AdminController@archive');
    Route::get('/demandes/a-relancer','Admin\AdminController@arelancer');
    Route::get('/demandes/dossier-envoye-au-client','Admin\AdminController@dossierenvoyeauclient');
    Route::get('/demandes/dossier-transmis-en-ehpad','Admin\AdminController@dossiertransmisenehpad');

    Route::get('/suggestion/','FaqController@suggestion');
    Route::get('/suggestion/{id}','FaqController@show');

    Route::post('fiche/{id}/maisonderetraite/creation','MaisonDeRetraiteController@store');
    Route::delete('fiche/{id}/maisonderetraite/suppression/{mdr}','MaisonDeRetraiteController@delete');
    Route::put('fiche/{id}/maisonderetraite/choix/{mdr}','MaisonDeRetraiteController@choix');

});

Route::get('/email', function () { return view('mails.signature');});


Route::get('/ville','HomeController@ville');
Route::get('admin/demandelistlien','Admin\AdminController@demandelist');


//Route::get('/excel','HomeController@excel');


Route::get('/0-place','HomeController@zeroplace');
Route::get('/1-place','HomeController@oneplace');
Route::get('/2-places','HomeController@twoplaces');