<?php

use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regions')->insert([
            'nom' => 'Auvergne-Rhône-Alpes',
        ]);

        DB::table('regions')->insert([
            'nom' => 'Bourgogne-Franche-Comté',
        ]);

        DB::table('regions')->insert([
            'nom' => 'Bretagne',
        ]);

        DB::table('regions')->insert([
            'nom' => 'Centre-Val de Loire',
        ]);

        DB::table('regions')->insert([
            'nom' => 'Corse',
        ]);

        DB::table('regions')->insert([
            'nom' => 'Grand Est',
        ]);

        DB::table('regions')->insert([
            'nom' => 'Hauts-de-France',
        ]);

        DB::table('regions')->insert([
            'nom' => 'Île-de-France',
        ]);

        DB::table('regions')->insert([
            'nom' => 'Normandie',
        ]);

        DB::table('regions')->insert([
            'nom' => 'Nouvelle-Aquitaine',
        ]);

        DB::table('regions')->insert([
            'nom' => 'Occitanie',
        ]);

        DB::table('regions')->insert([
            'nom' => 'Outre-mer',
        ]);

        DB::table('regions')->insert([
            'nom' => 'Pays de la Loire',
        ]);

        DB::table('regions')->insert([
            'nom' => 'Provence-Alpes-Côte d\'Azur',
        ]);
    }
}
