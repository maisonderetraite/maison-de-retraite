<?php

use Illuminate\Database\Seeder;

class FaqsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faqs')->insert([
            'question' => 'Combien coûte une maison de retraite ?',
            'reponse' => 'Le coût restant à la charge des résidents et/ou de leur famille dépend de chaque établissement. Les tarifs hébergement sont librement fixés en fonction de critères qualitatifs (cadre de vie, taille de la chambre, emplacement géographique, services supplémentaires proposés…). Cependant, le montant mensuel moyen à la charge des résidents est de 2 200 euros par mois.',
            'categorie_id' => '1',
        ]);


        DB::table('faqs')->insert([
            'question' => 'Que comprend le coût d’une maison de retraite ?',
            'reponse' => 'Le coût d’une maison de retraite comprend trois types de prestations : des prestations « hébergement », à savoir la location de la chambre, la restauration, le ménage, la lingerie, les animations, des prestations « dépendance », notamment la fourniture de produits d’incontinence, et des prestations « soins », à savoir toute la partie médicalisation (matériels et consommables médicaux, personnel médical et paramédical).',
            'categorie_id' => '1',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Y a-t-il des aides au financement de ce reste à charge ?',
            'reponse' => 'Il existe une multitude d’aides au financement du coût d’une maison de retraite :<br/><br/>
            - l’allocation personnalisée à l’autonomie (APA), versée par le Département aux personnes âgées de plus de 60 ans en fonction de leur niveau de dépendance, et sans conditions de revenus,<br/>
            - l’allocation personnalisée au logement (APL) ou l’allocation de logement social (ALS), versée par la Caisse d’Allocations Familiales, en fonction des revenus du bénéficiaire,<br/>
            - l’aide sociale, dans les établissements habilités (EHPAD publics), ou au bout de trois ans de séjour dans les établissements non habilités,<br/>
            - les réductions d’impôt, égale à 25% des dépenses réelles d’hébergement et de dépendance, dans la limite de 2 500 € par an pour une personne seule (5 000 € pour un couple),<br/>
            - les mutuelles complémentaires, lorsqu’une assurance dépendance a été contractée par la personne admise en EHPAD, peuvent participer aux frais de séjour<br/>
            - les régimes spéciaux de retraite, notamment dans la fonction publique, peuvent participer aux frais de séjour.<br/><br/>

            ',
            'categorie_id' => '1',
        ]);


        DB::table('faqs')->insert([
            'question' => 'Un résident peut-il recevoir une aide de la Caisse d’Allocation Familiale ?',
            'reponse' => '  Oui, il s’agit de l’allocation personnalisée au logement (APL), si l’établissement est conventionné APL avec la CAF. Sinon, il s’agit de l’allocation de logement social (ALS). Ces deux aides sont conditionnées au niveau des revenus du bénéficiaire.',
            'categorie_id' => '1',
        ]);


        DB::table('faqs')->insert([
            'question' => 'Quels sont les repas servis ? D’où viennent-ils ?',
            'reponse' => 'La plupart des repas servis en maison de retraite médicalisée font l’objet d’une attention particulière, quelques soient les moyens de production mis en œuvre (internalisé, sous-traité ou externalisé). Une diététicienne doit en principe préparer les trames de menus en amont, afin qu’ils soient validés par l’équipe médicale de l’établissement (médecin coordonnateur, cadre de santé, infirmière coordinatrice). Des repas spéciaux peuvent également être proposés en fonction de critères médicaux (textures modifiées) ou cultuels. Enfin leur provenance dépend des moyens de production mis en œuvre au sein de l’établissement. Dans la majorité des cas, les repas sont préparés sur place par des cuisiniers qualifiés.',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Quelles activités sont disponibles pour divertir les résidents (animations, sorties) ?',
            'reponse' => 'Des équipes d’animation sont présentes en EHPAD afin de divertir les résidents. Ces animations peuvent-être diverses : exercices physiques, éveil artistique, jardinage, revues de presse, jeux de société, animations chantantes, spectacles musicaux, sorties…',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Comment se passe une journée type dans une maison de retraite ?',
            'reponse' => 'Une journée type en EHPAD se déroule de la même manière qu’une journée type à domicile. Une attention toute particulière est apportée au fait d’allier les contraintes d’une vie en collectivité avec les besoins et habitudes personnelles des résidents. Le libre choix leur est donc laissé, tout au long de la journée, de faire les activités qui leur conviennent, et au moment qu’ils le souhaitent. Les contraintes pourront être présentes pour les heures des repas, ou pour les horaires de prise en charge médicale ou soignante pour les personnes les plus dépendantes.',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Quels sont les diplômes des infirmières ? des médecins sont-ils disponibles 24h/24 ?',
            'reponse' => 'Les infirmier(e)s sont titulaires d’un diplôme d’état d’infirmier(e) sanctionné par un concours d’entrée en école et un examen de fin de parcours.<br/><br/>
            Les médecins ne sont pas présents 24h/24. Cependant, un médecin coordonnateur salarié de l’établissement est présent plusieurs jours par semaine, en fonction de la taille de l’établissement, et les résidents conservent leur médecin traitant qui se déplace à la demande des infirmiers. Enfin, les EHPAD sont désormais tenus de signer une convention avec des organismes tels que « SOS médecins » afin de pallier les ssituations d’urgence.
            ',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Les maisons de retraite ont elles des partenariats avec les hôpitaux pour des soins d’urgences ?',
            'reponse' => 'Oui, les EHPAD sont tenus de contractualiser des partenariats avec des centres hospitaliers, services d’hospitalisation à domicile, ou d’adhérer à des groupements hospitaliers de territoire. Ces partenariats visent à répondre efficacement aux urgences médicales.',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Les locaux et le personnel sont-ils adaptés pour les personnes à mobilité réduite ?',
            'reponse' => 'Oui, comme tout établissement recevant du public, les EHPAD avaient pour l’échéance de fin 2015 pour rendre accessible leurs locaux, ou déposer un Agenda d’Accessibilité Programmée, en vue de programmer les travaux à réaliser. A ce jour, la plupart des établissements ont remplis cette obligation, de telle sorte que la majorité du parc d’EHPAD Français est accessible ou en cours de le devenir.',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Les résidents ont-ils accès à l’extérieur (jardin, parc)  ?',
            'reponse' => 'Si l’établissement dispose d’extérieurs, ceux-ci doivent évidemment être sécurisés et accessibles pour le plaisir des résidents !',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Les maisons de retraite médicalisées sont-elles protégées contre les incendies et autres risques pouvant nuire à la sécurité des résidents ?',
            'reponse' => 'Oui, les maisons de retraite médicalisées sont des établissements classés en 4ème catégorie au regard de la sécurité incendie. Leurs gestionnaires sont donc tenus de faire vérifier toutes les installations annuellement, afin de garantir leur bon fonctionnement et limiter tout risque. Ils peuvent également être dotés de systèmes d’alarme pour assurer leur protection. Enfin, des commissions de sécurité, en présence de représentants de la préfecture, des pompiers, de la gendarmerie et de la mairie, sont organisés tous les 3 ans pour vérifier le respect des règles de sécurité en vigueur.',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Quand et comment peut-on contacter une personne résidente dans une maison de retraite ?',
            'reponse' => 'Ce point est propre à chaque établissement. Cependant, dans la mesure ou une permanence du secrétariat est assurée, les résidents peuvent être contactés par leurs proches à tout moment lors de cette permanence. Par ailleurs, la plupart des établissements équipent les chambres de lignes téléphoniques indépendantes, afin que les résidents puissent être directement contactés dans leur chambre.',
            'categorie_id' => '2',

        ]);

        DB::table('faqs')->insert([
            'question' => 'Quels sont les soins prodigués pour le bien-être et la santé d’un résident ?',
            'reponse' => 'Les soins sont prodigués 24h/24 et 7j/7 par des équipes soignantes et médicales, et dépendent des pathologies dont souffrent les résidents accueillis. Ces soins sont donc personnalisés, et peuvent aller d’une simple aide aux activités de la vie courante, à une prise en charge médicale plus lourde, voir de l’accompagnement de fin de vie.',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Est-ce qu’il existe des maisons de retraite plus agréables géographiquement (bord de mer, montagne, loin de l’autoroute, facile d’accès, etc. ) ?',
            'reponse' => 'La répartition des maisons de retraite est très diverse, et il existe effectivement des emplacements géographiques plus appréciables que d’autres ! Mais dans la grande majorité des cas, les maisons de retraite étant avant tout des lieux de vie propices au repos et à la tranquillité, leurs emplacements sont choisis en conséquences.',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Les maisons de retraite sont-elles équipées d’une climatisation ?',
            'reponse' => 'Depuis 2004, les maisons de retraite sont tenues de disposer d’au moins une pièce rafraichie, afin de faire face le plus efficacement possible aux épisodes de canicule. Plusieurs années après cette obligation règlementaire, nous constatons que de nombreux EHPAD ont équipés leurs salles communes (salon, restaurant, salles d’activité), mais également leurs chambres.',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Quels sont les équipements des chambres ? Y a t-il une douche adaptée personnelle ?',
            'reponse' => 'Les équipements des chambres peuvent-être variés. Généralement, elles sont équipées de mobilier mis à disposition par l’EHPAD (lit médicalisé, chevet, bureau, fauteuil, télévision), d’équipements de sécurité (appels malade), et d’autres équipements divers (prise téléphonique, wifi…) Chaque chambre dispose d’une salle de bain privative, accessible pour les personnes à mobilité réduite, et d’un placard-dressing.',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Les résidents sont-ils dans des chambres simples ? Ou à plusieurs ?',
            'reponse' => 'Les deux types de chambre existent : chambre individuelle ou chambre double.',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Combien de personnes maximums par maison de retraite ?',
            'reponse' => 'Il n’y a pas de maximum fixé par la loi, mais la taille moyenne d’une maison de retraite se situe autour de 80 lits.',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Quels sont les horaires des visites ?',
            'reponse' => 'Les horaires de visite sont propres à chaque établissement, mais généralement ils se font entre 14h et 19h (le matin n’étant pas le moment idéal compte tenu des soins qui sont prodigués)',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Les animaux de compagnies sont-ils admis dans les établissements ?',
            'reponse' => 'cela dépend de chaque établissement.',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Quelle organisation est mise en place la nuit ?',
            'reponse' => 'L’organisation la nuit dépend de chaque établissement. Généralement, la sécurité et la surveillance est assurée par des aides-soignant(e)s. Certains établissements de grande taille disposent également d’infirmier(e)s de nuit.',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Y a-t-il des transports en commun à proximité ?',
            'reponse' => 'Cela dépend de la situation géographique de chaque établissement.',
            'categorie_id' => '2',
        ]);


        DB::table('faqs')->insert([
            'question' => 'Est-il possible de se garer facilement ? ',
            'reponse' => 'Cela dépend de chaque établissement. Cependant, chaque maison de retraite dispose d’un parking dont la taille est réglementaire en fonction de la capacité d’accueil de l’établissement.',
            'categorie_id' => '2',
        ]);


        DB::table('faqs')->insert([
            'question' => 'L’établissement possède-t-il une cafétéria ?',
            'reponse' => 'Généralement non, mais le personnel de service se tient à votre disposition pour proposer aux résidents et à leurs visiteurs une boisson, une collation ou toute autre chose.',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Y a-t-il des éclairages adaptés à la lecture ?',
            'reponse' => 'Les chambres sont généralement équipées de liseuses ou de bandeaux lumineux au-dessus du lit.',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Les couloirs sont-ils équipés d’une main courante pour se déplacer plus facilement ?',
            'reponse' => 'Généralement oui, afin de répondre aux règles d’accessibilité pour les personnes à mobilité réduite.',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Y a-t-il un ascenseur ?',
            'reponse' => 'oui, si l’établissement dispose d’un étage, afin de répondre aux règles d’accessibilité pour les personnes à mobilité réduite.',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Comment fonctionne le système d’appel malade dans les chambres ?',
            'reponse' => 'Il s’agit d’un boitier se trouvant au-dessus du lit, dans la salle de bain et les WC, ou simplement d’un médaillon que le résident porte autour du cour. Il suffit d’appuyer sur le bouton d’appel afin que le signal soit envoyé sur les téléphones ou récepteurs du personnel soignant de l’établissement, pour leur permettre de se rendre auprès de la personne dans les plus brefs délais.',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Les chambres sont-elles équipées de TV et du câble ?',
            'reponse' => 'Cela dépend de chaque établissement, mais généralement les chambres sont équipées de télévisions TNT.',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Combien de douches sont possibles de prendre par jour ?',
            'reponse' => 'Autant que nécessaire si le résident est très dépendant ou qu’il le souhaite s’il est valide.',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Y a-t-il du personnel pour l’accompagnement en fin de vie ?',
            'reponse' => 'Cela dépend de chaque établissement, des politiques de formation de leur personnel soignant, et des conventions signées (avec une équipe mobile de soins palliatifs notamment).',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Peut-on apporter son propre mobilier ?',
            'reponse' => 'Cela dépend de chaque établissement, mais généralement les chambres sont entièrement personnalisables avec le mobilier du résident (à l’exception du lit médicalisé).',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Le personnel de l\'établissement : sont-ils présents ? Nombreux ? Souriants ? Comment sont-ils vêtus ?',
            'reponse' => 'Les personnels d’EHPAD font leur métier avec passion et professionnalisme. Cela passe nécessairement par une attitude irréprochable, la politesse, l’accessibilité aux interrogations des résidents et leurs proches, le savoir-être en général. Ils sont généralement vêtus de pantalons et tuniques professionnelles.',
            'categorie_id' => '2',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Combien de temps d’attente pour accéder à une maison de retraite ?',
            'reponse' => 'La file d’attente en maison de retraite dépend de nombreux paramètres : emplacement géographique, taille, statut juridique (un établissement public étant moins cher qu’un établissement privé, sa liste d’attente sera plus longue).',
            'categorie_id' => '3',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Quels sont les critères d’entrées dans une maison de retraite ?',
            'reponse' => 'Pour entrer en EHPAD il est nécessaire d’avoir plus de 60 ans et de remplir un dossier de demande d’admission comportant notamment un volet médical qui sera renseigné par le médecin traitant de la personne concernée.',
            'categorie_id' => '3',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Les consultations chez le dentiste, ophtalmo, etc. sont-elles prises en charge par la maison de retraite?',
            'reponse' => 'En principe non, tout dépend du système de financement de la maison de retraite. Mais généralement, s’agissant d’intervenants extérieurs, ceux-ci sont pris en charges par la sécurité sociale du résident et sa mutuelle.',
            'categorie_id' => '3',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Quelle est la différence entre une maison de retraite privée commerciale et associative ?',
            'reponse' => 'La différence est essentiellement juridique. Il s’agit de deux statuts différents, mais dont le fonctionnement reste le même. Cependant, les maisons de retraite associatives peuvent être habilitées à l’aide sociale (ce qui est moins le cas des maisons de retraite privées commerciales), et donc accessibles aux personnes à très faibles revenus.',
            'categorie_id' => '3',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Peut-on recevoir au préalable un contrat de séjour ou le règlement intérieur ?',
            'reponse' => 'Le contrat de séjour et le règlement intérieur doivent être signés peu de temps après l’admission effective. Ils peuvent effectivement être remis pour que le résident, sa famille, sa personne de confiance ou son représentant légal en prennent connaissance avant signature.',
            'categorie_id' => '3',
        ]);

        DB::table('faqs')->insert([
            'question' => 'Les établissements possèdent-ils un label ?',
            'reponse' => 'Dans le cadre de la signature de leur convention tripartite avec l’agence régionale de santé et le département, les EHPAD sont tenus de valider une évaluation interne et externe tous les 5 ans, pour permettre la reconduction de leur convention tripartite. Cette évaluation constitue un gage de qualité et de sérieux, assimilable à un label.',
            'categorie_id' => '3',
        ]);

    }
}
