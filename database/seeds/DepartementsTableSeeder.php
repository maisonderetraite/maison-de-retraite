<?php

use Illuminate\Database\Seeder;

class DepartementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departements')->insert([
            'nombre' => '01',
            'nom' => 'Ain',
            'region_id' => '1'
        ]);

        DB::table('departements')->insert([
            'nombre' => '02',
            'nom' => 'Aisne',
            'region_id' => '7'
        ]);

        DB::table('departements')->insert([
            'nombre' => '20',
            'nom' => 'Corse-du-Sud',
            'region_id' => '5'
        ]);


        DB::table('departements')->insert([
            'nombre' => '20',
            'nom' => 'Haute-Corse',
            'region_id' => '5'
        ]);

        DB::table('departements')->insert([
            'nombre' => '03',
            'nom' => 'Allier',
            'region_id' => '1'
        ]);

        DB::table('departements')->insert([
            'nombre' => '04',
            'nom' => 'Alpes-de-Haute-Provence',
            'region_id' => '14'
        ]);

        DB::table('departements')->insert([
            'nombre' => '05',
            'nom' => 'Hautes-Alpes',
            'region_id' => '14'
        ]);

        DB::table('departements')->insert([
            'nombre' => '06',
            'nom' => 'Alpes-Maritimes',
            'region_id' => '14'
        ]);

        DB::table('departements')->insert([
            'nombre' => '07',
            'nom' => 'Ardèche',
            'region_id' => '1'
        ]);

        DB::table('departements')->insert([
            'nombre' => '08',
            'nom' => 'Ardennes',
            'region_id' => '6'
        ]);

        DB::table('departements')->insert([
            'nombre' => '09',
            'nom' => 'Ariège',
            'region_id' => '11'
        ]);

        DB::table('departements')->insert([
            'nombre' => '10',
            'nom' => 'Aube',
            'region_id' => '6'
        ]);

        DB::table('departements')->insert([
            'nombre' => '11',
            'nom' => 'Aude',
            'region_id' => '11'
        ]);

        DB::table('departements')->insert([
            'nombre' => '12',
            'nom' => 'Aveyron',
            'region_id' => '11'
        ]);

        DB::table('departements')->insert([
            'nombre' => '13',
            'nom' => 'Bouches-du-Rhône',
            'region_id' => '14'
        ]);

        DB::table('departements')->insert([
            'nombre' => '14',
            'nom' => 'Calvados',
            'region_id' => '9'
        ]);

        DB::table('departements')->insert([
            'nombre' => '15',
            'nom' => 'Cantal',
            'region_id' => '1'
        ]);

        DB::table('departements')->insert([
            'nombre' => '16',
            'nom' => 'Charente',
            'region_id' => '10'
        ]);

        DB::table('departements')->insert([
            'nombre' => '17',
            'nom' => 'Charente-Maritime',
            'region_id' => '10'
        ]);

        DB::table('departements')->insert([
            'nombre' => '18',
            'nom' => 'Cher',
            'region_id' => '4'
        ]);

        DB::table('departements')->insert([
            'nombre' => '19',
            'nom' => 'Corrèze',
            'region_id' => '10'
        ]);


        DB::table('departements')->insert([
            'nombre' => '21',
            'nom' => 'Côte-d\'Or',
            'region_id' => '2'
        ]);

        DB::table('departements')->insert([
            'nombre' => '22',
            'nom' => 'Côtes-d\'Armor',
            'region_id' => '3'
        ]);

        DB::table('departements')->insert([
            'nombre' => '23',
            'nom' => 'Creuse',
            'region_id' => '10'
        ]);

        DB::table('departements')->insert([
            'nombre' => '24',
            'nom' => 'Dordogne',
            'region_id' => '10'
        ]);

        DB::table('departements')->insert([
            'nombre' => '25',
            'nom' => 'Doubs',
            'region_id' => '2'
        ]);

        DB::table('departements')->insert([
            'nombre' => '26',
            'nom' => 'Drôme',
            'region_id' => '1'
        ]);

        DB::table('departements')->insert([
            'nombre' => '27',
            'nom' => 'Eure',
            'region_id' => '9'
        ]);

        DB::table('departements')->insert([
            'nombre' => '28',
            'nom' => 'Eure-et-Loir',
            'region_id' => '4'
        ]);

        DB::table('departements')->insert([
            'nombre' => '29',
            'nom' => 'Finistère',
            'region_id' => '3'
        ]);

        DB::table('departements')->insert([
            'nombre' => '30',
            'nom' => 'Gard',
            'region_id' => '11'
        ]);

        DB::table('departements')->insert([
            'nombre' => '31',
            'nom' => 'Haute-Garonne',
            'region_id' => '11'
        ]);

        DB::table('departements')->insert([
            'nombre' => '32',
            'nom' => 'Gers',
            'region_id' => '11'
        ]);

        DB::table('departements')->insert([
            'nombre' => '33',
            'nom' => 'Gironde',
            'region_id' => '10'
        ]);

        DB::table('departements')->insert([
            'nombre' => '34',
            'nom' => 'Hérault',
            'region_id' => '11'
        ]);

        DB::table('departements')->insert([
            'nombre' => '35',
            'nom' => 'Ille-et-Vilaine',
            'region_id' => '3'
        ]);

        DB::table('departements')->insert([
            'nombre' => '36',
            'nom' => 'Indre',
            'region_id' => '4'
        ]);

        DB::table('departements')->insert([
            'nombre' => '37',
            'nom' => 'Indre-et-Loire',
            'region_id' => '4'
        ]);

        DB::table('departements')->insert([
            'nombre' => '38',
            'nom' => 'Isère',
            'region_id' => '1'
        ]);

        DB::table('departements')->insert([
            'nombre' => '39',
            'nom' => 'Jura',
            'region_id' => '2'
        ]);

        DB::table('departements')->insert([
            'nombre' => '40',
            'nom' => 'Landes',
            'region_id' => '10'
        ]);

        DB::table('departements')->insert([
            'nombre' => '41',
            'nom' => 'Loir-et-Cher',
            'region_id' => '4'
        ]);

        DB::table('departements')->insert([
            'nombre' => '42',
            'nom' => 'Loire',
            'region_id' => '1'
        ]);

        DB::table('departements')->insert([
            'nombre' => '43',
            'nom' => 'Haute-Loire',
            'region_id' => '1'
        ]);

        DB::table('departements')->insert([
            'nombre' => '44',
            'nom' => 'Loire-Atlantique',
            'region_id' => '13'
        ]);

        DB::table('departements')->insert([
            'nombre' => '45',
            'nom' => 'Loiret',
            'region_id' => '4'
        ]);

        DB::table('departements')->insert([
            'nombre' => '46',
            'nom' => 'Lot',
            'region_id' => '11'
        ]);

        DB::table('departements')->insert([
            'nombre' => '47',
            'nom' => 'Lot-et-Garonne',
            'region_id' => '10'
        ]);

        DB::table('departements')->insert([
            'nombre' => '48',
            'nom' => 'Lozère',
            'region_id' => '11'
        ]);

        DB::table('departements')->insert([
            'nombre' => '49',
            'nom' => 'Maine-et-Loire',
            'region_id' => '13'
        ]);

        DB::table('departements')->insert([
            'nombre' => '50',
            'nom' => 'Manche',
            'region_id' => '9'
        ]);

        DB::table('departements')->insert([
            'nombre' => '51',
            'nom' => 'Marne',
            'region_id' => '6'
        ]);

        DB::table('departements')->insert([
            'nombre' => '52',
            'nom' => 'Haute-Marne',
            'region_id' => '6'
        ]);

        DB::table('departements')->insert([
            'nombre' => '53',
            'nom' => 'Mayenne',
            'region_id' => '13'
        ]);

        DB::table('departements')->insert([
            'nombre' => '54',
            'nom' => 'Meurthe-et-Moselle',
            'region_id' => '6'
        ]);

        DB::table('departements')->insert([
            'nombre' => '55',
            'nom' => 'Meuse',
            'region_id' => '6'
        ]);

        DB::table('departements')->insert([
            'nombre' => '56',
            'nom' => 'Morbihan',
            'region_id' => '3'
        ]);

        DB::table('departements')->insert([
            'nombre' => '57',
            'nom' => 'Moselle',
            'region_id' => '6'
        ]);

        DB::table('departements')->insert([
            'nombre' => '58',
            'nom' => 'Nièvre',
            'region_id' => '2'
        ]);

        DB::table('departements')->insert([
            'nombre' => '59',
            'nom' => 'Nord',
            'region_id' => '7'
        ]);

        DB::table('departements')->insert([
            'nombre' => '60',
            'nom' => 'Oise',
            'region_id' => '7'
        ]);

        DB::table('departements')->insert([
            'nombre' => '61',
            'nom' => 'Orne',
            'region_id' => '9'
        ]);

        DB::table('departements')->insert([
            'nombre' => '62',
            'nom' => 'Pas-de-Calais',
            'region_id' => '7'
        ]);

        DB::table('departements')->insert([
            'nombre' => '63',
            'nom' => 'Puy-de-Dôme',
            'region_id' => '1'
        ]);

        DB::table('departements')->insert([
            'nombre' => '64',
            'nom' => 'Pyrénées-Atlantiques',
            'region_id' => '10'
        ]);

        DB::table('departements')->insert([
            'nombre' => '65',
            'nom' => 'Hautes-Pyrénées',
            'region_id' => '11'
        ]);

        DB::table('departements')->insert([
            'nombre' => '66',
            'nom' => 'Pyrénées-Orientales',
            'region_id' => '11'
        ]);

        DB::table('departements')->insert([
            'nombre' => '67',
            'nom' => 'Bas-Rhin',
            'region_id' => '6'
        ]);

        DB::table('departements')->insert([
            'nombre' => '68',
            'nom' => 'Haut-Rhin',
            'region_id' => '6'
        ]);

        DB::table('departements')->insert([
            'nombre' => '69',
            'nom' => 'Rhône',
            'region_id' => '1'
        ]);

        DB::table('departements')->insert([
            'nombre' => '70',
            'nom' => 'Haute-Saône',
            'region_id' => '2'
        ]);

        DB::table('departements')->insert([
            'nombre' => '71',
            'nom' => 'Saône-et-Loire',
            'region_id' => '2'
        ]);

        DB::table('departements')->insert([
            'nombre' => '72',
            'nom' => 'Sarthe',
            'region_id' => '13'
        ]);

        DB::table('departements')->insert([
            'nombre' => '73',
            'nom' => 'Savoie',
            'region_id' => '1'
        ]);

        DB::table('departements')->insert([
            'nombre' => '74',
            'nom' => 'Haute-Savoie',
            'region_id' => '1'
        ]);

        DB::table('departements')->insert([
            'nombre' => '75',
            'nom' => 'Paris',
            'region_id' => '8'
        ]);

        DB::table('departements')->insert([
            'nombre' => '76',
            'nom' => 'Seine-Maritime',
            'region_id' => '9'
        ]);

        DB::table('departements')->insert([
            'nombre' => '77',
            'nom' => 'Seine-et-Marne',
            'region_id' => '8'
        ]);

        DB::table('departements')->insert([
            'nombre' => '78',
            'nom' => 'Yvelines',
            'region_id' => '8'
        ]);

        DB::table('departements')->insert([
            'nombre' => '79',
            'nom' => 'Deux-Sèvres',
            'region_id' => '10'
        ]);

        DB::table('departements')->insert([
            'nombre' => '80',
            'nom' => 'Somme',
            'region_id' => '7'
        ]);

        DB::table('departements')->insert([
            'nombre' => '81',
            'nom' => 'Tarn',
            'region_id' => '11'
        ]);

        DB::table('departements')->insert([
            'nombre' => '82',
            'nom' => 'Tarn-et-Garonne',
            'region_id' => '11'
        ]);

        DB::table('departements')->insert([
            'nombre' => '83',
            'nom' => 'Var',
            'region_id' => '14'
        ]);

        DB::table('departements')->insert([
            'nombre' => '84',
            'nom' => 'Vaucluse',
            'region_id' => '14'
        ]);

        DB::table('departements')->insert([
            'nombre' => '85',
            'nom' => 'Vendée',
            'region_id' => '13'
        ]);

        DB::table('departements')->insert([
            'nombre' => '86',
            'nom' => 'Vienne',
            'region_id' => '10'
        ]);

        DB::table('departements')->insert([
            'nombre' => '87',
            'nom' => 'Haute-Vienne',
            'region_id' => '10'
        ]);

        DB::table('departements')->insert([
            'nombre' => '88',
            'nom' => 'Vosges',
            'region_id' => '6'
        ]);

        DB::table('departements')->insert([
            'nombre' => '89',
            'nom' => 'Yonne',
            'region_id' => '2'
        ]);

        DB::table('departements')->insert([
            'nombre' => '90',
            'nom' => 'Territoire de Belfort',
            'region_id' => '2'
        ]);

        DB::table('departements')->insert([
            'nombre' => '91',
            'nom' => 'Essonne',
            'region_id' => '8'
        ]);

        DB::table('departements')->insert([
            'nombre' => '92',
            'nom' => 'Hauts-de-Seine',
            'region_id' => '8'
        ]);

        DB::table('departements')->insert([
            'nombre' => '93',
            'nom' => 'Seine-Saint-Denis',
            'region_id' => '8'
        ]);

        DB::table('departements')->insert([
            'nombre' => '94',
            'nom' => 'Val-de-Marne',
            'region_id' => '8'
        ]);

        DB::table('departements')->insert([
            'nombre' => '95',
            'nom' => 'Val-d\'Oise',
            'region_id' => '8'
        ]);

        DB::table('departements')->insert([
            'nombre' => '971',
            'nom' => 'Guadeloupe',
            'region_id' => '12'
        ]);

        DB::table('departements')->insert([
            'nombre' => '972',
            'nom' => 'Martinique',
            'region_id' => '12'
        ]);

        DB::table('departements')->insert([
            'nombre' => '973',
            'nom' => 'Guyane',
            'region_id' => '12'
        ]);

        DB::table('departements')->insert([
            'nombre' => '974',
            'nom' => 'La Réunion',
            'region_id' => '12'
        ]);

        DB::table('departements')->insert([
            'nombre' => '976',
            'nom' => 'Mayotte',
            'region_id' => '12'
        ]);
    }
}
