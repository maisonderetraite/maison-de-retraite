<?php

use Illuminate\Database\Seeder;

class RegionsDepartementCorse extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departements')->insert([
            'nombre' => '20',
            'nom' => 'Ajaccio',
            'region_id' => '5'
        ]);


        DB::table('departements')->insert([
            'nombre' => '20',
            'nom' => 'Afa',
            'region_id' => '5'
        ]);

        DB::table('departements')->insert([
            'nombre' => '20',
            'nom' => 'Atlata',
            'region_id' => '5'
        ]);

        DB::table('departements')->insert([
            'nombre' => '20',
            'nom' => 'Albitreccia',
            'region_id' => '5'
        ]);

        DB::table('departements')->insert([
            'nombre' => '20',
            'nom' => 'Bastia',
            'region_id' => '5'
        ]);
    }
}
