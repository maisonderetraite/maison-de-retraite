<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Monetiz33',
            'email' => 'awatif@monetiz.fr',
            'password' => bcrypt('awatifmonetiz33*'),
            'roles' => 'administrateur',
        ]);

        DB::table('users')->insert([
            'name' => 'Monetiz33',
            'email' => 'aurelie@monetiz.fr',
            'password' => bcrypt('aureliemonetiz33*'),
            'roles' => 'administrateur',
        ]);

        DB::table('users')->insert([
            'name' => 'VOVAN Benoit',
            'email' => 'benoitvovan@gmail.com',
            'password' => bcrypt('hinamori05*'),
            'roles' => 'administrateur',
        ]);
    }
}
