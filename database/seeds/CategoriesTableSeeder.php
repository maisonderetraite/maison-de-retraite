<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Au niveau financier',
        ]);

        DB::table('categories')->insert([
            'name' => 'Équipement et vie en maison de retraite',
        ]);

        DB::table('categories')->insert([
            'name' => 'Questions globales',
        ]);
    }
}
