<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AskerCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demandeurs', function (Blueprint $table) {

            $table->increments('id');
            $table->enum('statut',['a traiter', 'en cours', 'archivé','Dossier transmis en Ehpad'])->nullable();
            $table->string('age')->nullable();
            $table->string('civilite')->nullable();
            $table->string('nom')->nullable();
            $table->string('prenom')->nullable();
            $table->string('lien_personne')->nullable();
            $table->string('telephone')->nullable();
            $table->string('email')->nullable();
            $table->string('periode_souhaite')->nullable();
            $table->string('patologies')->nullable();
            $table->string('name_firstname')->nullable();
            $table->text('commentaire_agent')->nullable();
            $table->text('commentaire_annexe')->nullable();



            /**
             * * Etat civil senior
             */

            $table->string('etat_civil_civilite')->nullable();
            $table->string('etat_civil_nom_de_naissance')->nullable();
            $table->string('etat_civil_prenoms')->nullable();
            $table->string('etat_civil_date_de_naissance')->nullable();
            $table->string('etat_civil_lieu_de_naissance')->nullable();
            $table->string('etat_civil_pays_ou_departement')->nullable();
            $table->string('etat_civil_immatriculation')->nullable();
            $table->string('etat_civil_adresse_rue')->nullable();
            $table->string('etat_civil_adresse_cp')->nullable();
            $table->string('etat_civil_adresse_commune_ville')->nullable();
            $table->string('etat_civil_telephone_fixe')->nullable();
            $table->string('etat_civil_telephone_portable')->nullable();
            $table->string('etat_civil_email')->nullable();
            $table->string('etat_civil_situation_familiale')->nullable();
            $table->string('etat_civil_nb_enfants')->nullable();
            $table->enum('etat_civil_protection_juridique',['Oui', 'Non' , 'En cours'])->nullable();
            $table->enum('etat_civil_protection_juridique_laquelle', ['Tutelle', 'Curatelle' , 'Sauvegarde de justice', 'Mandat de protection future'])->nullable();
            $table->text('etat_civil_protection_contexte')->nullable();

            /**
             * * Etat civil representant légal
             */
            $table->string('etat_civil_representant_civilite')->nullable();
            $table->string('etat_civil_representant_nom_de_naissance')->nullable();
            $table->string('etat_civil_representant_prenoms')->nullable();
            $table->string('etat_civil_representant_date_de_naissance')->nullable();
            $table->string('etat_civil_representant_lieu_de_naissance')->nullable();
            $table->string('etat_civil_representant_pays_ou_departement')->nullable();
            $table->string('etat_civil_representant_adresse_rue')->nullable();
            $table->string('etat_civil_representant_adresse_cp')->nullable();
            $table->string('etat_civil_representant_adresse_commune_ville')->nullable();
            $table->string('etat_civil_representant_telephone_fixe')->nullable();
            $table->string('etat_civil_representant_telephone_portable')->nullable();
            $table->string('etat_civil_representant_email')->nullable();

            /**
             * * Personne de confiance
             */

            $table->enum('personne_confiance_designation',['Oui ', 'Non'])->nullable();
            $table->string('personne_confiance_nom_de_naissance')->nullable();
            $table->string('personne_confiance_prenoms')->nullable();
            $table->string('personne_confiance_adresse_rue')->nullable();
            $table->string('personne_confiance_adresse_cp')->nullable();
            $table->string('personne_confiance_adresse_commune_ville')->nullable();
            $table->string('personne_confiance_telephone_fixe')->nullable();
            $table->string('personne_confiance_telephone_portable')->nullable();
            $table->string('personne_confiance_email')->nullable();
            $table->string('personne_confiance_lien_parente')->nullable();


            /**
             * * Demande
             */

            $table->enum('demande_type_hebergement',['Hébergement permanent', 'Hébergement temporaire'])->nullable();
            $table->enum('demande_accompagnement',['Oui', 'Non'])->nullable();
            $table->string('demande_duree_sejour_temporaire')->nullable();
            $table->enum('demande_situation',['Domicile', 'Chez enfant/proche', 'Logement foyer','EHPAD', 'Hôpital',' SSIAD/SAD','Accueil de séjour'])->nullable();
            $table->string('demande_autres')->nullable();
            $table->text('demande_nom_etablissement_service')->nullable();
            $table->enum('demande_personne_informe',['Oui', 'Non'])->nullable();
            $table->enum('demande_personne_consentante',['Oui', 'Non'])->nullable();

            $table->boolean('demande_consentement_recueilli')->default(false);

            /**
             * * Personne à contacter
             */

            $table->enum('personne_contact_personne_concernee',['Oui', 'Non'])->nullable();
            $table->string('personne_contact_nom_de_naissance')->nullable();
            $table->string('personne_contact_prenoms')->nullable();
            $table->string('personne_contact_adresse_rue')->nullable();
            $table->string('personne_contact_adresse_cp')->nullable();
            $table->string('personne_contact_adresse_commune_ville')->nullable();
            $table->string('personne_contact_telephone_fixe')->nullable();
            $table->string('personne_contact_telephone_portable')->nullable();

            $table->string('personne_contact_email')->nullable();
            $table->string('personne_contact_lien_parente')->nullable();

            $table->string('personne_contact2_nom_de_naissance')->nullable();
            $table->string('personne_contact2_prenoms')->nullable();

            $table->string('personne_contact2_adresse_rue')->nullable();
            $table->string('personne_contact2_adresse_cp')->nullable();
            $table->string('personne_contact2_adresse_commune_ville')->nullable();
            $table->string('personne_contact2_telephone_fixe')->nullable();
            $table->string('personne_contact2_telephone_portable')->nullable();

            $table->string('personne_contact2_email')->nullable();
            $table->string('personne_contact2_lien_parente')->nullable();


            /**
             * * Aspect financier
             */

            $table->enum('aspects_financiers_comment_frais',['Seule', 'Avec l’aide d’un ou plusieurs tiers'])->nullable();
            $table->enum('aspects_financiers_aide_social',['Oui', 'Non', 'Demande en cours envisagée'])->nullable();
            $table->enum('aspects_financiers_allocation_logement',['Oui', 'Non', 'Demande en cours envisagée'])->nullable();
            $table->enum('aspects_financiers_allocation_personnalise',['Oui', 'Non', 'Demande en cours envisagée'])->nullable();
            $table->enum('aspects_financiers_prestation_compensation',['Oui', 'Non'])->nullable();

            $table->text('aspects_financiers_commentaire')->nullable();

            /**
             * * Date entrée
             */

            $table->enum('date_entree_souhaitee',['Immédiat', 'Dans les 6 mois', 'Echéance plus lointaine'])->nullable();
            $table->string('date_entree_souhaitee_hergemenent_temporaire')->nullable();
            $table->string('date_entree_date_demande')->nullable();


            /**
             * * Maison de retraite
             */

            $table->text('maison_de_retraite_nom')->nullable();
            $table->text('maison_de_retraite_adresse')->nullable();
            $table->text('maison_de_retraite_cp')->nullable();
            $table->text('maison_de_retraite_ville')->nullable();
            $table->text('maison_de_retraite_email')->nullable();


            $table->integer('users_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demandeurs');
    }
}
