<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EhpadCommentaires extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ehpad_commentaires', function (Blueprint $table) {
            $table->increments('id');
            $table->string('commentaire')->nullable();
            $table->text('etat')->nullable();
            $table->integer('ehpad_ehpad_id')->unsigned()->index();
            $table->integer('users_id')->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
