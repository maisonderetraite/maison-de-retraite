<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MaisonDeRetraite extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maison_de_retraites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom')->nullable();
            $table->text('adresse')->nullable();
            $table->string('cp')->nullable();
            $table->string('ville')->nullable();
            $table->string('email')->nullable();
            $table->integer('demandeur_id')->unsigned()->index();
            $table->boolean('choix')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maison_de_retraites');
    }
}
