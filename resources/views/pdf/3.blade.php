<div id="page3">
    <table style="width:100%;">
        <tr>
            <td>
                <p style="
                font-size: 18px;
                font-weight: bold;
                line-height: 39px;
                color:#404040;
                margin-bottom:0px;
                ">DOSSIER ADMINISTRATIF</p>
                <p style="
                font-size: 16px;
                font-weight: bold;
                line-height: 39px;
                color:#404040;
                ">ETAT CIVIL DE LA PERSONNE CONCERNEE</p>
            </td>
        </tr>
        <tr>
            <td>
                <table id="page-form-3">
                    <!-- CIVILITE -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="checkbox">
                                        <label for="Civilité">Civilité :</label>
                                        <span>Monsieur</span><div></div>
                                        <span>Madame</span><div></div>
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                    <!-- NOM -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 35%;">
                                        <label>NOM de naissance
                                            <span style="display: block;font-size: 10px;margin-top: -2px;">(suivi, s’il y a lieu par le nom d’usage)</span>
                                        </label>
                                    </td>
                                    <td style="width: 65%;padding: 2px 10px;border: 1px solid #000;">
                                        <p>test</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Prenom -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 35%;">
                                        <label>Prénom(s)</label>
                                    </td>
                                    <td style="width: 65%;padding: 3px 10px;border: 1px solid #000;">
                                        <p>test</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Date de naissance -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 35%;">
                                        <label>Date de naissance</label>
                                    </td>
                                    <td style="width: 65%;padding: 3px 10px;border: 1px solid #000;">
                                        <p>test</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Lieu de naissance -->
                    <tr>
                        <td>
                            <table style="width:50%;display: inline-block;padding-right: 20px;">
                                <tr>
                                    <td style="width: 35%;">
                                        <label>Lieu de naissance</label>
                                    </td>
                                    <td style="width: 65%;padding: 3px 10px;border: 1px solid #000;margin-left: 1%;">
                                        <p>test</p>
                                    </td>
                                </tr>

                            </table>
                            <table style="width:50%;display: inline-block;">
                                <tr>
                                    <td style="width: 50%;">
                                        <label>Pays ou département</label>
                                    </td>
                                    <td style="width: 50%;padding: 3px 10px;border: 1px solid #000;">
                                        <p>test</p>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>
</div>