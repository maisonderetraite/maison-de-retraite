@extends('layouts.app')
@section('content')
<div id="contact">
    <div class="bandeau">
        <div class="container">
            <p>Nous contacter</p>
        </div>
    </div>
    <div id="bloc-contact">
        <div class="container">
            <div class="row">
                <div id="list-bloc" class="col-xs-12 col-md-10 col-md-offset-1">
                    <div class="row">


                        <div class="col-xs-12 col-sm-6">
                            <div class="box">
                                <a href="#" class="big-link" data-reveal-id="Modal-tel">
                                    <p>
                                        <i class="mdr-tel"></i>
                                        <span>Par Téléphone</span>
                                    </p>
                                </a>
                            </div>
                        </div>
                        <!--<div class="col-xs-12 col-sm-6">
                            <div class="box">
                                <a href="#" class="big-link" data-reveal-id="Modal-rappel">
                                    <p>
                                        <i class="mdr-rappel"></i>
                                        <span>Se faire rappeler</span>
                                    </p>
                                </a>
                            </div>
                        </div>-->
                        <!--
                        <div class="col-xs-12 col-sm-6">
                            <div class="box">
                                <p id="chat_button">
                                    <i class="mdr-chat"></i>
                                    <span>Par chat</span>
                                </p>
                            </div>
                        </div>
                        -->
                        <div class="col-xs-12 col-sm-6">
                            <div class="box">
                                <a href="#" class="big-link" data-reveal-id="Modal-mail">
                                    <p>
                                        <i class="mdr-email"></i>
                                        <span>Par mail</span>
                                    </p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
