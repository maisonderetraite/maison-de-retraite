@extends('layouts.app')
@section('content')
<div id="pdc" class="page">
    <div class="container">
        <h1>Politique de confidentialité</h1>
       <p>Le site Internet www.maisonderetraite.net est déclaré auprès de la La Commission Nationale de l'Informatique et des Libertés (CNIL) créée par la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, la CNIL est une autorité administrative indépendante chargée de veiller à la protection des données personnelles.</p>

        <p>www.maisonderetraite.net est déclaré à la Commission Nationale Informatique et Libertés sous le numéro 2048564 v 0.</p>

        <p>Ainsi, l’utilisateur dispose de divers droits concernant ses informations. En effet, il peut à tout moment accéder et faire opposition sur les données le concernant. Ce droit correspond à l’article 26 et 34 à 38 de la loi n°78-17 du 6 janvier 1978. L’article 36 de la loi n° 78-17 du 6 janvier 1978 prévoit également le droit à l’utilisateur de rectifier ses données.</p>

        <p><strong>RESPONSABILITÉ</strong></p>
        <p>Maisonderetraite.net propose un service gratuit permettant d’aider à la recherche de maison de retraite. Tous les établissements proposés font l’objet d’une convention tripartite avec l’Agence Régionale de Santé et le Conseil Départemental de leur département d’installation. Maisonderetraite.net n’est pas responsable des conditions de vie de la personne âgée dans la maison de retraite et notamment de sa sécurité.</p>

        <p><strong>AVERTISSEMENT</strong></p>
        <p>Nous vous informons que toutes les informations diffusées sur notre site www.maisonderetraite.net sont fournies de manière indicative et non contractuelles. Elles peuvent être modifiées à tout moment. Par ailleurs, nous nous efforçons de vous transmettre des informations à jour grâce à la mise à jour effectuée par notre équipe. Pour toute demande concernant vos informations, veuillez transmettre votre requête :</p>

        <p>
            -Par courrier à l’adresse suivante : Monetiz, 2 rue Buhan 33000 Bordeaux <br/>
            -Par email : contact@maisonderetraite.net</p>

    </div>
</div>
@endsection
