@extends('layouts.app')
@section('content')
<div id="ml" class="page">
    <div class="container">
        <h1>Mentions légales</h1>

        <p>Conformément aux dispositions de l'article 6 III-1 de la loi n°2004-575 du 21 juin 2004 pour la confiance dans l'économie numérique, nous vous informons que le présent site www.maisonderetraite.net est la propriété exclusive de la société MONETIZ.</p>

        <p><strong>Société :</strong></p>
        <p><strong>MONETIZ</strong> est une Société À Responsabilité Limitée, au capital de 10.000 €, immatriculée sous le numéro 538 223 025 au RCS de Bordeaux, ayant comme numéro de TVA intracommunautaire FR 06 538223025, et dont le siège social est sis 2 rue Buhan à BORDEAUX (33000).
        </p>
        <br/>
        <p><strong>Coordonnées de la société :</strong></p>
        <p>
            Siège social : 2 rue Buhan à BORDEAUX (33000)<br/>
            Téléphone : 0800 94 77 55 (appel gratuit)<br/>
            Service clientèle : contact@maisonderetraite.net<br/>
            Responsable de la publication : Monsieur Samuel RANSON<br/>
        </p>
        <br/>
        <p><strong>Déclaration CNIL :</strong></p>
        <p><strong>MONETIZ</strong> effectue un traitement automatisé de données personnelles déclaré auprès de la CNIL (récépissé n° 2048564 v 0).</p>
        <br/>

        <p><strong>Hébergeur :</strong></p>
        <p>Le Site Internet est hébergé par la société OVH, Société par Actions Simplifiée, au capital de 10.069.020 €, immatriculée sous le numéro B 424 761 419 au RCS de Lille Métropole,  ayant son siège social au 2 rue Kellermann à ROUBAIX (59100) et ayant comme numéro de téléphone 08.99.70.17.61.</p>

    </div>
</div>
@endsection

