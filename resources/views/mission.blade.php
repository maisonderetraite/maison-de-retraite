@extends('layouts.app')
@section('content')
<div id="mission">
    <div class="bandeau">
        <div class="container">
            <p>Notre mission</p>
        </div>
    </div>
    <div id="background">
    </div>
    <div id="content">
        <div class="container">
            <div class="contenu">
                <p>La mission de notre organisme est de vous accompagner dans la recherche d’une maison de retraite. Nos conseillers vous proposent des résidences répondant au mieux à vos besoins : le secteur géographique, le budget, la date d’entrée, les pathologies prises en charge, le type d’établissement, tout est pris en compte pour vous proposer la maison de retraite qui vous convient.<br/><br/>

                    Nos conseillers sont à votre disposition 24 heures sur 24 et 7 jours sur 7 pour répondre à vos questions et trouver la maison de retraite de votre choix.<br/><br/>

                    Nos conseillers sont disponibles par téléphone, email ou depuis notre chat en ligne.

                </p>

            </div>
        </div>
    </div>
</div>
@endsection
