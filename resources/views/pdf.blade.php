<style>
    @page { margin:20px 30px; }
    body {
        font-family: 'Arial',sans-serif;
        font-size: 12px;
        line-height: 20px;
    }
    p {
        margin: 0;
    }
    .clear {
        clear:both;
    }
    .page-break {
        page-break-after: always;
    }

    #cerfa {
        text-align: center;
    }
    td {
        vertical-align: middle;
        padding: 0;
    }
    .checkbox div  {
        width:10px;height:10px;border: 1px solid #000;display: inline-block;margin-right:15px;margin-left:10px;margin-top: 1px;

    }

    .checkbox div.coche:after  {
        content: '';
        display: block;
        width: 4px;
        height: 7px;

        /* "Center" the checkmark */
        position:relative;
        top:-1px;
        left:3px;

        border: solid #000;
        border-width: 0 2px 2px 0;
        transform: rotate(45deg);
    }

    .checkbox label {
        margin-right: 20px;
    }
    .checkbox span {
       font-size: 12px;
    }

    #page-form-3 {
        width:100%;
    }
    #page-form-3 p {
        margin-bottom: 0;

    }
    label {
        font-size: 13px;
    }
    table {
        margin-top: -1px;
        width:100%;
    }
    .trait {
        padding: 0;
        margin: 0;

    }
    .trait td {
        width: auto;
        display: inline-block;
        border-bottom: 1px solid #000;
        padding: 0 5px;
        margin-left: 0px;
    }


</style>
<div>
    <div id="page1" style="position:relative;margin: 0 auto;">
        <table style="width: 100%;">
            <tr>
                <td style="width:20%">

                </td>
                <td style="width:60%">
                    <div id="logo" style="text-align: center; vertical-align: top;">
                        {{ HTML::image('images/pdf/logo-francais.jpg','alt', array('width' => 130 )) }}
                        <p style="margin-top: 20px;font-weight: 600;">Ministère du travail, de l’emploi et de la santé<br/>
                            Ministère des solidarités et de la cohésion sociale</p>
                    </div>
                </td>
                <td style="width:20%; vertical-align: top;">
                    <div id="cerfa">
                        {{ HTML::image('images/pdf/cerfa.jpeg','alt', array('width' => 100 )) }}
                        <p style="text-align: center;font-size: 13px;">N°14732*01</p>
                    </div>
                </td>
            </tr>
        </table>
        <table style="width: 100%;margin-top: 70px;">
            <tr>
                <td style="
                padding: 30px;
                text-align: center;
                border: 1px solid #000;">
                    <p style="
                        font-size: 20px;
                        font-weight: bold;
                        line-height: 39px;
                    ">
                        DOSSIER DE DEMANDE D’ADMISSION<br/>
                        EN ETABLISSEMENT D’HEBERGEMENT<br/>
                        POUR PERSONNES AGEES DEPENDANTES
                    </p>
                    <p style="font-size: 14px;margin-top:10px;">
                        ARTICLE D.312-155-1 DU CODE DE L’ACTION SOCIALE ET DES FAMILLES
                    </p>
                </td>
            </tr>
        </table>
        <table style="width: 100%;margin-top: 70px;">
            <tr>
                <td style="
                padding: 10px;
                border: 1px solid #000;
                display: block;
                height: 350px;
                ">
                    <p style="border-bottom:2px solid #000;display:inline-block;font-size: 18px;font-weight: bold;">RESERVE A L’ETABLISSEMENT</p>
                    <p>

                    </p>

                </td>
            </tr>
        </table>
    </div>
</div>



<div class="page-break"></div>




<div id="page2">
    <table style="width: 100%;">
        <tr>
            <td>
                <p style="
                text-align: center;
                font-size: 18px;
                font-weight: bold;
                line-height: 39px;
                color:#404040;
                margin-bottom: 50px;
                ">INFORMATIONS IMPORTANTES A LIRE ATTENTIVEMENT</p>
                <p style="font-weight: 700;text-align: left;font-size: 16px;">La personne sollicitant une entrée en EHPAD doit adresser un dossier aux établissements de son choix.</p>
                <p style="font-size: 13px;">CE DOSSIER EST A REMPLIR EN UN SEUL EXEMPLAIRE ET A PHOTOCOPIER EN FONCTION DU NOMBRE D’ETABLISSEMENTS AUPRES DESQUELS LA PERSONNE SOUHAITE ENTRER.</p>
                <p style="font-size: 13px;margin-top: 40px;">CE DOSSIER COMPREND :</p>
                <ul style="font-size: 13px;margin-left: 0;padding-left: 15px;">

                    <li style="margin-bottom: 15px;">UN VOLET ADMINISTRATIF RENSEIGNE PAR LA PERSONNE CONCERNEE OU TOUTE PERSONNE HABILITEE POUR LE FAIRE (TRAVAILLEUR SOCIAL, ETC.)</li>

                    <li>UN VOLET MEDICAL, DATE ET SIGNE DU MEDECIN TRAITANT OU D’UN AUTRE MEDECIN, A METTRE SOUS PLI CONFIDENTIEL, QUI PERMET NOTAMMENT AU MEDECIN COORDONNATEUR EXERCANT DANS L’ETABLISSEMENT D’EMETTRE UN AVIS CIRCONSTANCIE SUR LA CAPACITE DE L’EHPAD A PRENDRE EN CHARGE LA PERSONNE AU VU DU NIVEAU DE MEDICALISATION DE L’ETABLISSEMENT.</li>
                </ul>
                <p style="font-size: 13px;margin-top: 30px;font-weight: 700;">CE DOSSIER NE VAUT QUE POUR UNE INSCRIPTION SUR UNE LISTE D’ATTENTE ET SON DEPOT NE VAUT EN AUCUN CAS ADMISSION.</p>
            </td>
        </tr>
    </table>

    <table style="width: 100%;margin-top: 30px;">
        <tr>
            <td style="padding:15px;border:1px solid #000;">
                <p style="font-size: 13px;font-weight: 700;">CE DOSSIER DOIT ETRE ACCOMPAGNE DE LA PHOTOCOPIE DES PIECES JUSTIFICATIVES SUIVANTES :</p>
                <ul style="font-size: 13px;margin-left: 0;padding-left: 15px;margin:20px 0;">

                    <li style="margin-bottom: 10px;">LE DERNIER AVIS D’IMPOSITION OU DE NON-IMPOSITION,</li>
                    <li>LES JUSTIFICATIFS DES PENSIONS.</li>

                </ul>
                <p style="font-size: 13px;">AU MOMENT DE L’ENTREE EN ETABLISSEMENT, UN CERTAIN NOMBRE DE PIECES JUSTIFICATIVES COMPLEMENTAIRES SERA DEMANDE.</p>
            </td>
        </tr>
    </table>
    <table style="width: 100%;margin-top: 110px;">
        <tr>
            <td>
                <p style="font-size: 11px;"><strong>NB :</strong> POUR TOUT RENSEIGNEMENT COMPLEMENTAIRE (HABILITATION A L’AIDE SOCIALE, TARIFS DES ETABLISSEMENTS, ATTRIBUTION DE L’ALLOCATION PERSONNALISEE A L’AUTONOMIE), IL CONVIENT DE CONTACTER LE CONSEIL GENERAL DE VOTRE DEPARTEMENT. SI L’ETABLISSEMENT SE TROUVE DANS UN AUTRE DEPARTEMENT, IL CONVIENT DE CONTACTER LE CONSEIL GENERAL DE CE DEPARTEMENT.</p>
            </td>
        </tr>
    </table>
</div>
<div class="page-break"></div>
<div id="page3">
    <table style="width:100%;">
        <tr>
            <td>
                <p style="
                font-size: 18px;
                font-weight: bold;
                line-height: 25px;
                color:#404040;
                margin-bottom:0px;
                ">DOSSIER ADMINISTRATIF</p>
                <p style="
                font-size: 16px;
                font-weight: bold;
                line-height: 20px;
                margin-bottom: 0;
                color:#404040;
                ">ETAT CIVIL DE LA PERSONNE CONCERNEE</p>
            </td>
        </tr>
        <tr>
            <td>
                <table >
                    <!-- CIVILITE -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="checkbox">
                                        <label for="Civilité">Civilité :</label>
                                        <span>Monsieur</span><div @if ( $demandeur->etat_civil_civilite == "Monsieur" ) class="coche" @endif></div>
                                        <span>Madame</span><div  @if ( $demandeur->etat_civil_civilite == "Madame" ) class="coche" @endif></div>

                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                    <!-- NOM -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 25%;">
                                        <label>NOM de naissance
                                            <span style="display: block;font-size: 10px;margin-top: -10px;">(suivi, s’il y a lieu par le nom d’usage)</span>
                                        </label>
                                    </td>
                                    <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{ $demandeur->etat_civil_nom_de_naissance }}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Prenom -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 25%;">
                                        <label>Prénom(s)</label>
                                    </td>
                                    <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{ $demandeur->etat_civil_prenoms }}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Date de naissance -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 25%;">
                                        <label>Date de naissance</label>
                                    </td>
                                    <td style="width: 75%;">
                                        <table class="trait">
                                            <tr>
                                                {{ $demandeur->etat_civil_date_de_naissance }}
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Lieu de naissance -->
                    <tr>
                        <td>
                            <table style="width:60%;padding-right: 20px;float: left;">
                                <tr>
                                    <td style="width: 54%;">
                                        <label>Lieu de naissance</label>
                                    </td>
                                    <td style="width: 70%;padding: 1px 10px;border: 1px solid #000;margin-left: 1%;">
                                        <p>{{ $demandeur->etat_civil_lieu_de_naissance }}</p>
                                    </td>
                                </tr>

                            </table>
                            <table style="width:40%;float: left;">
                                <tr>
                                    <td style="width: 50%;">
                                        <label>Pays ou département</label>
                                    </td>
                                    <td style="width: 50%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{ $demandeur->etat_civil_pays_ou_departement }}</p>
                                    </td>
                                </tr>

                            </table>
                            <div class="clear"></div>
                        </td>
                    </tr>
                    <!-- Immatriculation -->

                    <tr>
                        <td>
                            <table style="float: left;">
                                <tr>
                                    <td style="width: 25%;">
                                        <label>N° d’immatriculation</label>
                                    </td>
                                    <td style="width:75%;">
                                        <table class="trait">
                                            <tr>
                                                {{ $demandeur->etat_civil_immatriculation }}
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <div class="clear"></div>
                        </td>
                    </tr>
                    <!-- Adresse  -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="margin-left: 3px;font-size: 11px;font-weight: 600;margin-top: 5px;margin-bottom: 0;">ADRESSE</td>
                                </tr>
                            </table>
                            <table>

                                <tr>
                                    <td style="width: 25%;">
                                        <label>N° Voie, rue, boulevard</label>
                                    </td>
                                    <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{ $demandeur->etat_civil_adresse_rue }}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Code postal / VILLE -->
                    <tr>
                        <td>
                            <table style="width:60%;padding-right: 20px;float: left;">
                                <tr>
                                    <td style="width: 54%;">
                                        <label>Code postal</label>
                                    </td>
                                    <td style="width: 70%;margin-left: 1%;">
                                        <table class="trait">
                                            {{ $demandeur->etat_civil_adresse_cp }}
                                        </table>
                                    </td>
                                </tr>

                            </table>
                            <table style="width:40%;float: left;">
                                <tr>
                                    <td style="width: 50%;">
                                        <label>Commune/Ville</label>
                                    </td>
                                    <td style="width: 50%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{ $demandeur->etat_civil_adresse_commune_ville }}</p>
                                    </td>
                                </tr>

                            </table>
                            <div class="clear"></div>
                        </td>
                    </tr>
                    <!-- TELEPHONE  -->
                    <tr>
                        <td>
                            <table style="width:60%;padding-right: 20px;float: left;">
                                <tr>
                                    <td style="width: 54%;">
                                        <label>Téléphone fixe</label>
                                    </td>
                                    <td style="width: 70%;padding: 1px 10px;border: 1px solid #000;margin-left: 1%;">
                                        <p>{{ $demandeur->etat_civil_telephone_fixe }}</p>
                                    </td>
                                </tr>

                            </table>
                            <table style="width:40%;float: left;">
                                <tr>
                                    <td style="width: 50%;">
                                        <label>Téléphone portable</label>
                                    </td>
                                    <td style="width: 50%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{ $demandeur->etat_civil_telephone_portable }}</p>
                                    </td>
                                </tr>

                            </table>
                            <div class="clear"></div>
                        </td>
                    </tr>
                    <!-- EMAIL -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 25%;">
                                        <label>Adresse email</label>
                                    </td>
                                    <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{ $demandeur->etat_civil_email }}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- SITUATION -->
                    <tr>
                        <td>
                            <div class="clear"></div>
                            <table>
                                <tr>
                                    <td style="margin-left: 3px;font-size: 11px;font-weight: 600;">SITUATION FAMILIALE</td>
                                </tr>
                            </table>
                            <table style="float: left;width: 100%;">
                                <tr style="margin-top: 10px;">
                                    <td class="checkbox">
                                        <span>Célibataire</span><div @if ( $demandeur->etat_civil_situation_familiale == "Célibataire" ) class="coche" @endif></div>
                                        <span>Vit maritalement</span><div @if ( $demandeur->etat_civil_situation_familiale == "Vit maritalement" ) class="coche" @endif></div>
                                        <span>Pacsé(e)</span><div @if ( $demandeur->etat_civil_situation_familiale == "Pacsé(e)" ) class="coche" @endif></div>
                                        <span>Marié(e)</span><div @if ( $demandeur->etat_civil_situation_familiale == "Marié(e)" ) class="coche" @endif></div>
                                        <span>Veuf(ve)</span><div @if ( $demandeur->etat_civil_situation_familiale == "Veuf(ve)" ) class="coche" @endif></div>
                                        <span>Séparé(e)</span><div @if ( $demandeur->etat_civil_situation_familiale == "Séparé(e)" ) class="coche" @endif></div>
                                        <span>Divorcé(e)</span><div @if ( $demandeur->etat_civil_situation_familiale == "Divorcé(e)" ) class="coche" @endif></div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Nombre d'enfant -->
                    <tr>
                        <td>
                            <div class="clear"></div>
                            <table>
                                <tr>
                                    <td style="width: 25%;">
                                        <label>Nombre d'enfant(s)</label>
                                    </td>
                                    <td style="width: 75%;padding: 1px 10px;">
                                        <p style="width:3%;border: 1px solid #000;padding:0 5px;">{{ $demandeur->etat_civil_nb_enfants }}</p>
                                    </td>
                                </tr>
                            </table>
                            <div class="clear"></div>
                        </td>
                    </tr>
                    <!-- PROTECTION -->
                    <tr>
                        <td>
                            <div class="clear"></div>
                            <table>
                                <tr>
                                    <td class="checkbox">
                                        <label for="MESURE DE PROTECTION JURIDIQUE" style="font-size: 11px;font-weight: 600;">MESURE DE PROTECTION JURIDIQUE :</label>
                                        <span>OUI</span><div @if ($demandeur->etat_civil_protection_juridique == "Oui") class="coche" @endif></div>
                                        <span>NON</span><div @if ($demandeur->etat_civil_protection_juridique == "Non") class="coche" @endif></div>
                                        <span>En cours</span><div @if ($demandeur->etat_civil_protection_juridique == "En cours") class="coche" @endif></div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Oui ? lequel -->
                    <tr>
                        <td style="padding: 0;">
                            <table>
                                <tr>
                                    <td class="checkbox">
                                        <label for="Si oui, laquelle :">Si oui, laquelle :</label>
                                        <span>Tutelle</span><div @if ($demandeur->etat_civil_protection_juridique_laquelle == "Tutelle") class="coche" @endif></div>
                                        <span>Curatelle</span><div @if ($demandeur->etat_civil_protection_juridique_laquelle == "Curatelle") class="coche" @endif></div>
                                        <span>Sauvegarde de justice</span><div @if ($demandeur->etat_civil_protection_juridique_laquelle == "Sauvegarde de justice") class="coche" @endif></div>
                                        <span>Mandat de protection future</span><div @if ($demandeur->etat_civil_protection_juridique_laquelle == "Mandat de protection future") class="coche" @endif></div>
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                </table>
                <table style="width: 100%;">
                    <tr>
                        <td style=" padding: 10px;border: 1px solid #000;display: block;height: 100px;">
                            <p style="font-size: 12px;font-weight: bold;margin-bottom: 0;">CONTEXTE DE LA DEMANDE D'ADMISSION (évènement familial récent, décès du conjoint…) :</p>
                            <p style="font-size: 12px;">
                                {{ $demandeur->etat_civil_protection_contexte }}
                            </p>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="width:100%;">
        <tr>
            <td>
                <p style="
                border-top:2px solid #000;
                font-size: 16px;
                font-weight: bold;
                line-height: 20px;
                margin-bottom: 0;
                color:#404040;
                padding-top: 5px;
                margin-top: 5px;
                ">ETAT CIVIL DU REPRESENTANT LEGAL</p>
            </td>
        </tr>
        <tr>
            <td>
                <table id="page-form-3">
                    <!-- CIVILITE -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="checkbox">
                                        <label for="Civilité">Civilité :</label>
                                        <span>Monsieur</span><div @if ($demandeur->etat_civil_representant_civilite == "Monsieur") class="coche" @endif></div>
                                        <span>Madame</span><div @if ($demandeur->etat_civil_representant_civilite == "Madame") class="coche" @endif></div>
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                    <!-- NOM -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 25%;">
                                        <label>NOM de naissance
                                            <span style="display: block;font-size: 10px;margin-top: -10px;">(suivi, s’il y a lieu par le nom d’usage)</span>
                                        </label>
                                    </td>
                                    <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->etat_civil_representant_nom_de_naissance}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Prenom -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 25%;">
                                        <label>Prénom(s)</label>
                                    </td>
                                    <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->etat_civil_representant_prenoms}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Date de naissance -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 25%;">
                                        <label>Date de naissance</label>
                                    </td>
                                    <td style="width: 75%">
                                        <table class="trait">
                                            <tr>
                                                {{$demandeur->etat_civil_representant_date_de_naissance}}
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Lieu de naissance -->
                    <tr>
                        <td>
                            <table style="width:60%;padding-right: 20px;float: left;">
                                <tr>
                                    <td style="width: 54%;">
                                        <label>Lieu de naissance</label>
                                    </td>
                                    <td style="width: 70%;padding: 1px 10px;border: 1px solid #000;margin-left: 1%;">
                                        <p>{{$demandeur->etat_civil_representant_lieu_de_naissance}}</p>
                                    </td>
                                </tr>

                            </table>
                            <table style="width:40%;float: left;">
                                <tr>
                                    <td style="width: 50%;">
                                        <label>Pays ou département</label>
                                    </td>
                                    <td style="width: 50%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->etat_civil_representant_pays_ou_departement}}</p>
                                    </td>
                                </tr>

                            </table>
                            <div class="clear"></div>
                        </td>
                    </tr>
                    <!-- Adresse  -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="margin-left: 3px;font-size: 11px;font-weight: 600;margin-top: 5px;margin-bottom: 0;">ADRESSE</td>
                                </tr>
                            </table>
                            <table>

                                <tr>
                                    <td style="width: 25%;">
                                        <label>N° Voie, rue, boulevard</label>
                                    </td>
                                    <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->etat_civil_representant_adresse_rue}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Code postal / VILLE -->
                    <tr>
                        <td>
                            <table style="width:60%;padding-right: 20px;float: left;">
                                <tr>
                                    <td style="width: 54%;">
                                        <label>Code postal</label>
                                    </td>
                                    <td style="width: 70%;margin-left: 1%;">
                                        <table class="trait">
                                            <tr>
                                                {{$demandeur->etat_civil_representant_adresse_cp}}
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                            <table style="width:40%;float: left;">
                                <tr>
                                    <td style="width: 50%;">
                                        <label>Commune/Ville</label>
                                    </td>
                                    <td style="width: 50%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->etat_civil_representant_adresse_commune_ville}}</p>
                                    </td>
                                </tr>

                            </table>
                            <div class="clear"></div>
                        </td>
                    </tr>

                    <!-- Code postal / VILLE -->
                    <tr>
                        <td>
                            <table style="width:60%;padding-right: 20px;float: left;">
                                <tr>
                                    <td style="width: 54%;">
                                        <label>Téléphone fixe</label>
                                    </td>
                                    <td style="width: 70%;padding: 1px 10px;border: 1px solid #000;margin-left: 1%;">
                                        <p>{{$demandeur->etat_civil_representant_telephone_fixe}}</p>
                                    </td>
                                </tr>

                            </table>
                            <table style="width:40%;float: left;">
                                <tr>
                                    <td style="width: 50%;">
                                        <label>Téléphone portable</label>
                                    </td>
                                    <td style="width: 50%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->etat_civil_representant_telephone_portable}}</p>
                                    </td>
                                </tr>

                            </table>
                            <div class="clear"></div>
                        </td>
                    </tr>

                    <!-- EMAIL -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 25%;">
                                        <label>Adresse email</label>
                                    </td>
                                    <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->etat_civil_representant_email}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!--- Astériques -->
                <table>
                    <tr>
                        <td>
                            <p style="font-size: 11px;">1 En cas de mesure de protection juridique</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>


<div class="page-break"></div>

<div id="page3">
    <table>
        <tr>
            <td>
                <p style="
                font-size: 16px;
                font-weight: bold;
                line-height: 20px;
                margin-bottom: 0;
                color:#404040;
                ">PERSONNE DE CONFIANCE</p>
            </td>
        </tr>
        <tr>
            <td>
                <table id="page-form-3">
                    <!-- CIVILITE -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="checkbox">
                                        <label for="Designation">Une personne de confiance a-t-elle été désignée par la personne concernée :</label>
                                        <span>OUI</span><div @if ($demandeur->personne_confiance_designation == "Oui") class="coche" @endif></div>
                                        <span>NON</span><div @if ($demandeur->personne_confiance_designation == "Non") class="coche" @endif></div>
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                    <!-- NOM -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="margin-left: 3px;font-size: 11px;font-weight: 600;margin-top: 5px;margin-bottom: 0;">SI OUI :</td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                    <td style="width: 25%;">
                                        <label>NOM de naissance
                                            <span style="display: block;font-size: 10px;margin-top: -10px;">(suivi, s’il y a lieu par le nom d’usage)</span>
                                        </label>
                                    </td>
                                    <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->personne_confiance_nom_de_naissance}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Prenom -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 25%;">
                                        <label>Prénom(s)</label>
                                    </td>
                                    <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->personne_confiance_prenoms}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Adresse  -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="margin-left: 3px;font-size: 11px;font-weight: 600;margin-top: 5px;margin-bottom: 0;">ADRESSE</td>
                                </tr>
                            </table>
                            <table>

                                <tr>
                                    <td style="width: 25%;">
                                        <label>N° Voie, rue, boulevard</label>
                                    </td>
                                    <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->personne_confiance_adresse_rue}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Code postal / VILLE -->
                    <tr>
                        <td>
                            <table style="width:60%;padding-right: 20px;float: left;">
                                <tr>
                                    <td style="width: 54%;">
                                        <label>Code postal</label>
                                    </td>
                                    <td style="width: 70%;margin-left: 1%;">
                                        <table class="trait">
                                            <tr>
                                                {{$demandeur->personne_confiance_adresse_cp}}
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                            <table style="width:40%;float: left;">
                                <tr>
                                    <td style="width: 50%;">
                                        <label>Commune/Ville</label>
                                    </td>
                                    <td style="width: 50%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->personne_confiance_adresse_commune_ville}}</p>
                                    </td>
                                </tr>

                            </table>
                            <div class="clear"></div>
                        </td>
                    </tr>

                    <!-- TELEPHONE -->
                    <tr>
                        <td>
                            <table style="width:60%;padding-right: 20px;float: left;">
                                <tr>
                                    <td style="width: 54%;">
                                        <label>Téléphone fixe</label>
                                    </td>
                                    <td style="width: 70%;padding: 1px 10px;border: 1px solid #000;margin-left: 1%;">
                                        <p>{{$demandeur->personne_confiance_telephone_fixe}}</p>
                                    </td>
                                </tr>

                            </table>
                            <table style="width:40%;float: left;">
                                <tr>
                                    <td style="width: 50%;">
                                        <label>Téléphone portable</label>
                                    </td>
                                    <td style="width: 50%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->personne_confiance_telephone_portable}}</p>
                                    </td>
                                </tr>

                            </table>
                            <div class="clear"></div>
                        </td>
                    </tr>

                    <!-- EMAIL -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 25%;">
                                        <label>Adresse email</label>
                                    </td>
                                    <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->personne_confiance_email}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Lien de parente -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 50%;">
                                        <label>Lien de parenté ou de relation avec la personne concernée</label>
                                    </td>
                                    <td style="width: 50%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->personne_confiance_lien_parente}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>
    <!-- DEMANDE -->
    <table>
        <tr>
            <td>
                <p style="
                    border-top:2px solid #000;
                    font-size: 16px;
                    font-weight: bold;
                    line-height: 20px;
                    margin-bottom: 0;
                    color:#404040;
                    padding-top: 5px;
                    margin-top: 5px;
                    ">DEMANDE</p>
                <!-- Type hebergement -->
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td style="width: 50%;">  <label for="Type d'hébergement">Type d’hébergement/accompagnement recherché : </label></td>
                                <td class="checkbox">
                                    <span>Hébergement permanent</span><div @if ($demandeur->demande_type_hebergement == "Hébergement permanent") class="coche" @endif></div>
                                    <span>Hébergement temporaire</span><div @if ($demandeur->demande_type_hebergement == "Hébergement temporaire") class="coche" @endif></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 50%;"></td>
                                <td class="checkbox">
                                    <label for="Accueil couple" style="font-size: 12px;">Accueil couple souhaité</label>
                                    <span>OUI</span><div @if ($demandeur->demande_accompagnement == "Oui") class="coche" @endif></div>
                                    <span>NON</span><div @if ($demandeur->demande_accompagnement == "Non") class="coche" @endif></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 50%;"></td>
                                <td class="checkbox">
                                    <label for="Durée du séjour" style="font-size: 12px;display: inline-block;vertical-align:middle;">Durée du séjour pour l’hébergement temporaire</label>
                                    <p style="  font-size: 12px;display: inline-block;vertical-align:middle;text-decoration: underline;">{{$demandeur->demande_duree_sejour_temporaire}}</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- SITUATION -->
                <tr>
                    <td>
                        <div class="clear"></div>
                        <table>
                            <tr>
                                <td style="margin-left: 3px;font-size: 12px;">Situation de la personne concernée à la date de la demande :</td>
                            </tr>
                        </table>
                        <table>
                            <tr style="margin-top: 10px;">
                                <td class="checkbox">
                                    <span>Domicile</span><div @if ($demandeur->demande_situation == "Domicile") class="coche" @endif></div>
                                    <span>Chez enfant/proche</span><div @if ($demandeur->demande_situation == "Chez enfant/proche") class="coche" @endif></div>
                                    <span>Logement foyer</span><div @if ($demandeur->demande_situation == "Logement foyer") class="coche" @endif></div>
                                    <span>EHPAD</span><div @if ($demandeur->demande_situation == "EHPAD") class="coche" @endif></div>
                                    <span>Hôpital</span><div @if ($demandeur->demande_situation == "Hôpital") class="coche" @endif></div>
                                    <span>SSIAD/SAD</span><div @if ($demandeur->demande_situation == "SSIAD/SAD") class="coche" @endif></div>
                                    <span>Accueil de jour</span><div @if ($demandeur->demande_situation == "Accueil de jour") class="coche" @endif></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- AUTRES -->
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td style="width: 25%;">
                                    <label>Autre (précisez)</label>
                                </td>
                                <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;">
                                    <p>{{$demandeur->demande_autres}}</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- precision établissement -->
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td style="width: 25%;vertical-align: top;">
                                    <label style="font-size: 12px;line-height: 13px;">Dans tous les cas préciser le nom<br/>
                                        de l’établissement ou du service</label>
                                </td>
                                <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;vertical-align: top;">
                                    <p style="height:40px;">{{$demandeur->demande_nom_etablissement_service}}</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- La personne concernée est-elle informée de la demande ?-->
                <tr style="margin-top: -3px;">
                    <td>
                        <table>
                            <tr>
                                <td class="checkbox">
                                    <label for="Demande informée" style="font-size: 12px;">La personne concernée est-elle informée de la demande ?</label>
                                    <span>OUI</span><div @if ($demandeur->demande_personne_informe == "Oui") class="coche" @endif></div>
                                    <span>NON</span><div @if ($demandeur->demande_personne_informe == "Non") class="coche" @endif></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <!-- La personne concernée est-elle consentante (à la demande) ? -->
                <tr>
                    <td>
                        <table style="margin-top: -3px;">
                            <tr>
                                <td class="checkbox">
                                    <label for="Demande informée" style="font-size: 12px;">La personne concernée est-elle consentante (à la demande) ?</label>
                                    <span>OUI</span><div @if ($demandeur->demande_personne_consentante == "Oui") class="coche" @endif></div>
                                    <span>NON</span><div @if ($demandeur->demande_personne_consentante == "Non") class="coche" @endif></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- Dans le cas où la personne concernée ne remplit pas elle-même le document, le consentement éclairé n’a pu être recueilli -->
                <tr>
                    <td>
                        <table style="margin-top: -3px;">
                            <tr>
                                <td class="checkbox">
                                    <label for="Demande informée" style="font-size: 12px;">Dans le cas où la personne concernée ne remplit pas elle-même le document, le consentement éclairé n’a pu être recueilli</label>
                                    <div @if ($demandeur->demande_consentement_recueilli) class="coche" @endif></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </td>
        </tr>
    </table>

    <!-- COORDONNEES DES PERSONNES A CONTACTER AU SUJET DE CETTE DEMANDE -->
    <table>
        <tr>
            <td>
                <p style="
                    border-top:2px solid #000;
                    font-size: 16px;
                    font-weight: bold;
                    line-height: 20px;
                    margin-bottom: 0;
                    color:#404040;
                    padding-top: 5px;
                    margin-top: 5px;
                    ">COORDONNEES DES PERSONNES A CONTACTER AU SUJET DE CETTE DEMANDE</p>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="checkbox">
                                        <label for="personne concernée">La personne concernée elle-même</label>
                                        <span>OUI</span><div @if ($demandeur->personne_contact_personne_concernee == "Oui") class="coche" @endif></div>
                                        <span>NON</span><div @if ($demandeur->personne_contact_personne_concernee == "Non") class="coche" @endif></div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <p style="font-size: 12px;">Si ce n’est pas le cas, autre personne à contacter(1)</p>
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>

                    <!-- NOM -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 25%;">
                                        <label>NOM de naissance
                                            <span style="display: block;font-size: 10px;margin-top: -10px;">(suivi, s’il y a lieu par le nom d’usage)</span>
                                        </label>
                                    </td>
                                    <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->personne_contact_nom_de_naissance}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Prenom -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 25%;">
                                        <label>Prénom(s)</label>
                                    </td>
                                    <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->personne_contact_prenoms}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Adresse  -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="margin-left: 3px;font-size: 11px;font-weight: 600;margin-top: 5px;margin-bottom: 0;">ADRESSE</td>
                                </tr>
                            </table>
                            <table>

                                <tr>
                                    <td style="width: 25%;">
                                        <label>N° Voie, rue, boulevard</label>
                                    </td>
                                    <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->personne_contact_adresse_rue}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Code postal / VILLE -->
                    <tr>
                        <td>
                            <table style="width:60%;padding-right: 20px;float: left;">
                                <tr>
                                    <td style="width: 54%;">
                                        <label>Code postal</label>
                                    </td>
                                    <td style="width: 70%;margin-left: 1%;">
                                        <table class="trait">
                                            <tr>
                                                {{$demandeur->personne_contact_adresse_cp}}
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                            <table style="width:40%;float: left;">
                                <tr>
                                    <td style="width: 50%;">
                                        <label>Commune/Ville</label>
                                    </td>
                                    <td style="width: 50%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->personne_contact_adresse_commune_ville}}</p>
                                    </td>
                                </tr>

                            </table>
                            <div class="clear"></div>
                        </td>
                    </tr>

                    <!-- TELEPHONE -->
                    <tr>
                        <td>
                            <table style="width:60%;padding-right: 20px;float: left;">
                                <tr>
                                    <td style="width: 54%;">
                                        <label>Téléphone fixe</label>
                                    </td>
                                    <td style="width: 70%;padding: 1px 10px;border: 1px solid #000;margin-left: 1%;">
                                        <p>{{$demandeur->personne_contact_telephone_fixe}}</p>
                                    </td>
                                </tr>

                            </table>
                            <table style="width:40%;float: left;">
                                <tr>
                                    <td style="width: 50%;">
                                        <label>Téléphone portable</label>
                                    </td>
                                    <td style="width: 50%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->personne_contact_telephone_portable}}</p>
                                    </td>
                                </tr>

                            </table>
                            <div class="clear"></div>
                        </td>
                    </tr>

                    <!-- EMAIL -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 25%;">
                                        <label>Adresse email</label>
                                    </td>
                                    <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->personne_contact_email}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Lien de parente -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 50%;">
                                        <label>Lien de parenté ou de relation avec la personne concernée</label>
                                    </td>
                                    <td style="width: 50%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->personne_contact_lien_parente}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!--- Astériques -->
                <table>
                    <tr>
                        <td>
                            <p style="font-size: 11px;">1  SSIAD/SAD : service de soins infirmiers à domicile / service d’aide à domicile</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<div class="page-break"></div>
<!-- PAGE 4 -->
<div id="page4">
    <!-- COORDONNEES DES PERSONNES A CONTACTER AU SUJET DE CETTE DEMANDE SUITE -->
    <table>
        <tr>
            <td>
                <p style="
                    border-top:2px solid #000;
                    font-size: 16px;
                    font-weight: bold;
                    line-height: 20px;
                    margin-bottom: 0;
                    color:#404040;
                    padding-top: 5px;
                    margin-top: 5px;
                    ">COORDONNEES DES PERSONNES A CONTACTER AU SUJET DE CETTE DEMANDE</p>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <p style="font-size: 12px;">Autre personne à contacter ²</p>
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>

                    <!-- NOM -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 25%;">
                                        <label>NOM de naissance
                                            <span style="display: block;font-size: 10px;margin-top: -10px;">(suivi, s’il y a lieu par le nom d’usage)</span>
                                        </label>
                                    </td>
                                    <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->personne_contact2_nom_de_naissance}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Prenom -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 25%;">
                                        <label>Prénom(s)</label>
                                    </td>
                                    <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->personne_contact2_prenoms}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Adresse  -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="margin-left: 3px;font-size: 11px;font-weight: 600;margin-top: 5px;margin-bottom: 0;">ADRESSE</td>
                                </tr>
                            </table>
                            <table>

                                <tr>
                                    <td style="width: 25%;">
                                        <label>N° Voie, rue, boulevard</label>
                                    </td>
                                    <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->personne_contact2_adresse_rue}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Code postal / VILLE -->
                    <tr>
                        <td>
                            <table style="width:60%;padding-right: 20px;float: left;">
                                <tr>
                                    <td style="width: 54%;">
                                        <label>Code postal</label>
                                    </td>
                                    <td style="width: 70%;margin-left: 1%;">
                                        <table class="trait">
                                            <tr>
                                                {{$demandeur->personne_contact2_adresse_cp}}

                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                            <table style="width:40%;float: left;">
                                <tr>
                                    <td style="width: 50%;">
                                        <label>Commune/Ville</label>
                                    </td>
                                    <td style="width: 50%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->personne_contact2_adresse_commune_ville}}</p>
                                    </td>
                                </tr>

                            </table>
                            <div class="clear"></div>
                        </td>
                    </tr>

                    <!-- TELEPHONE -->
                    <tr>
                        <td>
                            <table style="width:60%;padding-right: 20px;float: left;">
                                <tr>
                                    <td style="width: 54%;">
                                        <label>Téléphone fixe</label>
                                    </td>
                                    <td style="width: 70%;padding: 1px 10px;border: 1px solid #000;margin-left: 1%;">
                                        <p>{{$demandeur->personne_contact2_telephone_fixe}}</p>
                                    </td>
                                </tr>

                            </table>
                            <table style="width:40%;float: left;">
                                <tr>
                                    <td style="width: 50%;">
                                        <label>Téléphone portable</label>
                                    </td>
                                    <td style="width: 50%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->personne_contact2_telephone_portable}}</p>
                                    </td>
                                </tr>

                            </table>
                            <div class="clear"></div>
                        </td>
                    </tr>

                    <!-- EMAIL -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 25%;">
                                        <label>Adresse email</label>
                                    </td>
                                    <td style="width: 75%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->personne_contact2_email}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Lien de parente -->
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 50%;">
                                        <label>Lien de parenté ou de relation avec la personne concernée</label>
                                    </td>
                                    <td style="width: 50%;padding: 1px 10px;border: 1px solid #000;">
                                        <p>{{$demandeur->personne_contact2_lien_parente}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>
    <!-- DEMANDE -->
    <table>
        <tr>
            <td>
                <p style="
                    border-top:2px solid #000;
                    font-size: 16px;
                    font-weight: bold;
                    line-height: 20px;
                    margin-bottom: 0;
                    color:#404040;
                    padding-top: 5px;
                    margin-top: 5px;
                    ">ASPECTS FINANCIERS</p>
                <!-- Type hebergement -->
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="checkbox">
                                    <label for="Comment la personne concernée pense-t-elle financer ses frais de séjour ?"  style="font-size: 12px;">Comment la personne concernée pense-t-elle financer ses frais de séjour ?</label>
                                    <span>Seule</span><div @if ($demandeur->aspects_financiers_comment_frais == "Seule") class="coche" @endif></div>
                                    <span>Avec l’aide d’un ou plusieurs tiers</span><div @if ($demandeur->aspects_financiers_comment_frais == "Avec l’aide d’un ou plusieurs tiers") class="coche" @endif></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- La personne concernée est-elle informée de la demande ?-->
                <tr style="margin-top: -3px;">
                    <td>
                        <table>
                            <tr>
                                <td class="checkbox">
                                    <label for="Demande informée" style="font-size: 12px;">Aide sociale à l’hébergement</label>
                                    <span>OUI</span><div @if ($demandeur->aspects_financiers_aide_social == "Oui") class="coche" @endif></div>
                                    <span>NON</span><div @if ($demandeur->aspects_financiers_aide_social == "Non") class="coche" @endif></div>
                                    <span>Demande en cours envisagée</span><div @if ($demandeur->aspects_financiers_aide_social == "Demande en cours envisagée") class="coche" @endif></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="margin-top: -3px;">
                    <td>
                        <table>
                            <tr>
                                <td class="checkbox">
                                    <label for="Demande informée" style="font-size: 12px;">Allocation logement (APL/ALS)</label>
                                    <span>OUI</span><div @if ($demandeur->aspects_financiers_aide_social == "Oui") class="coche" @endif></div>
                                    <span>NON</span><div @if ($demandeur->aspects_financiers_aide_social == "Non") class="coche" @endif></div>
                                    <span>Demande en cours envisagée</span><div @if ($demandeur->aspects_financiers_aide_social == "Demande en cours envisagée") class="coche" @endif></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="margin-top: -3px;">
                    <td>
                        <table>
                            <tr>
                                <td class="checkbox">
                                    <label for="Demande informée" style="font-size: 12px;">Allocation personnalisée à l’autonomie*</label>
                                    <span>OUI</span><div  @if ($demandeur->aspects_financiers_allocation_personnalise == "Oui") class="coche" @endif></div>
                                    <span>NON</span><div  @if ($demandeur->aspects_financiers_allocation_personnalise == "Non") class="coche" @endif></div>
                                    <span>Demande en cours envisagée</span><div  @if ($demandeur->aspects_financiers_allocation_personnalise == "Demande en cours envisagée") class="coche" @endif></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="margin-top: -3px;">
                    <td>
                        <table>
                            <tr>
                                <td class="checkbox">
                                    <label for="Demande informée" style="font-size: 12px;">Prestation de compensation du handicap/Allocation compensatrice pour tierce personne</label>
                                    <span>OUI</span><div @if ($demandeur->aspects_financiers_prestation_compensation == "Oui") class="coche" @endif></div>
                                    <span>NON</span><div @if ($demandeur->aspects_financiers_prestation_compensation == "Non") class="coche" @endif></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </td>
        </tr>
    </table>
    <table style=" border-top:2px solid #000;padding-top: 5px; margin-top: 5px;">
        <tr>
           <td>
               <table style="width: 100%;">
                   <tr>
                       <td style=" padding: 10px;border: 1px solid #000;display: block;height: 100px;">
                           <p style="font-size: 12px;font-weight: bold;margin-bottom: 0;">COMMENTAIRES</p>
                           <p style="font-size: 12px;">
                               {{$demandeur->aspects_financiers_commentaire}}
                           </p>

                       </td>
                   </tr>
               </table>
           </td>
        </tr>

    </table>
    <table style=" border-top:2px solid #000;padding-top: 5px; margin-top: 5px;">
        <tr style="margin-top: -3px;">
            <td>
                <table>
                    <tr>
                        <td class="checkbox">
                            <label for="Date entrée souhaiteé" style="font-size: 13px;font-weight:bold;">DATE D’ENTREE SOUHAITEE :</label>
                            <span>Immédiat</span><div @if ($demandeur->date_entree_souhaitee == "Immédiat") class="coche" @endif></div>
                            <span>Dans les 6 mois</span><div @if ($demandeur->date_entree_souhaitee == "Dans les 6 mois") class="coche" @endif></div>
                            <span>Echéance plus lointaine</span><div @if ($demandeur->date_entree_souhaitee == "Echéance plus lointaine") class="coche" @endif></div>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td class="checkbox">
                            <label for="Date entrée souhaiteé" style="font-size: 13px;font-weight:bold;">DATE D’ENTREE SOUHAITEE EN HEBERGEMENT TEMPORAIRE :</label>
                            {{$demandeur->date_entree_souhaitee_hergemenent_temporaire}}
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td class="checkbox" style="padding-top:15px;">
                            <label for="Date entrée souhaiteé">Date de la demande :</label>
                            {{$demandeur->date_entree_date_demande}}
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td class="checkbox">
                            <label for="Date entrée souhaiteé">Signature de la personne concernée ou<br/>
                                de son représentant légal</label>
                        </td>
                    </tr>
                </table>
                <table style="padding-top:10px;">
                    <tr>
                        <td style="padding:10px;border:1px solid #000;width: 40%;display: inline-block;height:60px">
                            <p></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="padding-top:20px;">
        <tr>
            <td>
                <p style="font-size: 13px;">La loi n°78-17 du 6 janvier 1978 modifiée relative à l’informatique, aux fichiers et aux libertés s’applique aux réponses faites sur ce formulaire. Elle garantit un droit d’accès et de rectification aux informations vous concernant auprès de l’établissement auquel vous avez adressé votre demande d’admission. Vous pouvez également, pour des motifs légitimes, vous opposer au traitement des données vous concernant.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="font-size: 11px;line-height: 15px;">*Dans certains établissements, l’APA, qui a vocation à prendre en charge le tarif dépendance, est versée directement aux établissements. Dans ce cas de figure, il n’y a pas de demande à réaliser. Pour plus d’informations, il convient de prendre contact auprès du conseil général ou de l’établissement souhaité.</p>
            </td>
        </tr>


    </table>
</div>
