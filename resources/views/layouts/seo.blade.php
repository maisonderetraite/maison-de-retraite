@if (Request::path() == '/')
<title>Trouver rapidement une maison de retraite | Maisonderetraite.net</title>
<meta name="description" content="Maisonderetraite.net est un organisme vous permettant de trouver une maison de retraite  en France, de manière rapide. Service gratuit - Conseillers disponibles 24h/24 et 7j/7." />
@endif

@if (Request::path() == 'confirmation')
<title>Un conseiller vous rappelle | Maisonderetraite.net</title>
<meta name="description" content="Notre organisme Maisonderetraite.net vous rappelle dans les plus brefs délais pour vous proposer une sélection de maison de retraite selon vos besoins." />
@endif


@if (Request::path() == 'qui-sommes-nous')
<title>Connaître l’organisme Maisonderetraite.net</title>
<meta name="description" content="Notre organisme Maisonderetraite.net propose un service gratuit permettant de vous aider à trouver une maison de retraite correspondant à vos besoins. Les conseillers sont à votre écoute 24h/24 et 7j/7." />
@endif

@if (Request::path() == 'notre-mission')
<title>La mission de Maisonderetraite.net</title>
<meta name="description" content="Notre organisme Maisonderetraite.net a pour mission de vous aider à trouver la maison de retraite qui correspond à vos besoins (budget, lieu, pathologies…). Nos conseillers sont disponibles 24h/24 et 7j/7. Ce service est gratuit." />
@endif

@if (Request::path() == 'contact')
<title>Contacter Maisonderetraite.net | Service gratuit</title>
<meta name="description" content="Nos conseillers sont disponibles 24h/24 et 7j/7 par téléphone, chat ou e-mail. Le service proposé par notre organisme est 100% gratuit." />
@endif

@if (Request::path() == 'mentions-legales')
<title>Mentions légales | Maisonderetraite.net</title>
<meta name="description" content="Maisonderetraite.net est un organisme qui offre un service 100% gratuit. Nos conseillers sont disponibles 24h/24 et 7j/7 afin de trouver une maison de retraite correspondant à vos besoins." />
@endif

@if (Request::path() == 'politique-de-confidentialite')
<title>Politique de confidentialité | Maisonderetraite.net</title>
<meta name="description" content="Maisonderetraite.net est un organisme qui offre un service 100% gratuit. Nos conseillers sont à votre écoute pour vous accompagner dans la recherche d’une maison de retraite." />
@endif

@if (Request::path() == 'cgu-cgv')
<title>CGU/CGV | Maisonderetraite.net</title>
<meta name="description" content="Maisonderetraite.net est un organisme qui offre un service 100% gratuit. Des conseillers à votre écoute 24h/24 et 7j/7 pour vous accompagner dans la recherche d’une maison de retraite." />
@endif