<header>
    <div id="topheader">
        <a id="logo" href="{{url('/admin')}}">
            {{ HTML::image('images/logo-white.png', 'Maison de retraite') }}
        </a>
        <div id="menu">
            <ul>
                <li><a href="{{ url('admin/') }}">Liste demandes</a>
                    <ul>

                        @if (Request::path() == 'admin/demandes/creation')
                        <li><a href="{{ url('/admin/demandes/creation') }}" class="active">Créer une fiche</a></li>
                        @else
                        <li><a href="{{ url('admin/demandes/creation') }}">Créer une fiche</a></li>
                        @endif
                        @if($user->roles != "operateur")
                            @if (Request::path() == 'admin')
                            <li><a href="{{ url('/admin/') }}" class="active">A traiter</a></li>
                            @else
                            <li><a href="{{ url('/admin/') }}">A traiter</a></li>
                            @endif

                            @if (Request::path() == 'admin/demandes/en-cours')
                            <li><a href="{{ url('/admin/demandes/en-cours') }}" class="active">En cours</a></li>
                            @else
                            <li><a href="{{ url('/admin/demandes/en-cours') }}">En cours</a></li>
                            @endif

                            @if (Request::path() == 'admin/demandes/archive')
                            <li><a href="{{ url('/admin/demandes/archive') }}" class="active">Archivées / traitées</a></li>
                            @else
                            <li><a href="{{ url('/admin/demandes/archive') }}">Archivées / traitées</a></li>
                            @endif

                            @if (Request::path() == 'admin/demandes/dossier-transmis-en-ehpad')
                            <li><a href="{{ url('/admin/demandes/dossier-transmis-en-ehpad') }}" class="active">Dossier envoyés aux Ehpad</a></li>
                            @else
                            <li><a href="{{ url('/admin/demandes/dossier-transmis-en-ehpad') }}">Dossier envoyés aux Ehpad</a></li>
                            @endif
                        @endif
                    </ul>
                </li>
                @if($user->roles == "administrateur")
                    @if (Request::path() == 'admin/utilisateurs')
                    <li>
                        <a href="{{ url('/admin/utilisateurs') }}" class="active">Utilisateurs</a>

                    </li>
                    @else
                    <li><a href="{{ url('/admin/utilisateurs') }}">Utilisateurs</a></li>
                    @endif
                @endif


                @if (Request::path() == 'admin/faq')
                <li><a href="{{ url('/admin/faq') }}" class="active">Faq</a></li>
                @else
                <li><a href="{{ url('/admin/faq') }}">Faq</a></li>
                @endif
                @if($user->roles != "operateur")
                    @if (Request::path() == 'admin/ehpad')
                    <li>
                        <a href="{{ url('/admin/ehpad') }}" class="active">Listing ehpad</a>
                        <ul>
                            <li><a href="{{ url('/admin/ehpad/creation') }}">Création ehpad</a> </li>
                        </ul>
                    </li>
                    @else
                    <li>
                        <a href="{{ url('/admin/ehpad') }}">Listing ehpad</a>
                        <ul>
                            <li><a href="{{ url('/admin/ehpad/creation') }}">Création ehpad</a> </li>
                        </ul>
                    </li>
                    @endif

                @endif

                @if($user->roles == "commercial")
                @if (Request::path() == 'admin/faq/suggestion')
                <li><a href="{{ url('/admin/suggestion') }}" class="active">Suggérer une question</a></li>
                @else
                <li><a href="{{ url('admin/suggestion') }}">Suggérer une question</a></li>
                @endif
                @endif
            </ul>

        </div>
    </div>
</header>