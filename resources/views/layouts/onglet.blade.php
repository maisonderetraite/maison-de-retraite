<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex">
    <title>{{ config('app.name', 'La maison de retraite') }}</title>
    <link rel="icon" type="image/png" href="{{url('/images/favicon.png')}}" />
    <!-- Styles -->
    <link href="https://file.myfontastic.com/cdBPjzbe2V8MKGteiJ822Z/icons.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
<div id="main">
    @include('layouts.admin.header')
    <div id="admincontent">
        @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        <div id="topheader">
            <ul id="profil">
                <li>
                    <a id="showprofil">{{ Auth::user()->name }} {{ Auth::user()->firstname }}<i class="mc-left-chevron"></i></a>
                    <div id="dropdown-menu">
                        <div class="bloc">
                            <div id="infos-head">
                                <p id="name-head">{{ Auth::user()->name }} {{ Auth::user()->firstname }}</p>
                                <p id="email-head">{{ Auth::user()->email }}</p>
                            </div>
                        </div>

                        <a id="logout" href="{{ url('/logout') }}"  onclick="event.preventDefault();  document.getElementById('logout-form').submit();">Deconnexion</a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>

                    </div>
                </li>
            </ul>
        </div>
        <div id="bloc-statut">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="box">
                        <p id="dossiergererpar">
                            <span>Dossier géré par :</span>
                            @if ($demandeur->users )
                            {{ $demandeur->users->email }}
                            @else
                            <p>Dossier non pris en charge</p>
                            @endif
                            @if (!$demandeur->users )
                                {!! Form::open(array('url' => '/admin/'.$demandeur->id .'')) !!}
                                {!! Form::hidden('_method', 'put') !!}
                                <button class="prise-en-charge alert">
                                    Prendre en charge ce dossier
                                </button>
                                {!! Form::close() !!}
                            @endif
                            @if($user->roles == "administrateur")
                                {!! Form::model($demandeur, ['url' => action("Admin\AdminController@updateuser", [$demandeur, $onglet]), 'method' =>"Put"]) !!}
                                    {{ Form::select('users_id',$users, null, ['placeholder' => 'Selectionner un commercial']) }}
                                    <button type="submit">Changer de commercial</button>
                                {!! Form::close() !!}
                            @endif
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="box">
                        <p id="statutencours">
                            <span>Statut du dossier :</span>
                            {{ $demandeur->statut }}
                        </p>
                        <div id="selectstatut">
                            {!! Form::model($demandeur, ['url' => action("Admin\AdminController@updatestatut", [$demandeur, $onglet]), 'method' =>"Put"]) !!}
                            {{ Form::select('statut', ['En cours' => 'En cours', 'Archivé' => 'Archivé','Dossier transmis en Ehpad' => 'Dossier transmis en Ehpad' ], null, ['placeholder' => 'Selectionner un statut']) }}

                            <button type="submit">Mettre à jour</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <table id="admin-onglet">
            <td class="col-10 @if ( $onglet == "0" ) actif @endif">Informations recherche<a href="{{ asset('admin/fiche/'.$demandeur->id.'/onglet-0') }}"></a></td>
            <td class="col-10 @if ( $onglet == "1" ) actif @endif">Etat civil senior<a href="{{ asset('admin/fiche/'.$demandeur->id.'/onglet-1') }}"></a></td>
            <td class="col-10 @if ( $onglet == "2" ) actif @endif">Civil<br/>représentant légal<a href="{{ asset('admin/fiche/'.$demandeur->id.'/onglet-2') }}"></a></td>
            <td class="col-10 @if ( $onglet == "3" ) actif @endif">Personne<br/>confiance<a href="{{ asset('admin/fiche/'.$demandeur->id.'/onglet-3') }}"></a></td>
            <td class="col-10 @if ( $onglet == "4" ) actif @endif">Personne à contacter<a href="{{ asset('admin/fiche/'.$demandeur->id.'/onglet-4') }}"></a></td>
            <td class="col-10 @if ( $onglet == "5" ) actif @endif">Demande<a href="{{ asset('admin/fiche/'.$demandeur->id.'/onglet-5') }}"></a></td>
            <td class="col-10 @if ( $onglet == "6" ) actif @endif">Budget<a href="{{ asset('admin/fiche/'.$demandeur->id.'/onglet-6') }}"></a></td>
            <td class="col-10 @if ( $onglet == "7" ) actif @endif">Date d'entrée<a href="{{ asset('admin/fiche/'.$demandeur->id.'/onglet-7') }}"></a></td>
            <td class="col-10 @if ( $onglet == "8" ) actif @endif">Suivi commentaires commercial<a href="{{ asset('admin/fiche/'.$demandeur->id.'/onglet-8') }}"></a></td>
            <td class="col-10 @if ( $onglet == "9" ) actif @endif">Maison de retraite sélectionnée"<a href="{{ asset('admin/fiche/'.$demandeur->id.'/onglet-9') }}"></a></td>
            <td class="col-10 @if ( $onglet == "10" ) actif @endif">Clôture du dossier<a href="{{ asset('admin/fiche/'.$demandeur->id.'/onglet-10') }}"></a></td>
        </table>


        @yield('content')
    </div>

</div>

<!-- Scripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
<script src="{{ asset('js/admin.js') }}"></script>

</body>
</html>
