<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NSQ9RS8');</script>
    <!-- End Google Tag Manager —>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @if (Request::path() == '0-place' || Request::path() == '1-place' || Request::path() == '2+-place' )
    <meta name="robots" content="noindex, nofollow">
    @endif
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('layouts.seo')
    <link rel="icon" type="image/png" href="{{url('/images/favicon.png')}}" />
    <!-- Styles -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://file.myfontastic.com/cdBPjzbe2V8MKGteiJ822Z/icons.css" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <!--Start of Zendesk Chat Script-->
    <!--<script type="text/javascript">
        window.$zopim||(function(d,s){var z=$zopim=function(c){
            z._.push(c)},$=z.s=
            d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
        _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
            $.src='https://v2.zopim.com/?4gTuwkqcU1YHzzS8SwBxmfmhTFRsKZmW';z.t=+new Date;$.
                type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
    </script>-->
    <!--End of Zendesk Chat Script-->

</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NSQ9RS8"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


    <div id="main">
        <header>
            <div id="topheader">
                <div class="container">
                    <p id="free-service">Service gratuit</p>
                    <i id="button-mobile" class="mdr-menu closed"></i>
                    <a id="logo" href="{{url('/')}}">
                        {{ HTML::image('images/logo.png', 'Maison de retraite') }}
                    </a>
                    <a href="tel:0800947750" id="callback" class="header-call" >{{ HTML::image('images/modal-tel.png', 'Maison de retraite') }}</a>
                </div>
            </div>
            <div id="menu">
                <div class="container">
                    <ul>
                        <li id="qui-sommes-nous">{{ HTML::linkAction('PageController@quisommesnous','Qui sommes nous ?') }}</li>
                        <li id="notre-mission">{{ HTML::linkAction('PageController@mission','Notre mission') }}</li>
                        <li id="nous-contactez">{{ HTML::linkAction('PageController@contact','Nous contacter') }}</li>
                        <!--<li id="demande-urgent"><a class="big-link" data-reveal-id="Modal-rappel"><p>Être appelé<span>immédiatement</span></p></a></li>-->
                    </ul>
                </div>
            </div>
        </header>
        @yield('content')
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div id="menu-footer">
                            <ul>
                                <li>{{ HTML::linkAction('PageController@quisommesnous','Qui sommes nous ?') }}</li>
                                <li>{{ HTML::linkAction('PageController@ml','Mentions légales') }}</li>
                                <li>{{ HTML::linkAction('PageController@pdc','Politique de confidentialité') }}</li>
                                <li>{{ HTML::linkAction('PageController@cgu','CGU / CGV') }}</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div id="docs-footer">
                            <p>Documents à télécharger :</p>
                            <ul>
                                <li><a target="_blank" href="{{ asset('documents/CERFA.pdf') }}">Demande d’admission <i class="mdr-down"></i></a></li>
                                <li><a target="_blank" href="{{ asset('documents/dossiermedical.pdf') }}">Dossier médical<i class="mdr-down"></i></a></li>
                            </ul>
                            <span id="copyright">Copyright © 2017 ­ Maisonderetraite.net</span>
                        </div>
                    </div>
                </div>

            </div>
        </footer>

        <div id="Modal-tel" class="reveal-modal xlarge ">
            <a class="close-reveal-modal"><i class="mdr-close"></i></a>
            <p class="title"><i class="mdr-tel"></i>Contacter Maisonderetraite.net :</p>
            <a href="tel:0800947750" >{{ HTML::image('images/modal-tel.png', 'Maison de retraite - Téléphone') }}</a>
            <span>24 heures sur 24, 7 jours sur 7</span>
        </div>


        <div id="Modal-mail" class="reveal-modal xlarge ">
            <a class="close-reveal-modal"><i class="mdr-close"></i></a>
            <p class="title"><i class="mdr-email"></i>Vous souhaitez être contacté par mail</p>
            {!! Form::open(array('route' => 'contact_store', 'class' => 'form')) !!}
                <div class="row">
                    <div id="nom-bloc-contact" class="col-xs-12 col-sm-6">
                        <label for="">Nom*</label>
                        {!! Form::text('nom', null, ['class' => 'inputeffect']) !!}
                        <p class="errortext"></p>
                    </div>
                    <div id="prenom-bloc-contact" class="col-xs-12 col-sm-6">
                        <label for="">Prénom*</label>
                        {!! Form::text('prenom', null, ['class' => 'inputeffect']) !!}
                        <p class="errortext"></p>
                    </div>
                    <div class="clear"></div>
                    <div id="email-bloc-contact" class="col-xs-12 col-sm-6">
                        <label for="">Email*</label>
                        {!! Form::text('mail', null, ['class' => 'inputeffect']) !!}
                        <p class="errortext"></p>
                    </div>
                    <div id="telephone-bloc" class="col-xs-12 col-sm-6">
                        <label for="">Téléphone</label>
                        {!! Form::text('telephone', null, ['class' => 'inputeffect']) !!}
                        <p class="errortext"></p>
                    </div>
                    <div class="clear"></div>
                    <div id="objet-bloc" class="col-xs-12">
                        <label for="">Objet</label>

                        {!! Form::text('objet', null, ['class' => 'inputeffect']) !!}
                        <p class="errortext"></p>
                    </div>
                    <div id="message-bloc" class="col-xs-12">
                        <label for="">Message</label>

                        {!! Form::textarea('texte', null, ['class' => 'inputeffect']) !!}
                        <p class="errortext"></p>
                    </div>
                    <div class="col-xs-12">
                        <div class="buttons">
                            <p id="requiredfields">* champs obligatoires</p>
                            <button type="submit" id="contact-email">Envoyer</button>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>

        <div id="Modal-rappel" class="reveal-modal xlarge ">
            <a class="close-reveal-modal"><i class="mdr-close"></i></a>
            <p class="title"><i class="mdr-call-center"></i>Un conseiller vous rappelle :</p>
            <div id="button">
               <p><a id="button-immediat"><i class="mdr-play"></i>Maintenant</a></p>
            </div>

            <div id="immediat">
                <p>Merci de nous indiquer votre numéro de téléphone,
                    un conseiller vous rappelle :</p>

                {!! Form::open(array('route' => 'contact_call_aswat', 'class' => 'form')) !!}
                    <input type="text" id="contact_call_aswat_phone" name="contact_call_aswat" required="required" pattern="^[0-9]{10}$" maxlength="10" class="form-control" placeholder="Téléphone">
                    <input type="submit" value="Valider">
                {!! Form::close() !!}
            </div>
        </div>

    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>


</body>
</html>
