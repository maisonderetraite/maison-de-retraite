<style>
    @page { margin:20px 30px; }
    body {
        font-family: 'Arial',sans-serif;
        font-size: 14px;
        line-height: 20px;
    }
    p {
        margin: 0;
    }
    .clear {
        clear:both;
    }
    .page-break {
        page-break-after: always;
    }

    #cerfa {
        text-align: center;
    }
    td {
        vertical-align: middle;
        padding: 0;
    }
    .checkbox div  {
        width:10px;height:10px;border: 1px solid #000;display: inline-block;margin-right: 20px;margin-left:10px;margin-top: 1px;
    }
    .checkbox label {
        margin-right: 20px;
    }
    .checkbox span {
        font-size: 12px;
    }

    #page-form-3 {
        width:100%;
    }
    #page-form-3 p {
        margin-bottom: 0;

    }
    #page-form-3 label {
        font-size: 13px;
    }
    table {
        width:100%;
    }
    .trait {
        padding: 0;
        margin: 0;

    }
    .trait td {
        width: 8px;
        display: inline-block;
        border-bottom: 1px solid #000;
        padding: 0 5px;
        margin-left: 0px;
    }


</style>

@yield('page')
