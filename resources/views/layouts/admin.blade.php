<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex">
    <title>{{ config('app.name', 'La maison de retraite') }}</title>
    <link rel="icon" type="image/png" href="{{url('/images/favicon.png')}}" />
    <!-- Styles -->
    <link href="https://file.myfontastic.com/cdBPjzbe2V8MKGteiJ822Z/icons.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
<div id="main">

    @include('layouts.admin.header')

    <div id="admincontent">
        @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        <div id="topheader">
            <ul id="profil">
                <li>
                    <a id="showprofil">{{ Auth::user()->name }} {{ Auth::user()->firstname }}<i class="mdr-angle-left"></i></a>
                    <div id="dropdown-menu">
                        <div class="bloc">
                            <div id="infos-head">
                                <p id="name-head">{{ Auth::user()->name }} {{ Auth::user()->firstname }}</p>
                                <p id="email-head">{{ Auth::user()->email }}</p>
                            </div>
                        </div>
                        <a id="logout" href="{{ url('/logout') }}"  onclick="event.preventDefault();  document.getElementById('logout-form').submit();">Deconnexion</a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>

                    </div>
                </li>
            </ul>
        </div>
        @yield('content')
    </div>
</div>
<!-- Scripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
<script src="{{ asset('js/admin.js') }}"></script>
<script>
    $(document).on( "click", ".list-ville li", function() {
        var ville = $(this).text();
        var parent = $(this).parent();
        var bigparent = parent.parent();
        bigparent.find('.secteur').val(ville);
        parent.hide();
    })


</script>
<div id="fade"></div>
</body>
</html>
