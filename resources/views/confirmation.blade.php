@extends('layouts.app')
@section('content')
<div id="home">
    <div id="formulaire">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div id="form">
                        <div class="title">
                            <div>
                                <i class="mdr-search-form"></i>
                                <p><span>TROUVER UNE MAISON DE RETRAITE</span>
                                    Orientation rapide et gratuite
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-10 col-md-offset-1">
                                <!-- multistep form -->
                                <div id="msform">
                                    <!-- progressbar -->
                                    <ul id="progressbar">
                                        <li class="active"><span>1.</span> Critères de recherche</li>
                                        <li class="active"><span>2.</span> Vos Coordonnées</li>
                                        <li class="active"><span>3.</span> Résultats</li>
                                    </ul>
                                    <div class="content">
                                        <p id="confirm-msg">Votre demande est enregistrée.</p>
                                        <p id="rappel-msg">Un conseiller vous contactera dans les plus brefs délais
                                            <span> pour vous accompagner dans vos démarches.</span></p>
                                    </div>
                                    <div class="confirmation-phone">
                                        <div id="information-rappel">
                                            <p>Pour toutes autres questions,<br/>
                                            un conseiller est à votre écoute au</p>
                                        </div>
                                        <div id="numero-form">
                                            <a href="tel:0800947750">{{ HTML::image('images/rappel-confirm.png', 'Maison de retraite') }}</a>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <div id="informations-call">
        <div class="container">
            <p class="big-title">Contacter notre organisme pour trouver <br/>la maison de retraite qui vous convient.</p>
            <div class="row">
                <div class="col-xs-6 col-sm-3 bloc">
                    <p id="chat_button">
                        <i class="mdr-chat"></i>
                        <span>Par Chat</span>
                    </p>
                </div>
                <div class="col-xs-6 col-sm-3 bloc">
                    <a href="#" class="big-link" data-reveal-id="Modal-mail">
                        <p>
                            <i class="mdr-email"></i>
                            <span>Par Mail</span>
                        </p>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-3 bloc">
                    <a href="#" class="big-link" data-reveal-id="Modal-tel">
                        <p>
                            <i class="mdr-tel"></i>
                            <span>Par Téléphone</span>
                        </p>
                    </a>

                </div>
                <div class="col-xs-6 col-sm-3 bloc">
                    <a href="#" class="big-link" data-reveal-id="Modal-rappel">
                        <p>
                            <i class="mdr-chat"></i>
                            <span>Se faire rappeler</span>
                        </p>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="avis-commentaire">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <div class="box">
                        <div class="photo">
                            {{ HTML::image('images/solange.png', 'Maison de retraite') }}
                        </div>
                        <div class="votes">
                            <ul>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                            </ul>
                        </div>
                        <p>"Très satisfaite / Ce service m’a permis de trouver une résidence adaptée à mes critères de recherche, le tout avec beaucoup d’écoute et de sympathie."</p>
                        <span class="author">Solange M</span>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="box">
                        <div class="photo">
                            {{ HTML::image('images/catherine.png', 'Maison de retraite') }}
                        </div>
                        <div class="votes">
                            <ul>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                            </ul>
                        </div>
                        <p>"Un service efficace !!! / Ma maison de retraite, nous a permis de placer notre maman très rapidement malgré de nombreux critères d’exigences. Bravo !"</p>
                        <span class="author">Catherine M</span>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="box">
                        <div class="photo">
                            {{ HTML::image('images/jacques.png', 'Maison de retraite') }}
                        </div>
                        <div class="votes">
                            <ul>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                                <li class="mdr-star" style="opacity:0.5"></li>
                            </ul>
                        </div>
                        <p>"Merci au conseiller / Une aide précieuse !<br/>Merci encore !"</p>
                        <span class="author">Jacques D</span>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div id="contenu">
        <div class="container">
            <h2> Maisonderetraite.net : une offre large de résidences pour seniors </h2>
            <p>Maisonderetraite.net est un organisme d’aide aux seniors qui vous accompagne dans la recherche d’une <strong> résidence spécialisée </strong> (Ehpad, <strong>maison de retraite </strong>, résidence services). Nos conseillers sont à votre écoute 7 jours sur 7 et 24 heures sur 24 pour vous  aider à trouver la <strong> résidence </strong> la plus adaptée à vos besoins.</p>

            <h2> Comment contacter les conseillers Maisonderetraite.net ? </h2>
            <p>Plusieurs moyens sont à votre disposition pour être en relation avec nos conseillers : par téléphone, e-mail ou depuis le chat en ligne. Nous disposons d’un très large choix d’établissements pour personnes âgées (EHPAD, maison de retraite, résidence services) dans toute la France. Notre conseiller prendra également en considération votre budget et les critères relatifs à la situation du demandeur.</p>

            <h2> Trouver une maison de retraite en urgence </h2>
            <p>Notre service propose également une offre d’urgence permettant de trouver rapidement une place en établissement. Nos professionnels sont disponibles à tout moment pour vous accompagner et vous conseiller.</p>
        </div>
    </div>
    <div id="reassurance">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <i class="mdr-happy"></i>
                    <p>Service Gratuit</p>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <i class="mdr-plus"></i>
                    <p>Professionnels Spécialisés</p>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <i class="mdr-information"></i>
                    <p>Aide & Conseils</p>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
