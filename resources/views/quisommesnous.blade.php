@extends('layouts.app')
@section('content')
<div id="quisommesnous">
    <div class="bandeau">
        <div class="container">
            <p>Qui sommes nous ?</p>
        </div>
    </div>
    <div id="background">
    </div>
    <div id="content">
        <div class="container">
            <div class="contenu">
                <p>Maisonderetraite.net est un service gratuit, permettant de trouver la résidence pour seniors qui correspond à vos besoins. Nos conseillers vous accompagnent jusqu’à l’obtention d’une place dans un établissement spécialisé (EHPAD, maison de retraite, résidence services).
                </p>

            </div>
        </div>
    </div>
    <div id="list-mission">
        <div class="bloc">
            <div class="container">
                <span class="title"><i class="mdr-home-heart"></i>A qui ce service est-il destiné ?</span>
                <p>Ce service est destiné à toutes les personnes souhaitant trouver une place dans une résidence pour seniors. Il s’adresse aussi bien aux seniors qu’à leurs aidants (famille, amis…), qui souhaitent trouver une résidence de manière rapide et efficace. Notre service s’adresse également aux prescripteurs tels que les médecins, les assistantes sociales et les mandataires de justice (tuteurs, curateurs…) </p>
            </div>

        </div>
        <div class="bloc">
            <div class="container">
                <span class="title"><i class="mdr-engrenage"></i>Comment ça marche ?</span>
                <p>Pour accéder à nos services, il vous suffit de nous contacter par téléphone ou de remplir le formulaire de contact pour être ensuite rappelé dans le plus bref délai. Nos conseillers prendront en considération vos critères afin de vous proposer la meilleure solution d’accueil. Ma Maison de Retraite vous propose également la possibilité de discuter en ligne avec un conseiller via le chat ou de nous contacter depuis le formulaire de contact.</p>
            </div>
        </div>
        <div class="bloc">
            <div class="container">
                <span class="title"><i class="mdr-call-center"></i>Qui sont les conseillers ?</span>
                <p>Nos conseillers sont à votre écoute 7 jours sur 7 et 24 heures sur 24. Notre équipe est composée de spécialistes du grand âge et de l’aide à la personne, par ailleurs, expérimentés dans la plupart des domaines de l’accompagnement des personnes âgées. Nos conseillers sauront être à votre écoute et soucieux de votre bien-être.</p>
            </div>
        </div>
        <div class="bloc">
            <div class="container">
                <span class="title"><i class="mdr-agreement"></i>Quels sont nos engagements ? </span>
                <p>Maisonderetraite.net offre :<br/>
                    - Un service gratuit<br/>
                    - Un accompagnement de qualité<br/>
                    - Une orientation personnalisée<br/>
                    - Un large choix de maisons de retraite dans toute la France<br/>
                    - Des conseillers attentifs, à votre écoute 24 heures sur 24 et 7 jours sur 7
                </p>
            </div>
        </div>
    </div>
</div>
@endsection
