@extends('layouts.app')
@section('content')
<div id="home">
    <div class="bandeau">
        <div class="container">
            <p>Notre objectif : vous proposer la résidence adaptée à vos besoins</p>
        </div>
    </div>
    <div id="formulaire">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div id="form">
                        <div class="title">
                            <div>
                                <i class="mdr-search-form"></i>
                                <p><span>TROUVER UNE MAISON DE RETRAITE</span>
                                    Orientation rapide et gratuite
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                @if(session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                                @endif
                                @if(session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                                @endif
                            <!-- multistep form -->
                                {!! Form::model($demandeurs, ['url' => action("HomeController@store"), 'method' => "Post", 'id' => 'msform']) !!}
                                    <!-- progressbar -->
                                    <ul id="progressbar">
                                        <li class="active"><span>1.</span> Critères de recherche</li>
                                        <li><span>2.</span> Vos coordonnées</li>
                                        <li><span>3.</span> Résultats</li>
                                    </ul>
                                    <!-- fieldsets -->
                                    <fieldset>
                                        <div class="etape-one">
                                            <div class="row">
                                                <div id="name-firstname-bloc" class="col-xs-12 col-sm-7">
                                                    <label for="name_firstname">Nom & prénom de la personne âgée :</label>
                                                    <p class="errortext"></p>
                                                        {!! Form::text('name_firstname', null, ['class' => 'inputeffect']) !!}
                                                </div>
                                                <div id="age-bloc" class="col-xs-12 col-sm-5">
                                                    <label for="age">Âge :</label>
                                                    <p class="errortext"></p>
                                                    {!! Form::number('age', '60', ['class' => 'inputeffect', 'min' =>'60','max' =>'130','id'=> 'agenumber']) !!}
                                                </div>


                                                <div id="lien-bloc" class="col-xs-12 col-sm-7">

                                                    <label for="lien">Lien avec la personne concernée :</label>
                                                    <p class="errortext"></p>
                                                    <div class="select-custom">
                                                        {!! Form::select('lien_personne', array('Moi-même' => 'Moi-même', 'Famille' => 'Famille' , 'Ami' => 'Ami', 'Médecin' => 'Médecin', 'Assistante sociale' => 'Assistante sociale','Mandataire judiciaire' => 'Mandataire judiciaire','Autre' => 'Autre'), null, ['class' => 'inputeffect'] ) !!}
                                                    </div>

                                                </div>
                                                <div id="periode-bloc" class="col-xs-12 col-sm-5">
                                                    <label for="periode">Période souhaitée :
                                                        <p class="errortext"></p>
                                                    </label>
                                                    <ul>
                                                        <li>
                                                            {{ Form::radio('periode_souhaite', 'urgent', '', ['id' => 'urgent']) }}
                                                            <label for="urgent">Demande urgente</label>
                                                            <div class="check"></div>
                                                        </li>

                                                        <li>
                                                            {{ Form::radio('periode_souhaite', '1 à 3 mois', '', ['id' => '1-3mois']) }}
                                                            <label for="1-3mois">1 à 3 mois</label>

                                                            <div class="check"><div class="inside"></div></div>
                                                        </li>

                                                        <li>
                                                            {{ Form::radio('periode_souhaite', '+ de 3 mois', '', ['id' => 'plusde3mois']) }}
                                                            <label for="plusde3mois">+ de 3 mois</label>

                                                            <div class="check"><div class="inside"></div></div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div id="secteur-bloc" class="col-xs-12 col-sm-7">
                                                    <label for="secteur">Secteur géographique :</label>
                                                    <div id="secteur-list">
                                                        <div class="r-group">
                                                            <div id="first-secteur" class="input proposition">
                                                                <p class="errortext"></p>
                                                                {!! Form::text('secteur[]', null, [ 'id' => 'proposition_0' , 'data-pattern-id' => 'proposition__++', 'class' => 'secteur','placeholder' => 'Ville' ]) !!}
                                                            </div>
                                                            <div id="other-proposition" class="input proposition">
                                                                {!! Form::text('secteur[]', null, [ 'id' => 'proposition_0' , 'data-pattern-id' => 'proposition__++', 'class' => 'secteur','placeholder' => 'Ville' ]) !!}
                                                                <button type="button" class="r-btnRemove">Supprimer ce choix</button>
                                                            </div>
                                                        </div>
                                                        <button type="button" class="r-btnAdd">+ <span>ajouter un autre choix</span></button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <button id="form-submit" type="button" name="next" class="next action-button"/>
                                            <p>Rechercher<i class="mdr-play-1"></i></p>
                                        </button>
                                    </fieldset>
                                    <fieldset>
                                        <div id="etape-two">
                                            <div class="row">
                                                <div id="civilite-bloc" class="col-xs-12 col-sm-6">
                                                    <label for="nom">Civilité* :</label>

                                                    <ul clss="row">
                                                        <li>
                                                            <span>Monsieur</span>
                                                            {{ Form::radio('civilite', 'Monsieur', '', ['id' => 'monsieur']) }}
                                                        </li>
                                                        <li>

                                                            <span>Madame</span>
                                                            {{ Form::radio('civilite', 'Madame', '', ['id' => 'madame']) }}
                                                        </li>
                                                    </ul>
                                                    <p class="errortext"></p>
                                                </div>
                                                <div class="clear"></div>
                                                <div id="nom-bloc" class="col-xs-12 col-sm-6">

                                                    <label for="nom">Nom* :</label>
                                                    {!! Form::text('nom', null, ['class' => 'inputeffect']) !!}
                                                    <p class="errortext"></p>
                                                </div>
                                                <div id="prenom-bloc" class="col-xs-12 col-sm-6">

                                                    <label for="prenom">Prénom* :</label>
                                                    {!! Form::text('prenom', null, ['class' => 'inputeffect']) !!}
                                                    <p class="errortext"></p>
                                                </div>
                                                <div class="clear"></div>
                                                <div id="email-bloc" class="col-xs-12 col-sm-6">

                                                    <label for="email">Email* :</label>
                                                    {!! Form::text('email', null, ['class' => 'inputeffect']) !!}
                                                    <p class="errortext"></p>
                                                </div>
                                                <div id="telephone-bloc" class="col-xs-12 col-sm-6">

                                                    <label for="telephone">Téléphone* :</label>
                                                    {!! Form::text('telephone', null, ['class' => 'inputeffect']) !!}
                                                    <p class="errortext"></p>
                                                </div>

                                                <div id="remarque-bloc" class="col-xs-12 col-sm-6">


                                                    {!! Form::hidden('remarque', null, ['class' => 'inputeffect']) !!}

                                                </div>
                                            </div>
                                        </div>
                                        <div id="validerformulaire">
                                            <p id="cnil">
                                                Maisonderetraite.net s’engage à ne faire aucun usage commercial de vos coordonnées. Elles ne seront utilisées que pour le suivi de votre recherche.</p>
                                        </div>
                                        <button id="formulaire-button" type="submit" name="next" class="valid action-button"/>
                                        <p>Valider<i class="mdr-play-1"></i></p>
                                        </button>
                                    </fieldset>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="informations-call">
        <div class="container">
            <p class="big-title">Contacter notre organisme pour trouver <br/>la maison de retraite qui vous convient.</p>
            <div class="row">
                <!--<div class="col-xs-6 col-sm-3 bloc">
                    <p id="chat_button">
                        <i class="mdr-chat"></i>
                        <span>Par Chat</span>
                    </p>
                </div>-->
                <div class="col-xs-6 col-sm-3 col-sm-offset-3 bloc">
                    <a href="#" class="big-link" data-reveal-id="Modal-mail">
                        <p>
                            <i class="mdr-email"></i>
                            <span>Par Mail</span>
                        </p>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-3  bloc">
                    <a href="#" class="big-link" data-reveal-id="Modal-tel">
                        <p>
                            <i class="mdr-tel"></i>
                            <span>Par Téléphone</span>
                        </p>
                    </a>

                </div>
                <!--<div class="col-xs-6 col-sm-3 bloc">
                    <a href="#" class="big-link" data-reveal-id="Modal-rappel">
                        <p>
                            <i class="mdr-rappel"></i>
                            <span>Se faire rappeler</span>
                        </p>
                    </a>
                </div>-->
            </div>
        </div>
    </div>
    <div id="avis-commentaire">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="box">
                        <div class="photo">
                            {{ HTML::image('images/solange.png', 'Maison de retraite') }}
                        </div>
                        <div class="votes">
                            <ul>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                            </ul>
                        </div>
                        <p>"Très satisfaite / Ce service m’a permis de trouver une résidence adaptée à mes critères de recherche, le tout avec beaucoup d’écoute et de sympathie."</p>
                        <span class="author">Solange M</span>
                    </div>

                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="box">
                        <div class="photo">
                            {{ HTML::image('images/catherine.png', 'Maison de retraite') }}
                        </div>
                        <div class="votes">
                            <ul>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                            </ul>
                        </div>
                        <p>"Un service efficace !!! / Ma maison de retraite, nous a permis de placer notre maman très rapidement malgré de nombreux critères d’exigences. Bravo !"</p>
                        <span class="author">Catherine M</span>
                    </div>

                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="box">
                        <div class="photo">
                            {{ HTML::image('images/jacques.png', 'Maison de retraite') }}
                        </div>
                        <div class="votes">
                            <ul>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                                <li class="mdr-star"></li>
                                <li class="mdr-star" style="opacity:0.5"></li>
                            </ul>
                        </div>
                        <p>"Merci au conseiller / Une aide précieuse !<br/>Merci encore !"</p>
                        <span class="author">Jacques D</span>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <div id="contenu">
        <div class="container">
            <h2>Maisonderetraite.net : une offre large de résidences pour seniors</h2>
            <p>Maisonderetraite.net est un organisme d’aide aux seniors qui vous accompagne dans la recherche d’une <strong> résidence spécialisée </strong> (Ehpad, <strong>maison de retraite</strong>, résidence services). Nos conseillers sont à votre écoute 7 jours sur 7 et 24 heures sur 24 pour vous  aider à trouver la <strong>résidence</strong> la plus adaptée à vos besoins.</p>

            <h2>Comment contacter les conseillers Maisonderetraite.net ? </h2>
            <p>Plusieurs moyens sont à votre disposition pour être en relation avec nos conseillers : par téléphone, e-mail ou depuis le chat en ligne. Nous disposons d’un très large choix d’établissements pour personnes âgées (EHPAD, maison de retraite, résidence services) dans toute la France. Notre conseiller prendra également en considération votre budget et les critères relatifs à la situation du demandeur.</p>

            <h2>Trouver une maison de retraite en urgence</h2>
            <p>Notre service propose également une offre d’urgence permettant de trouver rapidement une place en établissement. Nos professionnels sont disponibles à tout moment pour vous accompagner et vous conseiller.</p>
        </div>

    </div>
    <div id="reassurance">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <i class="mdr-happy"></i>
                    <p>Service Gratuit</p>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <i class="mdr-plus"></i>
                    <p>Professionnels Spécialisés</p>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <i class="mdr-information"></i>
                    <p>Aide & Conseils</p>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
