@extends('layouts.onglet')
@section('content')
<div id="form">
    <p class="title">ETAT CIVIL DE LA PERSONNE CONCERNEE</p>
    {!! Form::model($demandeur, ['url' => action("Admin\AdminController@update", [$demandeur,1]), 'method' =>"Put"]) !!}
    <div class="row">
        <div class="col-xs-12 col-md-4">
            <div class="box">
                <p class="subtitle">Civilité :</p>
                <ul class="radio">
                    <li>
                        {{ Form::radio('etat_civil_civilite', 'Monsieur', '') }}
                        <label for="Monsieur">Monsieur</label>
                    </li>
                    <li>
                        {{ Form::radio('etat_civil_civilite', 'Madame', '') }}
                        <label for="Madame">Madame</label>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="box">
                <p class="subtitle">NOM de naissance (suivi, s’il y a lieu par le nom d’usage)</p>
                {!! Form::text('etat_civil_nom_de_naissance', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="box">
                <p class="subtitle">Prénom(s)</p>
                {!! Form::text('etat_civil_prenoms', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>
        <div class="clear"></div>
        <div class="col-xs-12 col-md-4">
            <div class="box">
                <p class="subtitle">Date de naissance</p>
                {!! Form::text('etat_civil_date_de_naissance', null, ['class' => 'datedemande']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-md-4">
            <div class="box">
                <p class="subtitle">Lieu de naissance</p>
                {!! Form::text('etat_civil_lieu_de_naissance', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-md-4">
            <div class="box">
                <p class="subtitle">Pays ou département</p>
                {!! Form::text('etat_civil_pays_ou_departement', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Numéro d'immatriculation</p>
                {!! Form::text('etat_civil_immatriculation', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>
        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Niveau de dépendance :</p>
                {!! Form::select('dependances', ['' => '', 'GIR 1' => 'GIR 1', 'GIR 2' => 'GIR 2', 'GIR 3' => 'GIR 3', 'Autres' => 'Autres'], $demandeur->dependances, ['class' => 'inputeffect'] ) !!}
            </div>
        </div>
        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">N° Voie, rue, boulevard</p>
                {!! Form::text('etat_civil_adresse_rue', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Code postal</p>
                {!! Form::text('etat_civil_adresse_cp', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Commune/Ville</p>
                {!! Form::text('etat_civil_adresse_commune_ville', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Téléphone fixe</p>
                {!! Form::text('etat_civil_telephone_fixe', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Téléphone portable</p>
                {!! Form::text('etat_civil_telephone_portable', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Adresse email</p>
                {!! Form::text('etat_civil_email', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">SITUATION FAMILIALE</p>
                <ul class="radio">
                    <li>
                        {{ Form::radio('etat_civil_situation_familiale', 'Célibataire', '') }}
                        <label for="Célibataire">Célibataire</label>
                    </li>
                    <li>
                        {{ Form::radio('etat_civil_situation_familiale', 'Vit maritalement', '') }}
                        <label for="Vit maritalement">Vit maritalement</label>
                    </li>
                    <li>
                        {{ Form::radio('etat_civil_situation_familiale', 'Pacsé(e)', '') }}
                        <label for="Pacsé(e)">Pacsé(e)</label>
                    </li>
                    <li>
                        {{ Form::radio('etat_civil_situation_familiale', 'Marié(e)', '') }}
                        <label for="Marié(e)">Marié(e)</label>
                    </li>
                    <li>
                        {{ Form::radio('etat_civil_situation_familiale', 'Veuf(ve)', '') }}
                        <label for="Veuf(ve)">Veuf(ve)</label>
                    </li>
                    <li>
                        {{ Form::radio('etat_civil_situation_familiale', 'Séparé(e)', '') }}
                        <label for="Séparé(e">Séparé(e</label>
                    </li>
                    <li>
                        {{ Form::radio('etat_civil_situation_familiale', ' Divorcé(e)', '') }}
                        <label for=" Divorcé(e)"> Divorcé(e)</label>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Nombre d'enfants</p>
                {!! Form::text('etat_civil_nb_enfants', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">MESURE DE PROTECTION JURIDIQUE</p>
                <ul class="radio" style="margin-bottom: 30px;">
                    <li>
                        {{ Form::radio('etat_civil_protection_juridique', 'Oui', '') }}
                        <label for="oui">Oui</label>
                    </li>
                    <li>
                        {{ Form::radio('etat_civil_protection_juridique', 'Non', '') }}
                        <label for="non">Non</label>
                    </li>
                    <li>
                        {{ Form::radio('etat_civil_protection_juridique', 'En cours', '') }}
                        <label for="En cours">En cours</label>
                    </li>
                </ul>
                <p class="subtitle">Si oui, laquelle :</p>
                <ul class="radio">
                    <li>
                        {{ Form::radio('etat_civil_protection_juridique_laquelle', 'Tutelle', '') }}
                        <label for="Tutelle">Tutelle</label>
                    </li>
                    <li>
                        {{ Form::radio('etat_civil_protection_juridique_laquelle', 'Curatelle', '') }}
                        <label for="Curatelle">Curatelle</label>
                    </li>
                    <li>
                        {{ Form::radio('etat_civil_protection_juridique_laquelle', 'Sauvegarde de justice', '') }}
                        <label for="Sauvegarde de justice">Sauvegarde de justice</label>
                    </li>
                    <li>
                        {{ Form::radio('etat_civil_protection_juridique_laquelle', 'Mandat de protection future', '') }}
                        <label for="Mandat de protection future">Mandat de protection future</label>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">CONTEXTE DE LA DEMANDE D’ADMISSION (évènement familial récent, décès du conjoint…) :</p>
                {!! Form::textarea('etat_civil_protection_contexte', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>
        <div class="col-xs-12">
            <div class="buttons">
                <button type="submit" id="submit-document">Enregistrer et étape suivante</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection
