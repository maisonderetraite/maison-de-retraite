@extends('layouts.onglet')
@section('content')
<div id="form">
    <p class="title">COORDONNEES DES PERSONNES A CONTACTER AU SUJET DE CETTE DEMANDE</p>
    {!! Form::model($demandeur, ['url' => action("Admin\AdminController@update", [$demandeur,4]), 'method' =>"Put"]) !!}
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">La personne concernée elle-même :</p>
                <ul class="radio">
                    <li>
                        {{ Form::radio('personne_contact_personne_concernee', 'Oui', '') }}
                        <label for="Oui">Oui</label>
                    </li>
                    <li>
                        {{ Form::radio('personne_contact_personne_concernee', 'Non', '') }}
                        <label for="Non">Non</label>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12">
            <p class="condition">Si ce n’est pas le cas, autre personne à contacter(1)</p>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">NOM de naissance (suivi, s’il y a lieu par le nom d’usage)</p>
                {!! Form::text('personne_contact_nom_de_naissance', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Prénom(s)</p>
                {!! Form::text('personne_contact_prenoms', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">N° Voie, rue, boulevard</p>
                {!! Form::text('personne_contact_adresse_rue', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Code postal</p>
                {!! Form::text('personne_contact_adresse_cp', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Commune/Ville</p>
                {!! Form::text('personne_contact_adresse_commune_ville', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Téléphone fixe</p>
                {!! Form::text('personne_contact_telephone_fixe', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Téléphone portable</p>
                {!! Form::text('personne_contact_telephone_portable', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>
        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Adresse email</p>
                {!! Form::text('personne_contact_email', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Lien de parenté ou de relation avec la personne concernée</p>
                {!! Form::text('personne_contact_lien_parente', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12">
            <p class="condition">Autre personne à contacter (2)</p>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">NOM de naissance (suivi, s’il y a lieu par le nom d’usage)</p>
                {!! Form::text('personne_contact2_nom_de_naissance', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Prénom(s)</p>
                {!! Form::text('personne_contact2_prenoms', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">N° Voie, rue, boulevard</p>
                {!! Form::text('personne_contact2_adresse_rue', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Code postal</p>
                {!! Form::text('personne_contact2_adresse_cp', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Commune/Ville</p>
                {!! Form::text('personne_contact2_adresse_commune_ville', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Téléphone fixe</p>
                {!! Form::text('personne_contact2_telephone_fixe', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Téléphone portable</p>
                {!! Form::text('personne_contact2_telephone_portable', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>
        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Adresse email</p>
                {!! Form::text('personne_contact2_email', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Lien de parenté ou de relation avec la personne concernée</p>
                {!! Form::text('personne_contact2_lien_parente', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12">
            <div class="buttons">
                <button type="submit" id="submit-document">Enregistrer et étape suivante</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection
