@extends('layouts.onglet')
@section('content')
<div id="form">

    <p class="title">DATE D’ENTREE</p>
    {!! Form::model($demandeur, ['url' => action("Admin\AdminController@update", [$demandeur,7]), 'method' =>"Put"]) !!}
    <div class="row">

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">DATE D’ENTREE SOUHAITEE : </p>
                <ul class="radio">
                    <li>
                        {{ Form::radio('date_entree_souhaitee', 'Immédiat', '') }}
                        <label for="DATE D’ENTREE SOUHAITEE :">Immédiat</label>
                    </li>
                    <li>
                        {{ Form::radio('date_entree_souhaitee', 'Dans les 6 mois', '') }}
                        <label for="Dans les 6 mois">Dans les 6 mois</label>
                    </li>
                    <li>
                        {{ Form::radio('date_entree_souhaitee', 'Echéance plus lointaine', '') }}
                        <label for="Echéance plus lointaine">Echéance plus lointaine</label>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">DATE D’ENTREE SOUHAITEE EN HEBERGEMENT TEMPORAIRE :</p>
                {!! Form::text('date_entree_souhaitee_hergemenent_temporaire', null, ['class' => 'datedemande']) !!}
            </div>
        </div>

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Date de la demande :</p>
                {!! Form::text('date_entree_date_demande', null, ['class' => 'datedemande']) !!}
            </div>
        </div>

        <div class="col-xs-12">
            <div class="buttons">
                <button type="submit" id="submit-document">Enregistrer et étape suivante</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection
