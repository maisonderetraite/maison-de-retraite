@extends('layouts.onglet')
@section('content')
<div id="form">
    <div class="col-xs-12 col-md-10 col-md-offset-1">
        <p class="title">Informations Recherche</p>
        {!! Form::model($demandeur, ['url' => action("Admin\AdminController@update", [$demandeur, 0]), 'method' =>"Put"]) !!}
        <div class="row">
           <div class="col-xs-12">
               <p class="title-small">Contact</p>
               <div class="box">
                   <div class="block">
                       <p class="subtitle">Civilite :</p>
                       <ul class="radio">
                           <li>
                               {{ Form::radio('civilite', 'Monsieur', '') }}
                               <label for="monsieur">Monsieur</label>
                           </li>
                           <li>
                               {{ Form::radio('civilite', 'Madame', '') }}
                               <label for="madame">Madame</label>
                           </li>
                       </ul>
                   </div>
                   <div class="block">
                       <p class="subtitle">Nom :</p>
                       {!! Form::text('nom', null, ['class' => 'inputeffect']) !!}
                   </div>
                   <div class="block">
                       <p class="subtitle">Prénom :</p>
                       {!! Form::text('prenom', null, ['class' => 'inputeffect']) !!}
                   </div>
                   <div class="block">
                       <p class="subtitle">Lien avec la personne :</p>
                       {!! Form::text('lien_personne', null, ['class' => 'inputeffect']) !!}
                   </div>
                   <div class="block">
                       <p class="subtitle">Téléphone :</p>
                       {!! Form::text('telephone', null, ['class' => 'inputeffect']) !!}
                   </div>
                   <div class="block">
                       <p class="subtitle">Email :</p>
                       {!! Form::text('email', null, ['class' => 'inputeffect']) !!}
                   </div>
               </div>
           </div>
            <div class="col-xs-12">
                <p class="title-small">Informations sur le senior</p>
                <div class="box">
                    <div class="block">
                        <p class="subtitle">Nom & prénom :</p>
                        {!! Form::text('name_firstname', null, ['class' => 'inputeffect']) !!}
                    </div>

                    <div class="block">
                        <p class="subtitle">Commentaires de l'agent :</p>
                        {!! Form::textarea('commentaire_agent', null, ['class' => 'inputeffect']) !!}
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <p class="title-small">Etablissement recherché</p>
                <div class="box">
                    <div id="secteurs" class="block">
                        <p class="subtitle">Secteur géographique :</p>

                        @foreach ($demandeur->secteurs as $secteur )
                            {!! Form::text('secteur[]', $secteur->name, ['class' => 'inputeffect']) !!}
                        @endforeach
                    </div>
                    <div class="block">
                        <p class="subtitle">Pathologies prises en charge :</p>
                        {!! Form::text('patologies', null, ['class' => 'inputeffect']) !!}
                    </div>
                    <div class="block">
                        <p class="subtitle">Niveau de dépendance :</p>
                        {!! Form::select('dependances', ['' => '', 'GIR 1' => 'GIR 1', 'GIR 2' => 'GIR 2', 'GIR 3' => 'GIR 3', 'Autres' => 'Autres'], $demandeur->dependances, ['class' => 'inputeffect'] ) !!}
                    </div>
                    <div class="block">
                        <p class="subtitle">Commentaires annexe :</p>
                        {!! Form::textarea('commentaire_annexe', null, ['class' => 'inputeffect']) !!}
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="buttons">
                    <button type="submit" id="submit-document">Enregistrer et étape suivante</button>

                    <!--<a href="{{ asset('admin/fiche/'.$demandeur->id.'/onglet-'.$next.'') }}">
                        Etape suivante
                    </a>-->
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
