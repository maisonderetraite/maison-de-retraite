@extends('layouts.onglet')
@section('content')
<div id="form">
    <p class="title">ETAT CIVIL REPRESENTANT LEGAL</p>
    {!! Form::model($demandeur, ['url' => action("Admin\AdminController@update", [$demandeur,2]), 'method' =>"Put"]) !!}
    <div class="row">
        <div class="col-xs-12 col-md-4">
            <div class="box">
                <p class="subtitle">Civilité :</p>
                <ul class="radio">
                    <li>
                        {{ Form::radio('etat_civil_representant_civilite', 'Monsieur', '') }}
                        <label for="Monsieur">Monsieur</label>
                    </li>
                    <li>
                        {{ Form::radio('etat_civil_representant_civilite', 'Madame', '') }}
                        <label for="Madame">Madame</label>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="box">
                <p class="subtitle">NOM de naissance (suivi, s’il y a lieu par le nom d’usage)</p>
                {!! Form::text('etat_civil_representant_nom_de_naissance', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="box">
                <p class="subtitle">Prénom(s)</p>
                {!! Form::text('etat_civil_representant_prenoms', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>
        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Date de naissance</p>
                {!! Form::text('etat_civil_representant_date_de_naissance', null, ['class' => 'datedemande']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Lieu de naissance</p>
                {!! Form::text('etat_civil_representant_lieu_de_naissance', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Pays ou département</p>
                {!! Form::text('etat_civil_representant_pays_ou_departement', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">N° Voie, rue, boulevard</p>
                {!! Form::text('etat_civil_representant_adresse_rue', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Code postal</p>
                {!! Form::text('etat_civil_representant_adresse_cp', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Commune/Ville</p>
                {!! Form::text('etat_civil_representant_adresse_commune_ville', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Téléphone fixe</p>
                {!! Form::text('etat_civil_representant_telephone_fixe', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Téléphone portable</p>
                {!! Form::text('etat_civil_representant_telephone_portable', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>
        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Adresse email</p>
                {!! Form::text('etat_civil_representant_email', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12">
            <div class="buttons">
                <button type="submit" id="submit-document">Enregistrer et étape suivante</button>

            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection
