@extends('layouts.onglet')
@section('content')
<div id="form">
    <p class="title">PERSONNE DE CONFIANCE</p>
    {!! Form::model($demandeur, ['url' => action("Admin\AdminController@update", [$demandeur,3]), 'method' =>"Put"]) !!}
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Une personne de confiance a-t-elle été désignée par la personne concernée : </p>
                <ul class="radio">
                    <li>
                        {{ Form::radio('personne_confiance_designation', 'Oui', '') }}
                        <label for="oui">Oui</label>
                    </li>
                    <li>
                        {{ Form::radio('personne_confiance_designation', 'Non', '') }}
                        <label for="non">Non</label>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="box">
                <p class="condition">SI OUI :</p>
                <p class="subtitle">NOM de naissance (suivi, s’il y a lieu par le nom d’usage)</p>
                {!! Form::text('personne_confiance_nom_de_naissance', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>
        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Prénom(s)</p>
                {!! Form::text('personne_confiance_prenoms', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>


        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">N° Voie, rue, boulevard</p>
                {!! Form::text('personne_confiance_adresse_rue', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Code postal</p>
                {!! Form::text('personne_confiance_adresse_cp', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Commune/Ville</p>
                {!! Form::text('personne_confiance_adresse_commune_ville', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Téléphone fixe</p>
                {!! Form::text('personne_confiance_telephone_fixe', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Téléphone portable</p>
                {!! Form::text('personne_confiance_telephone_portable', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>
        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Adresse email</p>
                {!! Form::text('personne_confiance_email', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Lien de parenté ou de relation avec la personne concernée</p>
                {!! Form::text('personne_confiance_lien_parente', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12">
            <div class="buttons">
                <button type="submit" id="submit-document">Enregistrer et étape suivante</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection
