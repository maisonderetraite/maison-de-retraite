@extends('layouts.onglet')
@section('content')
<div id="form">

    @if( $demandeur->maisonderetraite->count() >0  )

        <div id="listemaisonderetraite">
            <table class="tableuser">
                <th>Voeux</th>
                <th>Nom</th>
                <th>Adresse</th>
                <th>Code postal</th>
                <th>Ville</th>
                <th>Email</th>
                <th>Actions</th>
                <th>Choix final</th>
                <?php  $i = 1 ;?>
                @foreach ($demandeur->maisonderetraite as $maisonderetraite )

                    @if ( $maisonderetraite->choix == 1 )
                    <tr class="choisie">
                        <td>{{ $i }}</td>
                        <td>{{ $maisonderetraite->nom }}</td>
                        <td>{{ $maisonderetraite->adresse }}</td>
                        <td>{{ $maisonderetraite->cp }}</td>
                        <td>{{ $maisonderetraite->ville }}</td>
                        <td>{{ $maisonderetraite->email }}</td>
                        <td>
                            {!! Form::open(array('url' => '/admin/fiche/'.$demandeur->id .'/maisonderetraite/suppression/'. $maisonderetraite->id .'', 'class' => 'formdelete')) !!}
                            {!! Form::hidden('_method', 'delete') !!}
                            <button class="delete alert alert-danger">
                                Supprimer
                            </button>
                            {!! Form::close() !!}
                        </td>
                        <td>

                            {!! Form::open(array('url' => '/admin/fiche/'.$demandeur->id .'/maisonderetraite/choix/'. $maisonderetraite->id .'')) !!}
                            {!! Form::hidden('_method', 'put') !!}
                            <button class="alert alert-success">
                                Choix final
                            </button>
                            {!! Form::close() !!}

                        </td>
                    </tr>


                    @else
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $maisonderetraite->nom }}</td>
                        <td>{{ $maisonderetraite->adresse }}</td>
                        <td>{{ $maisonderetraite->cp }}</td>
                        <td>{{ $maisonderetraite->ville }}</td>
                        <td>{{ $maisonderetraite->email }}</td>
                        <td>
                            {!! Form::open(array('url' => '/admin/fiche/'.$demandeur->id .'/maisonderetraite/suppression/'. $maisonderetraite->id .'', 'class' => 'formdelete')) !!}
                            {!! Form::hidden('_method', 'delete') !!}
                            <button class="delete alert alert-danger">
                                Supprimer
                            </button>
                            {!! Form::close() !!}
                        </td>
                        <td>
                            {!! Form::open(array('url' => '/admin/fiche/'.$demandeur->id .'/maisonderetraite/choix/'. $maisonderetraite->id .'')) !!}
                            {!! Form::hidden('_method', 'put') !!}
                            <button class="alert alert-success">
                                Choisir
                            </button>
                            {!! Form::close() !!}
                        </td>
                    </tr>

                    @endif
                    <?php $i++;?>
                @endforeach
            </table>
        </div>

    @endif

    <p class="title">Ajouter une maison de retraite</p>
    {!! Form::model($maisonderetraite, ['url' => action("MaisonDeRetraiteController@store", [$demandeur->id, $maisonderetraite ]), 'method' =>"Post"]) !!}
    <div class="row">

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Nom de la maison de retraite</p>
                <input type="text" name="nom" class="inputeffect">
            </div>
        </div>

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Adresse</p>
                <textarea name="adresse"  rows="10" class="inputeffect" style="height:100px;"></textarea>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Code postale</p>
                <input type="text" name="cp" class="inputeffect">
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="box">
                <p class="subtitle">Ville</p>
                <input type="text" name="ville" class="inputeffect">
            </div>
        </div>

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Email</p>
                <input type="email" name="email" class="inputeffect">
            </div>
        </div>

        <div class="col-xs-12">
            <div class="buttons">
                <button type="submit" id="submit-document">Ajouter une maison de retraite</button>
                <a href="{{ asset('admin/fiche/'.$demandeur->id.'/onglet-'.$next.'') }}">
                    Etape suivante
                </a>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection
