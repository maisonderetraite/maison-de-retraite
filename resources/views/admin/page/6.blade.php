@extends('layouts.onglet')
@section('content')
<div id="form">
    <p class="title">ASPECTS FINANCIERS</p>
    {!! Form::model($demandeur, ['url' => action("Admin\AdminController@update", [$demandeur,6]), 'method' =>"Put"]) !!}
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Comment la personne concernée pense-t-elle financer ses frais de séjour ?</p>
                <ul class="radio">
                    <li>
                        {{ Form::radio('aspects_financiers_comment_frais', 'Seule', '') }}
                        <label for="Seule">Seule</label>
                    </li>
                    <li>
                        {{ Form::radio('aspects_financiers_comment_frais', 'Avec l’aide d’un ou plusieurs tiers', '') }}
                        <label for="Avec l’aide d’un ou plusieurs tiers">Avec l’aide d’un ou plusieurs tiers</label>
                    </li>
                </ul>
            </div>

        </div>

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Aide sociale à l’hébergement</p>
                <ul class="radio">
                    <li>
                        {{ Form::radio('aspects_financiers_aide_social', 'Oui', '') }}
                        <label for="oui">Oui</label>
                    </li>
                    <li>
                        {{ Form::radio('aspects_financiers_aide_social', 'Non', '') }}
                        <label for="non">Non</label>
                    </li>
                    <li>
                        {{ Form::radio('aspects_financiers_aide_social', 'Demande en cours', '') }}
                        <label for="Demande en cours">Demande en cours</label>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Allocation logement (APL/ALS)</p>
                <ul class="radio">
                    <li>
                        {{ Form::radio('aspects_financiers_allocation_logement', 'Oui', '') }}
                        <label for="oui">Oui</label>
                    </li>
                    <li>
                        {{ Form::radio('aspects_financiers_allocation_logement', 'Non', '') }}
                        <label for="non">Non</label>
                    </li>
                    <li>
                        {{ Form::radio('aspects_financiers_allocation_logement', 'Demande en cours', '') }}
                        <label for="Demande en cours">Demande en cours</label>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Allocation personnalisée à l’autonomie</p>
                <ul class="radio">
                    <li>
                        {{ Form::radio('aspects_financiers_allocation_personnalise', 'Oui', '') }}
                        <label for="oui">Oui</label>
                    </li>
                    <li>
                        {{ Form::radio('aspects_financiers_allocation_personnalise', 'Non', '') }}
                        <label for="non">Non</label>
                    </li>
                    <li>
                        {{ Form::radio('aspects_financiers_allocation_personnalise', 'Demande en cours', '') }}
                        <label for="Demande en cours">Demande en cours</label>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">Prestation de compensation du handicap/Allocation compensatrice pour tierce personne</p>
                <ul class="radio">
                    <li>
                        {{ Form::radio('aspects_financiers_prestation_compensation', 'Oui', '') }}
                        <label for="oui">Oui</label>
                    </li>
                    <li>
                        {{ Form::radio('aspects_financiers_prestation_compensation', 'Non', '') }}
                        <label for="non">Non</label>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 ">
            <div class="box">
                <p class="subtitle">Avez-vous une estimation du budget mensuel ?</p>

                {!! Form::select('estimation_budget_commentaire', ['1500 - 2000 €' => '1500 - 2000 €', '2000 - 3000 €' => '2000 - 3000 €', '+ 3000 €' => '+ 3000 €'], $demandeur->estimation_budget_commentaire, ['class' => 'inputeffect'] ) !!}

            </div>
        </div>
        <div class="col-xs-12 ">
            <div class="box">
                <p class="subtitle">Commentaires :</p>
                {!! Form::textarea('aspects_financiers_commentaire', null, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12">
            <div class="buttons">
                <button type="submit" id="submit-document">Enregistrer et étape suivante</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

</div>
@endsection
