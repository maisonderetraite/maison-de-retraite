@extends('layouts.onglet')
@section('content')
<div id="form">
    <p class="title">Commentaires</p>
    <div class="row">
    {!! Form::model($demandeur, ['url' => action("Admin\AdminController@updatecommentaire", [$demandeur,8] ), 'method' =>"Put"]) !!}
        <div id="commentaire-bloc" class="col-xs-12">
                @if ($demandeur->commentaires)
                    @foreach ($demandeur->commentaires()->orderBy('id', 'desc')->get() as $commentaire)
                    <div class="comments">
                        <p>{{ $commentaire->commentaire }}</p>
                        <p class="comment-date">{{ $commentaire->created_at }}</p>
                    </div>

                    @endforeach
                 @endif
                <div id="commentaire-list">
                    <div>
                        <div class="input commentaire">
                            {!! Form::textarea('commentaire', null, ['class' => 'inputeffect']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="buttons">
            <button type="submit" id="submit-document">Ajouter un commentaire</button>
            <a href="{{ asset('admin/fiche/'.$demandeur->id.'/onglet-'.$next.'') }}">
                Etape suivante
            </a>
        </div>

    {!! Form::close() !!}
</div>
@endsection
