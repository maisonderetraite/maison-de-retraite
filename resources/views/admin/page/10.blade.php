@extends('layouts.onglet')
@section('content')
<div id="form">
    <p class="title">CLOTURE DU DOSSIER</p>
    <div id="download-view">
        <div class="row">
            <div class="buttons col-xs-12 col-md-6">
                <p><a id="submit-document" target="_blank" href="{{ asset('admin/fiche/'.$demandeur->id.'/pdf') }}">Aperçu CERFA ( PDF )</a></p>
                <p><a href="{{ asset('admin/fiche/'.$demandeur->id.'/download') }}">Télécharger le CERFA ( PDF )</a></p>
            </div>
            <!--
            <div class="col-xs-12 col-md-6">
                <div class="box">
                    <p class="subtitle">Envoyer par email</p>
                    {!! Form::model($demandeur, ['url' => action("Admin\AdminController@sendemail",$demandeur), 'method' => "Post"]) !!}
                    {!! Form::text('email', null, ['class' => 'inputeffect']) !!}
                    <div class="buttons" style="margin-top:15px;">
                        <button type="submit" id="submit-document">Envoyer</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

            -->
        </div>

    </div>
</div>
@endsection
