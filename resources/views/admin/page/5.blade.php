@extends('layouts.onglet')
@section('content')
    <div id="form">
        <div class="col-xs-12 col-md-10 col-md-offset-1">
            <p class="title">Demande</p>
            {!! Form::model($demandeur, ['url' => action("Admin\AdminController@update", [$demandeur,5]), 'method' =>"Put"]) !!}
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="box">
                            <p class="subtitle">Type d’hébergement/accompagnement recherché :</p>
                            <ul class="radio">
                                <li>
                                    {{ Form::radio('demande_type_hebergement', 'Hébergement permanent', '') }}
                                    <label for="urgente">Hébérgement permanent</label>
                                </li>
                                <li>
                                    {{ Form::radio('demande_type_hebergement', 'Hébergement temporaire', '') }}
                                    <label for="1-3mois">Hébérgement temporaire</label>
                                </li>
                            </ul>
                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <div class="box">
                            <p class="subtitle">Accueil couple souhaité</p>
                            <ul class="radio">
                                <li>
                                    {{ Form::radio('demande_accompagnement', 'Oui', '') }}
                                    <label for="oui">Oui</label>
                                </li>
                                <li>
                                    {{ Form::radio('demande_accompagnement', 'Non', '') }}
                                    <label for="non">Non</label>
                                </li>
                            </ul>
                        </div>
                        <div class="box">
                            <p class="subtitle">Nom et prénom du conjoint</p>
                            {!! Form::text('link_other_fiche', null, [ 'id' => 'lien_demande','class' => 'inputeffect']) !!}


                        </div>

                    </div>



                    <div class="col-xs-12">
                        <div class="box">
                            <p class="subtitle">Durée du séjour pour l’hébergement temporaire</p>
                            {!! Form::text('demande_duree_sejour_temporaire', null, ['class' => 'inputeffect']) !!}
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="box">
                            <p class="subtitle">Situation de la personne concernée à la date de la demande :</p>
                            <ul class="radio">
                                <li>
                                    {{ Form::radio('demande_situation', 'Domicile', '') }}
                                    <label for="Domicile">Domicile</label>
                                </li>
                                <li>
                                    {{ Form::radio('demande_situation', 'Chez enfant/proche', '') }}
                                    <label for="Chez enfant/proche">Chez enfant/proche</label>
                                </li>
                                <li>
                                    {{ Form::radio('demande_situation', 'Logement foyer', '') }}
                                    <label for="Logement foyer">Logement foyer</label>
                                </li>
                                <li>
                                    {{ Form::radio('demande_situation', 'EHPAD', '') }}
                                    <label for="ehpad">EHPAD</label>
                                </li>
                                <li>
                                    {{ Form::radio('demande_situation', 'Hôpital', '') }}
                                    <label for="Hôpital">Hôpital</label>
                                </li>
                                <li>
                                    {{ Form::radio('demande_situation', 'SSIAD/SAD', '') }}
                                    <label for="SSIAD/SAD">SSIAD/SAD</label>
                                </li>
                                <li>
                                    {{ Form::radio('demande_situation', 'Accueil de jour', '') }}
                                    <label for="Accueil de jour">Accueil de jour</label>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="box">
                            <p class="subtitle">Autres :</p>
                            {!! Form::textarea('demande_autres', null, ['class' => 'inputeffect']) !!}
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="box">
                            <p class="subtitle">Dans tous les cas préciser le nom  de l’établissement ou du service</p>
                            {!! Form::textarea('demande_nom_etablissement_service', null, ['class' => 'inputeffect']) !!}
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="box">
                            <p class="subtitle">La personne concernée est-elle informée de la demande ?</p>
                            <ul class="radio">
                                <li>
                                    {{ Form::radio('demande_personne_informe', 'Oui', '') }}
                                    <label for="oui">Oui</label>
                                </li>
                                <li>
                                    {{ Form::radio('demande_personne_informe', 'Non', '') }}
                                    <label for="non">Non</label>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="box">
                            <p class="subtitle">La personne concernée est-elle consentante (à la demande) ?</p>
                            <ul class="radio">
                                <li>
                                    {{ Form::radio('demande_personne_consentante', 'Oui', '') }}
                                    <label for="oui">Oui</label>
                                </li>
                                <li>
                                    {{ Form::radio('demande_personne_consentante', 'Non', '') }}
                                    <label for="non">Non</label>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="box">
                            <p class="subtitle">Dans le cas où la personne concernée ne remplit pas elle-même le document, le consentement éclairé n’a pu être recueilli</p>
                            <ul class="radio">
                                <li>
                                    {{ Form::checkbox('demande_consentement_recueilli', true) }}
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="buttons">
                            <button type="submit" id="submit-document">Enregistrer et étape suivante</button>
                        </div>
                    </div>

                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
