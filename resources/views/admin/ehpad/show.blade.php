@extends('layouts.admin')
@section('content')

<div id="form">
    <div class="col-xs-12">
        {!! Form::model($ehpad, ['url' => action("EhpadController@update", $ehpad->ehpad_id), 'method' =>"Put"]) !!}
        <p class="title">{{ $ehpad->name }}</p>
        <div class="row">
            <div id="ehpad__on-off" class="col-xs-12">
                <div class="box">
                    <div class="switch-field">
                        {{ Form::radio('on_off', 'ON','',[ 'id' => 'switch_left']  ) }}
                        <label for="switch_left">ON</label>
                        {{ Form::radio('on_off', 'OFF', '', [ 'id' => 'switch_right']) }}
                        <label for="switch_right">OFF</label>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <p class="title-small">Fiche de l'établissement</p>
            </div>
            <div class="col-xs-12 col-sm-8">
                <div class="box">
                    <div class="block">
                        <p class="subtitle">Nom de l'établissement : </p>
                        <p class="infos">{{ $ehpad->name}}</p>
                    </div>
                    <div class="block">
                        <p class="subtitle">Adresse : </p>

                        <p class="infos">
                            {!! Form::text('address', $ehpad->address, ['class' => 'inputeffect']) !!}<br/><br/>
                            {!! Form::text('postal_code', $ehpad->postal_code, ['class' => 'inputeffect']) !!}<br/><br/>
                            {!! Form::text('city', $ehpad->city, ['class' => 'inputeffect']) !!}<br/><br/>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="box">
                    <p class="subtitle">Email : </p>
                    {!! Form::textarea('email', $ehpad->email, ['class' => 'inputeffect']) !!}
                </div>
            </div>
            <div class="clear"></div>
            <div class="col-xs-4">
                <div class="box">
                    <p class="subtitle">Téléphone: </p>

                    {!! Form::text('phone', $ehpad->phone, ['class' => 'inputeffect']) !!}
                </div>
            </div>
             <div class="col-xs-4">
                <div class="box">
                    <p class="subtitle">Site internet: </p>
                    {!! Form::text('website', $ehpad->website, ['class' => 'inputeffect']) !!}

                </div>
            </div>
            <div class="clear"></div>
            <div class="col-xs-4">
                <div class="box">
                    <p class="subtitle">Type hébergement: </p>
                    {!! Form::text('housing_type',  $ehpad->housing_type, ['class' => 'inputeffect']) !!}
                </div>
            </div>
            <div class="col-xs-4">
                <div class="box">
                    <p class="subtitle">Capacités: </p>
                    {!! Form::text('capacity',  $ehpad->capacity, ['class' => 'inputeffect']) !!}
                </div>
            </div>
            <div class="col-xs-4">
                <div class="box">
                    <p class="subtitle">Aides publiques: </p>
                    {!! Form::text('public_grant',  $ehpad->public_grant, ['class' => 'inputeffect']) !!}
                </div>
            </div>
            <div class="col-xs-4">
                <div class="box">
                    <p class="subtitle">Pathologies traitées: </p>
                    {!! Form::textarea('specific_accompaniment',  $ehpad->specific_accompaniment, ['class' => 'inputeffect']) !!}
                </div>
            </div>
            <div class="col-xs-4">
                <div class="box">
                    <p class="subtitle">Tarifs: </p>
                    {!! Form::textarea('other_pricing',  $ehpad->pricing, ['class' => 'inputeffect']) !!}
                </div>
            </div>
            <div class="col-xs-4">
                <div class="box">
                    <p class="subtitle">Autres tarifs: </p>
                    {!! Form::textarea('other_pricing',  $ehpad->other_pricing, ['class' => 'inputeffect']) !!}
                </div>
            </div>

            <div class="col-xs-4">
                <div class="box">
                    <p class="subtitle">Disponibilités: </p>
                    {!! Form::textarea('disponibilites',  $ehpad->disponibilites, ['class' => 'inputeffect']) !!}
                </div>
            </div>
            <div class="col-xs-12">
                <p class="title-small">Informations sur le décisionnaire</p>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="box">
                    <p class="subtitle">Civilité du décisionnaire:</p>
                    <ul class="radio">
                        <li>
                            {{ Form::radio('civilite', 'Monsieur', '') }}
                            <label for="Monsieur">Monsieur</label>
                        </li>
                        <li>
                            {{ Form::radio('civilite', 'Madame', '') }}
                            <label for="Madame">Madame</label>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4">
                <div class="box">
                    <p class="subtitle">NOM du décisionnaire</p>
                    {!! Form::text('nom', null, ['class' => 'inputeffect']) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="box">
                    <p class="subtitle">Prénom du décisionnaire</p>
                    {!! Form::text('prenom', null, ['class' => 'inputeffect']) !!}
                </div>
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <p class="subtitle">Poste du décisionnaire</p>
                    {!! Form::text('poste', null, ['class' => 'inputeffect']) !!}
                </div>
            </div>
            <div class="col-xs-12">
                <div class="buttons">
                    <button type="submit" id="submit-document">Mettre à jour</button>
                </div>
            </div>
            {!! Form::close() !!}

            <div class="col-xs-12">
                <div id="relancebloc" class="box">
                    <p class="subtitle">Etat de relance : </p>
                    <table class="tableuser">
                        <thead>
                        <th>
                            Date
                        </th>
                        <th>
                            Relance
                        </th>
                        <th>
                            Objet
                        </th>
                        <th>
                            Commentaire
                        </th>
                        <th>
                            Par ( email du commercial )
                        </th>
                        @if($user->roles == "administrateur")
                        <th>Supprimer</th>
                        @endif
                        </thead>
                        <tbody>
                        @if ($ehpad->commentaires)
                        @foreach ($ehpad->commentaires()->orderBy('id', 'desc')->get() as $commentaire)
                        <tr>
                            <td class="comment-date">{{ $commentaire->created_at }}</td>
                            <td>{{ $i }}</td>
                            <td>{{ $commentaire->etat }}</td>
                            <td>{{ $commentaire->commentaire }}</td>

                            <td class="comment-email">{{ $commentaire->users->email }}</td>
                            @if($user->roles == "administrateur")
                            <td class="comment-delete">
                                {!! Form::open(array('url' => '/admin/ehpad/'.$commentaire->id .'/delete', 'class' => 'formdelete')) !!}
                                {!! Form::hidden('_method', 'delete') !!}
                                <button class="delete alert alert-danger">
                                    X
                                </button>
                                {!! Form::close() !!}
                            </td>
                            @endif
                        </tr>
                        <?php $i++;?>
                        @endforeach
                        </tbody>
                        @endif
                    </table>
                    {!! Form::model($ehpad, ['url' => action("EhpadController@updatecomment", $ehpad), 'method' =>"Put"]) !!}
                    <div id="commentaire-bloc">
                        <p class="subtitle"">Ajouter une relance</p>
                        <div class="input etat">
                            <label for="etat">Objet :</label>
                            {!! Form::text('etat', null, ['class' => 'inputeffect']) !!}
                        </div>
                        <div class="input commentaire">
                            <label for="commentaire">Commentaire : </label>
                            {!! Form::textarea('commentaire', null, ['class' => 'inputeffect']) !!}
                        </div>
                        <div class="buttons">
                            <button type="submit" id="submit-document">Enregistrer</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
