@extends('layouts.admin')
@section('content')

<div id="list-ehpad">
    <form id="ehpad-search" action="{{ url('admin/ehpad/recherche') }}" method="get">
        <input  type="text"  name="ehpad" placeholder="Rechercher un etablissement par département ou par ville"/>
        <input  type="text"  name="ehpad_name" placeholder="Rechercher par nom"/>
        <div class="submit">
            <button id="submit-member" type="submit">
                Rechercher
            </button>
        </div>
    {!! Form::close() !!}




    <div class="paginate">{{ $ehpads->links() }}</div>
    <table class="tableuser">
        <thead>
        <th>key</th>
        <th>Nom</th>
        <th>Code postale</th>
        <th>Ville</th>
        <th>Actions</th>
        <th>Etat</th>
        </thead>
        <tbody>

        @foreach($ehpads as $key => $ehpad)

        <tr>
            <td>{{$ehpad->ehpad_id}}</td>
            <td>{{$ehpad->name}}</td>
            <td>{{$ehpad->postal_code}}</td>
            <td>{{$ehpad->city}}</td>
            <td>
                <a class="alert alert-success" href="{{ action('EhpadController@show', $ehpad->ehpad_id ) }}">Accéder</a>
            </td>
            <td>
                @if ($ehpad->on_off == "ON" )
                <span  class="alert alert-success">{{ $ehpad->on_off }}</span>
                @else
                <span  class="alert alert-danger">Off</span>
                @endif
            </td>

        </tr>
        @endforeach
        </tbody>
    </table>
    <div class="paginate">{{ $ehpads->links() }}</div>
</div>
@endsection
