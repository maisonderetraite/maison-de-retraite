@extends('layouts.admin')
@section('content')

<div id="list-ehpad">
    <form id="ehpad-search" action="{{ url('admin/ehpad/recherche') }}" method="get">
        <input  type="text"  name="ehpad" placeholder="Rechercher un etablissement par département ou par ville"/>
        <input  type="text"  name="ehpad_name" placeholder="Rechercher par nom"/>
        <div class="submit">
            <button id="submit-member" type="submit">
                Rechercher
            </button>
        </div>
    {!! Form::close() !!}



    <p id="title">Il y a {{ $results->total() }} résultats correspondant à votre recherche </p>
    <div class="paginate">{{ $results->appends(Request::only('ehpad'))->links()}}</div>
    <table class="tableuser">
        <thead>
        <th>key</th>
        <th>Nom</th>
        <th>Code postale</th>
        <th>Ville</th>
        <th>Actions</th>
        </thead>
        <tbody>
        @foreach($results as $key => $ehpad)
        <tr>
            <td>{{$ehpad->ehpad_id}}</td>
            <td>{{$ehpad->name}}</td>
            <td>{{$ehpad->postal_code}}</td>
            <td>{{$ehpad->city}}</td>
            <td>
                <a class="alert alert-success" href="{{ action('EhpadController@show', $ehpad->ehpad_id ) }}">Accéder</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    <div class="paginate">{{ $results->appends(Request::only('ehpad'))->links()}}</div>
</div>
@endsection
