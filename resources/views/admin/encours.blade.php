@extends('layouts.admin')
@section('content')
<div id="list-demandeurs">
    <table class="table-fill">
        <thead>
        <th>Id</th>
        <th>Statut</th>
        <th>Nom commercial</th>
        <th>Nom / Prénom</th>
        <th>Lien</th>
        <th>Email /Téléphone</th>
        <th>Secteurs</th>
        <th>Période</th>
        <th>Date Modification</th>
        <th>Actions</th>
        </thead>
        <tbody>
        @foreach($demandeurs as $demandeur)
        <tr>
            <td>{{$demandeur->id}}</td>
            <td>{{$demandeur->statut}}</td>
            <td> @if ( $demandeur->users)
                {{$demandeur->users->name}}
                @endif
            </td>
            <td>{{$demandeur->nom}}<br/>{{$demandeur->prenom}}</td>
            <td>{{$demandeur->lien_personne}}</td>
            <td>{{$demandeur->email}}<br/>{{$demandeur->telephone}}</td>
            <td>
                @foreach ($demandeur->secteurs as $secteur )
                {{$secteur->name}}<br/>
                @endforeach
            </td>
            <td>{{$demandeur->periode_souhaite}}</td>
            <td>{{$demandeur->updated_at}}</td>
            <td>
                {{ HTML::linkAction('Admin\AdminController@show','editer',[$demandeur->id , 0], ['class' => 'alert alert-success' ]) }}
                @if($user->roles == "administrateur")
                    {!! Form::open(array('url' => '/admin/'.$demandeur->id .'', 'class' => 'formdelete')) !!}
                    {!! Form::hidden('_method', 'delete') !!}
                    <button class="delete alert alert-danger">
                        Supprimer
                    </button>
                    {!! Form::close() !!}
                @endif
            </td>
        </tr>
        @endforeach

        </tbody>
    </table>
</div>
@endsection
