@extends('layouts.admin')
@section('content')

<div id="faq" class="liste-faq">
    <p id="title">FAQ</p>
    @if($user->roles == "administrateur")
    <p id="createfaq">
        <a href="{{ url('/admin/faq/creation') }}">Ajouter une question</a>
    </p>
    @endif
    <p id="allquestion">
        <a href="{{ url('/admin/faq') }}">Toutes les questions</a>
    </p>
    <form id="faq-search" action="{{ url('admin/faq/search') }}" method="get">
        <input  type="text"  name="faq" placeholder="Rechercher une question"/>
        <div class="submit">
            <button id="submit-member" type="submit">
                Rechercher
            </button>
        </div>
        {!! Form::close() !!}
    @foreach ( $results as $faq )
    <div class="categories">
        <div class="faq-box closed">
            <p class="questions">{{ $faq->question }}</p>
            <p class="reponses">    {!! $faq->reponse !!}</p>
        </div>
    </div>
    @endforeach
</div>
@endsection
