@extends('layouts.admin')
@section('content')

<div id="faq">
    <div id="form" class="create-faq">
    <p id="title">Suggérer une question</p>
    {!! Form::model($faq, ['url' => action("FaqController@store", $faq), 'method' =>"Post"]) !!}
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <p class="subtitle">question : </p>
                {!! Form::text('question',  $faq->question, ['class' => 'inputeffect']) !!}
            </div>
        </div>

        <div class="col-xs-12">
            <div class="buttons">
                <button type="submit" id="submit-document">Enregistrer</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
