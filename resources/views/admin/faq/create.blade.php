@extends('layouts.admin')
@section('content')
<div id="faq">
    <div id="form" class="create-faq">
        {!! Form::model($faq, ['url' => action("FaqController@store", $faq), 'method' =>"Post"]) !!}
        <p class="title">Création de faq</p>
        <div class="row">

            <div class="col-xs-12">
                <div class="box">
                    <p class="subtitle">question : </p>
                    {!! Form::text('question',  $faq->question, ['class' => 'inputeffect']) !!}
                </div>
            </div>

            <div class="col-xs-12">
                <div class="box">
                    <p class="subtitle">question : </p>
                    {!! Form::textarea('reponse',  $faq->reponse, ['class' => 'inputeffect']) !!}
                </div>
            </div>

            <div class="col-xs-12">
                <div id="categories-faq" class="box">
                    <p class="subtitle">question : </p>
                    <select name="categorie_id"  class="inputeffect">
                        @foreach ( $categories as $category )
                        <option value=" {{ $category->id }}" >{{ $category->name }}</option>
                        @endforeach
                    </select>

                </div>
            </div>

            <div class="col-xs-12">
                <div class="buttons">
                    <button type="submit" id="submit-document">Enregistrer</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
