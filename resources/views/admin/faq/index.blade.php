@extends('layouts.admin')
@section('content')

<div id="faq" class="liste-faq">
    <p id="title">FAQ</p>
    @if($user->roles != "administrateur")
    <p id="createfaq">
        <a href="{{ url('/admin/faq/creation') }}">Ajouter une question</a>
    </p>
    @endif
    <form id="faq-search" action="{{ url('admin/faq/search') }}" method="get">
        <input  type="text"  name="faq" placeholder="Rechercher une question"/>
        <div class="submit">
            <button id="submit-member" type="submit">
               Rechercher
            </button>
        </div>
    {!! Form::close() !!}

    @foreach ( $categories as $i )
        <div class="categories">
            <p class="categorie-name">{{$i->name }}</p>
            @foreach ($i->faq as $faq )
            <div class="faq-box closed">
                <div class="questions">
                    {{ $faq->question }}

                    @if($user->roles == "administrateur")
                    {!! Form::open(array('url' => '/admin/faq/'.$faq->id .'/delete', 'class' => 'formdelete')) !!}
                    {!! Form::hidden('_method', 'delete') !!}
                    <button class="delete alert alert-danger">
                        X
                    </button>
                    {!! Form::close() !!}
                    @endif
                </div>
                <p class="reponses">    {!! $faq->reponse !!}</p>
            </div>
            @endforeach
        </div>
    @endforeach

    @if($user->roles == "administrateur")
    <div id="questionsuggere">
        <p id="questionsuggere__title">Question à ajouter</p>
        <table class="tableuser">
            <thead>
                <th>Question</th>
            <th>Actions</th>
            </thead>

        @foreach ( $faqs as $faq )
            <tr>
                <td>{!! $faq->question !!}</td>
                <td>

                    {!! Form::close() !!}
                    {{ HTML::linkAction('FaqController@show','editer',[$faq], ['class' => 'alert alert-success' ]) }}

                    {!! Form::open(array('url' => '/admin/faq/'.$faq->id .'/delete', 'class' => 'formdelete')) !!}
                    {!! Form::hidden('_method', 'delete') !!}
                    <button class="delete alert alert-danger">
                        Supprimer
                    </button>
                    {!! Form::close() !!}

                </td>
            </tr>

        @endforeach
        </table>
    </div>
    @endif
</div>
@endsection
