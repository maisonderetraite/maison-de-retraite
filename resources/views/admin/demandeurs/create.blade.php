@extends('layouts.admin')
@section('content')

<div id="form" class="create-fiche">
    <p class="title">Création d'un nouveau utilisateur</p>
    {!! Form::model($demandeurs, ['url' => action("Admin\DemandeController@store"), 'method' => "Post" ,'id' =>'fiche-form']) !!}
    <div class="etape-one">
        <p class="title-small">Critère de recherche</p>
        <div class="row">
            <div id="name-firstname-bloc" class="col-xs-12 col-md-6">
                <div class="box">
                    <p class="subtitle">Nom & prénom de la personne âgée :</p>
                    <p class="errortext"></p>
                    {!! Form::text('name_firstname', null, ['class' => 'inputeffect']) !!}
                </div>
            </div>
            <div id="age-bloc" class="col-xs-12 col-md-6">
                <div class="box">
                    <p class="subtitle">Âge :</p>
                    <p class="errortext"></p>
                    {!! Form::number('age', '60', ['class' => 'inputeffect', 'min' =>'60','max' =>'130','id'=> 'agenumber']) !!}
                </div>
            </div>
            <div class="clear"></div>
            <div id="lien-bloc" class="col-xs-12 col-md-6">
                <div class="box">
                    <p class="subtitle">Lien avec la personne concernée :</p>
                    <p class="errortext"></p>
                    {!! Form::select('lien_personne', array('Moi-même' => 'Moi-même', 'Famille' => 'Famille' , 'Ami' => 'Ami', 'Médecin' => 'Médecin', 'Assistante sociale' => 'Assistante sociale','Mandataire judiciaire' => 'Mandataire judiciaire','Autre' => 'Autre'), null, ['class' => 'inputeffect'] ) !!}
                </div>
            </div>
            <div id="periode-bloc" class="col-xs-12 col-md-6">

                <div class="box">
                    <p class="subtitle">Période souhaitée :</p>
                    <p class="errortext"></p>
                    <ul>
                        <li>
                            {{ Form::radio('periode_souhaite', 'urgent', '', ['id' => 'urgent']) }}
                            <label for="urgent">Demande urgente</label>
                            <div class="check"></div>
                        </li>

                        <li>
                            {{ Form::radio('periode_souhaite', '1 à 3 mois', '', ['id' => '1-3mois']) }}
                            <label for="1-3mois">1 à 3 mois</label>

                            <div class="check"><div class="inside"></div></div>
                        </li>

                        <li>
                            {{ Form::radio('periode_souhaite', '+ de 3 mois', '', ['id' => 'plusde3mois']) }}
                            <label for="plusde3mois">+ de 3 mois</label>

                            <div class="check"><div class="inside"></div></div>
                        </li>
                    </ul>
                </div>
            </div>
            <div id="secteur-bloc" class="col-xs-12">
                <div class="box">
                    <p class="subtitle">Secteur géographique :</p>
                    <div id="secteur-list">
                        <div class="r-group">
                            <div id="first-secteur" class="input proposition">
                                <p class="errortext"></p>
                                {!! Form::text('secteur[]', null, [ 'id' => 'proposition_0' , 'data-pattern-id' => 'proposition__++', 'class' => 'secteur inputeffect','placeholder' => 'Ville' ]) !!}
                            </div>
                            <div id="other-proposition" class="input proposition">
                                {!! Form::text('secteur[]', null, [ 'id' => 'proposition_0' , 'data-pattern-id' => 'proposition__++', 'class' => 'secteur inputeffect','placeholder' => 'Ville' ]) !!}
                                <button type="button" class="r-btnRemove">Supprimer ce choix</button>
                            </div>
                        </div>
                        <button type="button" class="r-btnAdd">+ <span>ajouter un autre choix</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="etape-two">
        <p class="title-small">Les coordonnées</p>
        <div class="row">
            <div id="civilite-bloc" class="col-xs-12 col-sm-6">
                <div class="box">
                    <p class="subtitle">Civilité* :</p>
                    <p class="errortext"></p>
                    <ul>
                        <li>
                            {{ Form::radio('civilite', 'Monsieur', '', ['id' => 'monsieur']) }}
                            <label for="Monsieur">Monsieur</label>
                        </li>
                        <li>
                            {{ Form::radio('civilite', 'Madame', '', ['id' => 'madame']) }}
                            <label for="Madame">Madame</label>
                        </li>
                    </ul>
                </div>

            </div>

            <div id="nom-bloc" class="col-xs-12 col-sm-6">
                <div class="box">
                    <p class="subtitle">Nom* :</p>
                    <p class="errortext"></p>
                    {!! Form::text('nom', null, ['class' => 'inputeffect']) !!}
                </div>
            </div>
            <div class="clear"></div>
            <div id="prenom-bloc" class="col-xs-12 col-sm-6">
                <div class="box">
                    <p class="subtitle">Prénom* :</p>
                    <p class="errortext"></p>
                    {!! Form::text('prenom', null, ['class' => 'inputeffect']) !!}
                </div>

            </div>

            <div id="email-bloc" class="col-xs-12 col-sm-6">
                <div class="box">
                    <p class="subtitle">Email* :</p>
                    <p class="errortext"></p>
                    {!! Form::text('email', null, ['class' => 'inputeffect']) !!}
                </div>
            </div>
            <div class="clear"></div>
            <div id="telephone-bloc" class="col-xs-12 col-sm-6">
                <div class="box">
                    <p class="subtitle">Téléphone* :</p>
                    <p class="errortext"></p>
                    {!! Form::text('telephone', null, ['class' => 'inputeffect']) !!}
                </div>
            </div>
        </div>
        <div class="buttons">
            <button id="formulaire-fiche" type="submit"  />
            Créer une fiche
            </button>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection
