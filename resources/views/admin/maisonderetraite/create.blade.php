@extends('layouts.admin')
@section('content')
<div id="faq">
    <div id="form" class="create-faq">
        {!! Form::model($maisonderetraite, ['url' => action("MaisonDeRetraiteController@store", [$demandeur->id, $maisonderetraite ]), 'method' =>"Post"]) !!}
        <p class="title">Ajouter un établissement</p>
        <div class="row">

            <div class="col-xs-12">
                <div class="box">
                    <p class="subtitle">Nom : </p>
                    {!! Form::text('nom', null , ['class' => 'inputeffect']) !!}
                </div>
            </div>

            <div class="col-xs-12">
                <div class="box">
                    <p class="subtitle">Adresse : </p>
                    {!! Form::textarea('adresse',  null, ['class' => 'inputeffect']) !!}
                </div>
            </div>

            <div class="col-xs-12 col-sm-6">
                <div class="box">
                    <p class="subtitle">Code postal : </p>
                    {!! Form::textarea('cp',  null, ['class' => 'inputeffect']) !!}
                </div>
            </div>

            <div class="col-xs-12 col-sm-6">
                <div class="box">
                    <p class="subtitle">Ville : </p>
                    {!! Form::textarea('ville',  null, ['class' => 'inputeffect']) !!}
                </div>
            </div>

            <div class="col-xs-12">
                <div class="box">
                    <p class="subtitle">Email : </p>
                    {!! Form::textarea('email',  null, ['class' => 'inputeffect']) !!}
                </div>
            </div>

            <div class="col-xs-12">
                <div class="buttons">
                    <button type="submit" id="submit-document">Enregistrer</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
