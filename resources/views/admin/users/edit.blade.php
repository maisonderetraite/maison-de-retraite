@extends('layouts.admin')
@section('content')
<div id="form" class="edit-users">
    <p id="backuser">
        <a href="{{ url('/admin/utilisateurs') }}">Retour aux utilisateurs</a>
    </p>
    <p class="title">Création d'un nouveau utilisateur</p>
    {!! Form::model($user, ['url' => action("Admin\UserController@update", $user), 'method' =>"Put"]) !!}
    <div class="box">
        <div class="input"  id="nom_profil">
            <p class="subtitle">
                Votre nom*
            </p>
            <p class="errortext"></p>
            {!! Form::text('name', null, ['class' => 'inputeffect']) !!}

        </div>
    </div>
    <div class="box">
        <div class="input" id="email_profil">
            <p class="subtitle">
                Votre email*
            </p>
            <p class="errortext"></p>
            {!! Form::email('email', null, ['id'=> 'email_profil' , 'class' => 'inputeffect']) !!}
        </div>
    </div>
    <div class="buttons" id="profil-form">
        <button type="submit" class="inputbutton">
            Modifier
        </button>
    </div>
    {!! Form::close() !!}
    <div id="regions-list">
        <ul>
        @foreach ( $user->regions as $region_user )
            <li>
                <p>{{ $region_user->nom }}</p>
                {!! Form::open(array('url' => '/admin/utilisateurs/'.$user->id .'/modifier/regions/'.$region_user->id.'', 'class' => 'formdelete')) !!}
                {!! Form::hidden('_method', 'delete') !!}
                <button class="delete alert alert-danger">
                    Supprimer
                </button>
                {!! Form::close() !!}
            </li>
        @endforeach
        </ul>
    </div>
    {!! Form::model($user, ['url' => action("Admin\UserController@createregions", $user), 'method' =>"Put"]) !!}
    <div class="input" id="regions_profil">
        <label for="email">
        </label><p class="errortext"></p>
        <select name="region"  class="inputeffect">
            @foreach ( $allregions as $region )
                <option value="{{ $region->id}}">{{ $region->nom}}</option>
            @endforeach
        </select>
    </div>
    <div class="buttons" id="profil-form">
        <button type="submit" class="inputbutton">
            Ajouter une région
        </button>
    </div>
    {!! Form::close() !!}

</div>
@endsection
