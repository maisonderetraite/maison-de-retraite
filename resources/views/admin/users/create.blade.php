@extends('layouts.admin')
@section('content')

<div id="form" class="edit-users">
    <p id="backuser">
        <a href="{{ url('/admin/utilisateurs') }}">Retour aux utilisateurs</a>
    </p>
    <p class="title">Création d'un nouveau utilisateur</p>
    {!! Form::open(['action' => 'Auth\RegisterController@register']) !!}
        <div class="row">

            <div id="register_name"  class="col-xs-12 col-sm-6">
                <div class="box">
                    <p class="subtitle">Votre nom*</p>
                    {!! Form::text('name', null, ['class' => 'inputeffect']) !!}
                </div>
                @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                @endif
            </div>

            <div class="col-xs-12 col-sm-6">
                <div id="register_email"  class="box">
                    <p class="subtitle">Votre email*</p>
                    {!! Form::text('email', null, ['class' => 'inputeffect']) !!}
                </div>
                @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                @endif
            </div>

            <div class="col-xs-12 col-sm-6">
                <div id="register_email"  class="box">
                    <p class="subtitle"> Mot de passe*</p>
                    {!! Form::text('password', null, ['class' => 'inputeffect']) !!}
                </div>
                @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                @endif
            </div>

            <div class="col-xs-12 col-sm-6">
                <div id="register_email"  class="box">
                    <p class="subtitle"> Confirmation mot de passe*</p>
                    {!! Form::text('password_confirmation', null, ['class' => 'inputeffect']) !!}
                </div>
                @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                @endif
            </div>

            <div class="col-xs-12">
                <div class="buttons">
                    <button type="submit" id="register-submit-welcome">
                        Ajouter un utilisateur
                    </button>
                </div>
            </div>



        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection
