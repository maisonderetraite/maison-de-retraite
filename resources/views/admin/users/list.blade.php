@extends('layouts.admin')
@section('content')

<div id="list-users">
    <p id="createuser">
        <a href="{{ url('/admin/utilisateurs/creation') }}">Ajouter un utilisateur</a>
    </p>
    <table class="tableuser">
        <thead>
        <th>Id</th>
        <th>Nom</th>
        <th>Email</th>
        <th>Role</th>
        <th>Editer role</th>
        <th>Actions</th>
        </thead>
        <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->roles}}</td>
            <td>
                {!! Form::model($user, ['url' => action("Admin\UserController@editrole",$user), 'method' => "Put"]) !!}
                {{ Form::select('roles', ['administrateur' => 'administrateur', 'commercial' => 'commercial', 'chatteur' => 'chatteur', 'operateur' => 'operateur' ], null, ['placeholder' => 'Selectionner un role']) }}
                <button class="alert alert-success">
                    Changer son role
                </button>
                {!! Form::close() !!}
            </td>
            <td>
                {{ HTML::linkAction('Admin\UserController@edit','editer',[$user->id], ['class' => 'alert alert-success' ]) }}
                {!! Form::open(array('url' => '/admin/utilisateurs/'.$user->id .'', 'class' => 'formdelete')) !!}
                {!! Form::hidden('_method', 'delete') !!}
                <button class="delete alert alert-danger">
                    Supprimer
                </button>
                {!! Form::close() !!}
            </td>
        </tr>

        @endforeach

        </tbody>
    </table>
</div>
@endsection
