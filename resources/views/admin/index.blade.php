@extends('layouts.admin')
@section('content')
    @if($user->roles == "administrateur")
        <div id="admin-home">
            <div id="stats">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="bloc">
                            <i class="mdr-call-center"></i>
                            <p>
                                <span>
                                    {{ $users->count() }}
                                </span>
                                Commerciaux </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="bloc">
                            <i class="mdr-multiple-users-silhouette"></i>

                            <p><span>{{ $demandeurscount->count() }}</span>Nombre de demande</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div id="list-demandeurs">
        <table class="table-fill">
            <thead>
                <th>Id</th>
                <th>Statut</th>
                <th>Nom commercial</th>
                <th>Nom / Prénom</th>
                <th>Lien</th>
                <th>Email /Téléphone</th>
                <th>Secteurs</th>
                <th>Période</th>
                <th>Date Modification</th>
                <th>Actions</th>
            </thead>
            <tbody>

            @foreach($demandeurs as $demandeur)

                <tr>
                    <td>{{$demandeur->id}}</td>
                    <td>{{$demandeur->statut}}</td>

                    <td> @if ( isset($demandeur->users))
                        {{$demandeur->users->name}}
                        @endif
                    </td>
                    <td>{{$demandeur->nom}}<br/>{{$demandeur->prenom}}</td>
                    <td>{{$demandeur->lien_personne}}</td>
                    <td>{{$demandeur->email}}<br/>{{$demandeur->telephone}}</td>
                    <td>

                        @if ( isset($demandeur->secteurs))
                        @foreach ($demandeur->secteurs as $secteur )

                            {{$secteur->name}}<br/>
                        @endforeach
                        @endif
                    </td>
                    <td>{{$demandeur->periode_souhaite}}</td>
                    <td>{{$demandeur->updated_at}}</td>
                    <td>

                        {{ HTML::linkAction('Admin\AdminController@show','editer',[$demandeur->id , 0], ['class' => 'alert alert-success' ]) }}

                        @if($user->roles == "administrateur")
                            {!! Form::open(array('url' => '/admin/'.$demandeur->id .'', 'class' => 'formdelete')) !!}
                            {!! Form::hidden('_method', 'delete') !!}
                            <button class="delete alert alert-danger">
                                Supprimer
                            </button>
                            {!! Form::close() !!}
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
