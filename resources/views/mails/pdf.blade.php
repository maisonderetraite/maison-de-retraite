


<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Simple Transactional Email</title>
    <style type="text/css">

        /* -------------------------------------
            INLINED WITH https://putsmail.com/inliner
        ------------------------------------- */
        /* -------------------------------------
            RESPONSIVE AND MOBILE FRIENDLY STYLES
        ------------------------------------- */
        @media only screen and (max-width: 620px) {
            table[class=body] h1 {
                font-size: 28px !important;
                margin-bottom: 10px !important; }
            table[class=body] p,
            table[class=body] ul,
            table[class=body] ol,
            table[class=body] td,
            table[class=body] span,
            table[class=body] a {
                font-size: 16px !important; }
            table[class=body] .wrapper,
            table[class=body] .article {
                padding: 10px !important; }
            table[class=body] .content {
                padding: 0 !important; }
            table[class=body] .container {
                padding: 0 !important;
                width: 100% !important; }
            table[class=body] .main {
                border-left-width: 0 !important;
                border-radius: 0 !important;
                border-right-width: 0 !important; }
            table[class=body] .btn table {
                width: 100% !important; }
            table[class=body] .btn a {
                width: 100% !important; }
            table[class=body] .img-responsive {
                height: auto !important;
                max-width: 100% !important;
                width: auto !important; }}
        /* -------------------------------------
            PRESERVE THESE STYLES IN THE HEAD
        ------------------------------------- */
        @media all {
            .ExternalClass {
                width: 100%; }
            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%; }
            .apple-link a {
                color: inherit !important;
                font-family: inherit !important;
                font-size: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
                text-decoration: none !important; }
            .btn-primary table td:hover {
                background-color: #34495e !important; }
            .btn-primary a:hover {
                background-color: #34495e !important;
                border-color: #34495e !important; }

            .logo {
                display:block;
                margin: 0 auto 20px;

            }
        }
    </style>
</head>
<body class="" style="background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
<table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;">
    <tr>
        <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
        <td class="container" style="font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;">
            <div class="content" style="box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;">
                <!-- START CENTERED WHITE CONTAINER -->
                <span class="preheader">
                    <p style="text-align: center;">
                   {{ HTML::image('images/logo.png', 'Maison de retraite') }}
                    </p>
                </span>
                <table class="main" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;border-radius:3px;width:100%;">
                    <!-- START MAIN CONTENT AREA -->
                    <tr>
                        <td class="wrapper" style="background:#fff;font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:40px;">
                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                <tr>
                                    <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">

                                        <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;color:#333;">{{$demandeur->etat_civil_civilite}} {{$demandeur->etat_civil_nom_de_naissance}},<br/>
                                            Vous êtes actuellement à la recherche d’une maison de retraite. Notre organisme est ravi
                                            de vous accompagner et vous remercie de votre confiance.</p><br/>
                                        <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;color:#333;">Suite à l’entretien téléphonique avec votre conseiller, nous vous envoyons comme convenu :</p>

                                        <ul style="color:#333">
                                            <li style="color:#333">Le dossier de demande d’admission en Etablissement d’Hébergement Pour
                                            Personnes Âgées Dépendantes, à nous renvoyer signé sous 48 heures par e-mail
                                            à l’adresse suivante :
                                            </li>
                                        </ul>
                                        <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;color:#333;">
                                            <a target="_blank" href="admission@mamaisonderetraite.org" style="color:#fff;">admission@mamaisonderetraite.org</a>
                                        </p>
                                        <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;color:#333;">
                                            Pour finaliser votre dossier, il est indispensable que l’établissement que vous aurez
                                            sélectionné dispose des pièces justificatives suivantes :
                                        </p>
                                        <ul style="color:#333">
                                            <li style="color:#333">Le dernier avis d’imposition ou de non-imposition</li>
                                            <li style="color:#333"> Les justificatifs des pensions</li>
                                            <li style="color:#333">Le volet médical, à faire compléter par votre médecin traitant ou un autre médecin</li>
                                        </ul>
                                        <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;color:#333;">
                                            Ces pièces justificatives devront être impérativement envoyées à l’adresse suivante :
                                        </p>

                                        <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;box-sizing:border-box;width:100%;">
                                            <tr>
                                                <td align="left" style="font-family:sans-serif;font-size:14px;vertical-align:top;padding:15px;border:1px solid #ccc;">
                                                    <p  style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:5px;color:#333;">{{$demandeur->maison_de_retraite_nom}}</p>
                                                    <p  style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:5px;color:#333;">{{$demandeur->maison_de_retraite_adresse}}</p>
                                                    <p  style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:5px;color:#333;">{{$demandeur->maison_de_retraite_cp}}</p>
                                                    <p  style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:5px;color:#333;">{{$demandeur->maison_de_retraite_ville}}</p>
                                                    <p  style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:5px;color:#333;">{{$demandeur->maison_de_retraite_email}}</p>
                                                </td>
                                            </tr>
                                        </table>



                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- END MAIN CONTENT AREA -->
                </table>
                <!-- START FOOTER -->
                <div class="footer" style="clear:both;padding-top:10px;text-align:center;width:100%;">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                        <tr>
                            <td class="content-block" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#333;font-size:12px;text-align:center;">
                                <span class="apple-link" style="color:#333;font-size:14px;text-align:center;">
                                    Pour plus de renseignement, nos conseillers sont à votre disposition par téléphone 24 heures/24 et 7 jours/7 au 0800 94 7750 (numéro gratuit).<br/><br/>
                                    Nous vous prions, d’agréer, {{$demandeur->etat_civil_civilite}}, nos sincères salutations. <br/><br/>
                                    Ma Maison De Retraite<br/>
                                    Organisme d’aide aux seniors
                                </span>
                                <br>

                            </td>
                        </tr>
                    </table>
                </div>

                <!-- END FOOTER -->

                <!-- START SIGNATURE -->
                <table style="margin-top: 50px;">
                    <tr><td>{{ HTML::image('images/logo.png', 'Maison de retraite') }}</td></tr>
                    <tr>
                        <td class="content-block" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#333;font-size:12px;text-align:center;">
                            <p style="color:#357799;text-align: left;font-size: 18px;margin-bottom: 0px;">{{ $user->name }}</p>
                            <p  style="color:#49a9c2;font-size: 16px;margin-top: 2px;font-style: italic;margin-bottom: 5px;">Votre conseiller Maison de Retraite</p>
                            {{ HTML::image('images/tel-email.jpg', 'Maison de retraite') }}<br/>
                            <div style="margin-top: 5px;">
                                <div style="float:left;">
                                    {{ HTML::image('images/home-email.jpg', 'Maison de retraite') }}
                                </div>
                                <div style="float:left;">
                                    <p  style="
                        color: #357799;
                        text-align: left;
                        font-size: 17px;
                        margin-bottom: 0px;
                        margin-top: 0;
                        margin-left: 10px;
                    "> 2 rue Buhan<br/>
                                        33000 Bordeaux</p>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>

                <!-- END SIGNATURE-->


                <!-- END CENTERED WHITE CONTAINER -->
            </div>
        </td>
        <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
    </tr>
</table>
</body>
</html>