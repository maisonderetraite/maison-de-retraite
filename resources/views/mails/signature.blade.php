<table>
    <tr>{{ HTML::image('images/logo.png', 'Maison de retraite') }}</tr>
    <tr>
        <td class="content-block" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#333;font-size:12px;text-align:center;">
            <p style="color:#357799;text-align: left;font-size: 18px;margin-bottom: 0px;">Aurélie Clément</p>
            <p  style="color:#49a9c2;font-size: 16px;margin-top: 2px;font-style: italic;margin-bottom: 5px;">Votre conseiller Maison de Retraite</p>
            {{ HTML::image('images/tel-email.jpg', 'Maison de retraite') }}<br/>
            <div style="margin-top: 5px;">
                <div style="float:left;">
                    {{ HTML::image('images/home-email.jpg', 'Maison de retraite') }}
                </div>
                <div style="float:left;">
                    <p  style="
                        color: #357799;
                        text-align: left;
                        font-size: 17px;
                        margin-bottom: 0px;
                        margin-top: 0;
                        margin-left: 10px;
                    "> 2 rue Buhan<br/>
                        33000 Bordeaux</p>
                </div>
            </div>
        </td>
    </tr>
</table>