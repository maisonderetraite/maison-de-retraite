<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex nofollow">
    <title>{{ config('app.name', 'La maison de retraite') }}</title>
    <!-- Styles -->
    <link href="https://file.myfontastic.com/cdBPjzbe2V8MKGteiJ822Z/icons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body id="login">
    <div id="loginform">
        <div id="form-login">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <a id="logo-login" href="{{url('/')}}">
                    {{ HTML::image('images/logo.png', 'Maison de retraite') }}
                </a>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">Votre email</label>
                    <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                     <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password">Mot de passe</label>
                    <input id="password" type="password" name="password" required>
                    @if ($errors->has('password'))
                    <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                    @endif
                </div>

                <div class="form-group">
                    <div id="rememberme" class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>Se souvenir de moi
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <p class="submit">
                        <button type="submit">
                            Se connecter
                        </button>
                    </p>
                    <p id="mdp-forget">
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            Mot de passe oublié ?
                        </a>

                    </p>
                </div>
            </form>
        </div>
    </div>
</body>


