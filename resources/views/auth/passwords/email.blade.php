<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex nofollow">
    <title>{{ config('app.name', 'La maison de retraite') }}</title>
    <!-- Styles -->
    <link href="https://file.myfontastic.com/cdBPjzbe2V8MKGteiJ822Z/icons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body id="login">
<div id="loginform">
    <div id="form-login">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}
            <a id="logo-login" href="{{url('/')}}">
                {{ HTML::image('images/logo.png', 'Maison de retraite') }}
            </a>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email">Votre adresse mail</label>
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
            </div>
            <div class="form-group">
                <p class="submit">
                    <button type="submit">
                        Réinitialiser le mot de passe
                    </button>
                </p>
            </div>
        </form>
    </div>
</div>
</body>



