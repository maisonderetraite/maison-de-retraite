@extends('layouts.app')
@section('content')
<div id="pdc" class="page">
    <div class="container">
        <h1>CGU/CGV</h1>
        <p><strong>PREAMBULE</strong></p>

        <p>Les présentes Conditions d’Utilisation définissent les règles et conditions d’utilisation du Site Internet et sont conclues entre l’<strong>Utilisateur</strong> et la société  <strong>MONETIZ</strong>, Société À Responsabilité Limitée, au capital de 10.000 €, immatriculée sous le numéro 538 223 025 au RCS de Bordeaux, ayant son siège social au 2 rue Buhan à BORDEAUX (33000), représentée par Monsieur Samuel RANSON (ci-après dénommée « <strong>MONETIZ</strong> »), qui propose, via son Site Internet accessible à l’adresse www.maisonderetraite.net, un service d’aide à la recherche de résidences spécialisées.</p>

        <p><strong>MONETIZ</strong> et l’<strong>Utilisateur</strong> sont ci-après individuellement dénommés une « <strong>Partie</strong> » et ensemble les « <strong>Parties</strong> ».</p>

        <p>Toute utilisation du Site Internet implique de plein droit l’acceptation sans réserve de l’intégralité des présentes Conditions d’Utilisation.</p>

        <p>L’acceptation par l’<strong>Utilisateur</strong> des présentes Conditions Générales d’Utilisation est réputée acquise du seul fait de la navigation sur ce Site Internet. Tout usage du Site Internet équivaut pour l’<strong>Utilisateur</strong> à reconnaître qu’il a pris pleinement connaissance et qu’il approuve, sans exception ni réserve, l’ensemble des Conditions générales indiquées ci-après.</p>


        <p><strong>Article 1 – Définitions</strong></p>

        <p>« Internet » désigne différents réseaux de serveurs localisés en divers lieux à travers le monde, reliés entre eux à l'aide de réseaux de communication, et communiquant à l'aide d'un protocole spécifique connu sous le nom de TCP/IP.</p>

        <p>« Service » désigne l’ensemble des Services fournis par <strong>MONETIZ</strong> à l’<strong>Utilisateur</strong> par l’intermédiaire de son Site Internet comprenant l’aide à la recherche d’une résidence spécialisée (EHPAD, maison de retraite, résidence services) par téléphone ou par email.</p>
        <p>« <strong>Site Internet</strong> » (ou « <strong>Application</strong> ») désigne l’infrastructure développée par <strong>MONETIZ</strong> selon les formats informatiques utilisables sur l’Internet comprenant des données de différentes natures, et notamment des textes, sons, images fixes ou animées, vidéos, bases de données, destinées à être consultées par les <strong>Utilisateur</strong>s de l’Internet dans le cadre d’un accès libre à l’adresse www.maisonderetraite.net.</p>

        <p>« <strong>Utilisateur</strong> » désigne tout <strong>Utilisateur</strong> qui navigue sur le Site Internet et prend connaissance de son contenu.</p>


        <p><strong>Article 2 – Déclarations et obligations de l’<strong>Utilisateur</strong></strong></p>

        <p>2.1. L’<strong>Utilisateur</strong> déclare disposer de la capacité juridique lui permettant de donner son accord aux présentes Conditions générales d’Utilisation.

        <p>2.2. <strong>L’Utilisateur reconnaît que le Site Internet nécessite une connexion Internet pour fonctionner dans son ensemble. A ce titre, l’Utilisateur déclare bien connaître l’Internet, ses caractéristiques et ses limites et reconnaît notamment :</strong></p>
        <ul>
        <li>- que les transmissions de données sur l’Internet ne bénéficient que d’une fiabilité technique relative, celles-ci circulant sur des réseaux hétérogènes aux caractéristiques et capacités techniques diverses qui sont parfois saturés à certaines périodes de la journée ;</li>
        <li>- que les <strong>Utilisateur</strong>s du Site Internet sont susceptibles d’être localisés en tous lieux à travers le monde, et que partant, le contenu du Site Internet peut être reproduit, représenté ou, plus généralement, diffusé sans aucune limitation géographique ;</li>
        <li>- que les données circulant sur l’Internet ne sont pas protégées contre des détournements éventuels et qu’ainsi la communication de mots de passe, codes confidentiels et plus généralement, de toutes informations à caractère sensible est effectuée par l’<strong>Utilisateur</strong> à ses risques et périls.</li>
        </ul>
        <br/>
        <p>2.3. Par ailleurs, l’<strong>Utilisateur</strong> s’engage à ne pas utiliser le Site Internet d’une manière non prévue par les présentes Conditions d’Utilisation. A ce titre, l’<strong>Utilisateur</strong> s’engage notamment à ne pas utiliser le Site Internet pour renseigner des commentaires constituants :</p>
        <ul>
            <li>- des messages à caractère pornographique et pédopornographique ;</li>
            <li>- des messages racistes, xénophobes, révisionnistes, faisant l'apologie de crime de guerre, discriminant ou incitant à la haine qu’elle soit à l'encontre d'une personne, d'un groupe de personnes en raison de leur origine, leur genre, leur ethnie, leur croyance ou leur mode de vie ;</li>
            <li>- des messages à caractère injurieux, violent, menaçant, au contenu choquant ou portant atteinte à la dignité humaine ;</li>
            <li>- des messages diffamatoires ;</li>
            <li>- des messages portant atteinte au droit d'auteur et plus généralement aux droits de propriété intellectuelle ;</li>
            <li>- des messages portant atteinte au droit à l'image et au respect à la vie privée ;</li>
            <li>- de manière générale, des messages contraires aux lois et règlements en vigueur en France ;</li>
            <li>- des publicités non sollicitées, qu'elles soient commerciales ou non.</li>
        </ul>
        <br/>
        <p>2.4. L’<strong>Utilisateur</strong> s’engage à ne pas utiliser des failles, bugs informatiques ou toute autre forme d'erreur pour obtenir des avantages dans l’utilisation du Site Internet De même, l’<strong>Utilisateur</strong> s’engage à avertir immédiatement <strong>MONETIZ</strong> lorsqu’il constate une faille ou une erreur sur le Site Internet.</p>

        <p>2.5. L’<strong>Utilisateur</strong> s’engage à ne pas utiliser le Site Internet d'une manière qui puisse le rendre inaccessible, l’endommager ou l’empêcher de fonctionner.</p>


        <p><strong>Article 3 – Accès aux Services</strong></p>

        <p>3.1. L’accès aux Services nécessite la communication de certaines informations par l’<strong>Utilisateur</strong> sur le Site Internet.</p>

        <p>3.2. Pour bénéficier des Services, l’<strong>Utilisateur</strong> doit être une personne physique majeure capable juridiquement de contracter.</p>

        <p>3.3. Si l’<strong>Utilisateur</strong> choisi d’être contacté par mail, il doit fournir une adresse email valide, ses nom et prénom, un objet et rédiger un message.</p>

        <p>3.4. Si l’<strong>Utilisateur</strong> choisi d’être contacté par téléphone, il doit indiquer les noms et prénoms de la personne concernée par la recherche, le secteur géographique et la période souhaitée, puis doit fournir une adresse email valide, ses nom et prénom, et son numéro de téléphone.</p>


        <p><strong>Article 4 – Licence relative à l’accès et à l’utilisation du Site Internet</strong></p>

        <p>4.1. <strong>MONETIZ accorde aux Utilisateurs une licence limitée à l'accès et à l'utilisation du Site Internet, pour une utilisation exclusivement privée et personnelle, non collective et non exclusive. En aucun cas, les Utilisateurs ne sont autorisés à télécharger ou à modifier tout ou partie du Site Internet sans l'autorisation écrite et préalable de MONETIZ. Cette licence ne permet en aucun cas aux Utilisateurs de procéder à une quelconque utilisation commerciale du Site Internet et/ou de tout ou partie de son contenu.</strong></p>

        <p>4.2. <strong>MONETIZ</strong> autorise, à titre non exclusif et révocable, la création de liens hypertextes pointant sur la page d'accueil du Site Internet, à la condition que ces liens ne puissent créer à l'encontre de <strong>MONETIZ</strong> et de ses Services aucun préjudice direct ou indirect. En aucun cas, la création ou la présence de ces liens hypertextes ne pourra engager la responsabilité de <strong>MONETIZ</strong>, à quelque titre et pour quelque motif que ce soit. Tout lien hypertexte créé sur un site tiers devra immédiatement être retiré sur demande de <strong>MONETIZ</strong>, sans préjudice des droits de <strong>MONETIZ</strong> d'obtenir réparation du préjudice éventuel causé par la création de ce lien.</p>


        <p><strong>Article 5 - Sanctions en cas de non respect</strong></p>

        <p>5.1. En cas de violation par l’<strong>Utilisateur</strong> de l’une quelconque des dispositions des présentes Conditions générales, <strong>MONETIZ</strong> se réserve le droit de suspendre temporairement ou définitivement, sans aucun avertissement préalable et à sa seule discrétion, l’accès au Compte de l’<strong>Utilisateur</strong> concerné, sans dédommagement. A ce titre, toute nouvelle demande d’inscription par l’<strong>Utilisateur</strong> pourra être bloquée.</p>

        <p>5.2. Les sanctions décrites ci-dessus peuvent être appliquées sans préjudice de toute poursuite, pénale ou civile, dont l’<strong>Utilisateur</strong> pourrait faire l’objet de la part des autorités publiques, de tiers, ou de <strong>MONETIZ</strong>.</p>


        <p><strong>Article 6 – Responsabilité</strong></p>

        <p>Article 6.1. Responsabilité du <strong>Utilisateur</strong></p>

        <p>6.1.1. L’<strong>Utilisateur</strong> est responsable de l’exactitude, la sincérité et la véracité des informations qu’il fournit à <strong>MONETIZ</strong>. <strong>MONETIZ</strong> ne saurait être tenu responsable en raison d’une erreur dans la saisie de ces informations.</p>

        <p>6.1.2. L’<strong>Utilisateur</strong> est responsable du paiement de tout frais ou dépenses liés à l’utilisation du Site Internet, y compris les frais facturés par le fournisseur d’accès pour l’utilisation du réseau et le partage de données.</p>

        <p>6.1.3. Lors de l’utilisation du Site Internet, l’<strong>Utilisateur</strong> est seul responsable de l’usage qu’il fait du Site Internet et du contenu qu’il communique. A ce titre, il est notamment responsable :
        du contenu produit par lui par le biais de son Compte, et notamment du respect des bonnes mœurs dudit contenu ;
        de son adéquation aux lois et aux règlements notamment en matière de protection des mineurs, de la répression de l'apologie des crimes contre l'humanité, de l'incitation à la haine raciale ainsi que de la pornographie enfantine, de l'incitation à la violence, notamment l'incitation aux violences faites aux femmes, ainsi que des atteintes à la dignité humaine et du respect de la personne humaine et ;
        du respect des droits des tiers notamment en matière de propriété intellectuelle.</p>

        <p>Article 6.2. Responsabilité de <strong>MONETIZ</strong></p>

        <p>6.2.1. Compte tenu des aléas techniques liés au fonctionnement décentralisé du réseau Internet, <strong>MONETIZ</strong> ne fournit aucune garantie de continuité de service ou d’absence d’erreurs du Site Internet.</p>

        <p>6.2.2. <strong>MONETIZ</strong> se réserve le droit de suspendre l’accès au Site Internet en tout ou partie sans préavis notamment pour procéder à toute opération de correction, de mise à jour ou de maintenance. <strong>MONETIZ</strong> ne peut en aucun cas être tenue responsable de tout préjudice et/ou perte qui en résulterait pour l’<strong>Utilisateur</strong>.</p>

        <p>6.2.3. <strong>MONETIZ</strong> n’est pas responsable si un quelconque dysfonctionnement du Site Internet, indépendant de sa volonté, empêche notamment l’accès aux Services.</p>

        <p>6.2.4. <strong>MONETIZ</strong> ne sera pas responsable des dommages subis ou occasionnés par l’utilisation du Site Internet qu’ils s’agissent de dommages matériels ou immatériels, consécutifs ou non consécutifs. A ce titre, il appartient à l’<strong>Utilisateur</strong> de prendre toutes les mesures appropriées de nature à protéger ses propres données et logiciels de la contamination par d'éventuels virus informatiques.</p>

        <p>6.2.5. <strong>MONETIZ</strong> est exclusivement responsable du contenu uniquement produit par lui et intégré au Site Internet et ses fonctionnalités.</p>


        <p><strong>Article 7 – Propriété intellectuelle</strong></p>

        <p>7.1. Tous les droits de propriété intellectuelle et autres droits liés au Site Internet, y compris les droits d’auteur, les marques, les dessins et modèles, les droits sur les bases de données, ainsi que tout autre droit de propriété intellectuelle ou autres, sont et restent la propriété exclusive de <strong>MONETIZ</strong> et, pour les technologies sous licence, de leurs auteurs et/ou propriétaires.</p>

        <p>7.2. Conformément et dans la limite des dispositions de l’article L.342-1 du Code de la propriété intellectuelle, <strong>MONETIZ</strong> interdit l’extraction ou la réutilisation de tout ou partie du contenu de son Site Internet</p>.

        <p>7.3. L’<strong>Utilisateur</strong> reconnaît l’existence de ces droits de propriété et de propriété intellectuelle, et ne prendra aucune mesure visant à porter atteinte, à limiter ou à restreindre de quelque manière que ce soit la propriété ou les droits de <strong>MONETIZ</strong> en ce qui concerne le Site Internet.</p>

        <p>7.4. Si l’<strong>Utilisateur</strong> souhaite utiliser dans un autre cadre, et/ou diffuser des données, informations et/ou contenus du Site Internet, il devra préalablement en faire la demande écrite à l’adresse du siège social de <strong>MONETIZ</strong>.</p>

        <p>7.5. L’<strong>Utilisateur</strong> accepte de ne pas utiliser le Site Internet dans un but commercial, de ne pas louer, prêter, vendre, publier, proposer de licence ou sous-licence, distribuer, attribuer ou de transférer de quelque manière tout ou partie du Site Internet à un tiers quel qu’il soit sans l'autorisation expresse, écrite et préalable de <strong>MONETIZ</strong> qui peut la conditionner à une contrepartie financière.</p>


        <p><strong>Article 8 – Informatique et Libertés</strong></p>

        <p>8.1. <strong>MONETIZ</strong> s’engage à respecter la vie privée de l’<strong>Utilisateur</strong>, ainsi que la sécurité et la confidentialité des données et des informations qui lui sont communiquées par l’<strong>Utilisateur</strong>.

        <p>8.2. A ce titre, <strong>MONETIZ</strong> s'engage à mettre en œuvre tous les moyens à sa disposition pour assurer la sécurité et la confidentialité des informations et données transmises par l’<strong>Utilisateur</strong> sur son Site Internet. <strong>MONETIZ</strong> s'engage notamment à ne pas divulguer les informations confidentielles qui pourraient lui être communiquées par l’<strong>Utilisateur</strong>.</p>

        <p>8.2. L’<strong>Utilisateur</strong> reconnaît expressément que toute donnée nominative le concernant, collectée par <strong>MONETIZ</strong>,  et notamment le formulaire d’inscription, fait l’objet d’un traitement automatisé déclaré auprès de la CNIL (récépissé n° 2048564 v 0).</p>

        <p>8.3. Conformément à la loi n° 2004-801 du 6 août 2004 relative à la protection des personnes physiques à l'égard des traitements de données à caractère personnel et modifiant la loi n °78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, le Client dispose d'un droit d'accès, de modification et de suppression des informations qui le concernent (article 34 de la loi « Informatique et Libertés »), et qu'il peut exercer à tout moment auprès de <strong>MONETIZ</strong> :</p>
        <ul>
            <li>- soit par courrier à l'adresse postale suivante : 2 rue Buhan à BORDEAUX (33000) ;</li>
            <li>- soit par téléphone au numéro suivant : 0800 94 77 55 (appel gratuit)</li>
        </ul>



        <p>8.4. Pour des besoins statistiques, <strong>MONETIZ</strong> peut être amenée à recueillir des informations de navigation via l'utilisation de cookies. L'acceptation de ces cookies est obligatoire pour toute inscription. L’<strong>Utilisateur</strong> peut à tout moment supprimer l’enregistrement des cookies, ou les cookies déjà enregistrés sur son appareil, en paramétrant les options de protection de la vie privée de son navigateur Internet. Le paramétrage du logiciel de navigation permet également d'informer de la présence de cookies et éventuellement de les refuser de la manière décrite à l’adresse suivante : http://www.cnil.fr.

        <p>8.5. Toutes les informations collectées indirectement ne seront utilisées que pour suivre le volume, le type et la configuration du trafic utilisant ce Site Internet, pour en développer la conception et l'agencement et à d'autres fins administratives et de planification, et plus généralement pour améliorer le service offert par <strong>MONETIZ</strong>.


        <p><strong>Article 9 – Modification</strong></p>

        <p>9.1. <strong>MONETIZ</strong> peut modifier à tout moment les présentes Conditions générales, notamment pour se conformer à une disposition légale. L’<strong>Utilisateur</strong> sera informé des modifications intervenues selon les modalités ci-dessous.</p>

        <p>9.2. La nouvelle version des Conditions générales sera disponible sur le Site Internet dans la rubrique concernée. L’<strong>Utilisateur</strong> devra accepter les Conditions générales lors de sa première connexion consécutive à ladite modification.</p>


        <p><strong>Article 10 – Dispositions Générales</strong></p>

        <p>10.1. Si l’une quelconque des stipulations des présentes Conditions générales, ou une partie d’entre elles, s’avérait nulle au regard d’un règlement, d’une loi en vigueur ou à la suite d’une décision judiciaire devenue définitive, elle sera réputée non écrite, mais n’entraînera pas la nullité des Conditions générales dans leur ensemble, ni celle de la clause seulement partiellement concernée.</p>

        <p>10.2. Le fait que l’une ou l’autre des Parties n’ait pas exigé, temporairement ou définitivement, l’application d’une stipulation des présentes Conditions générales ne pourra être considérée comme une renonciation aux droits détenus par cette Partie.</p>


        <p><strong>Article 11 – Médiation</strong></p>

        <p>11.1. Dans l'hypothèse où un litige surviendrait entre les Parties, l’<strong>Utilisateur</strong> a le droit de recourir gratuitement à un médiateur de la consommation en vue de la résolution amiable du litige qui l'oppose à <strong>MONETIZ</strong>. A ce titre, <strong>MONETIZ</strong> garantit à l’<strong>Utilisateur</strong> le recours effectif à un dispositif de médiation de la consommation.

        <p>11.2. Par défaut, <strong>MONETIZ</strong> propose à l’<strong>Utilisateur</strong> le recours au médiateur de la consommation suivant :</p>

        Nom du médiateur : Maître Anne CASIMIRO<br/>
        Organisme du médiateur : MEDICYS<br/>
        Adresse du médiateur : Bordeaux (33)<br/>
        Site Internet du médiateur : www.medicys.fr <br/>
        Contact du médiateur : contact@medicys.fr<br/>

        <p>11.3. Les Parties conviennent que le litige ne pourra être examiné par le médiateur de la consommation lorsque :</p>
        <ul>
            <li>- l’<strong>Utilisateur</strong> ne justifie pas avoir tenté, au préalable, de résoudre son litige directement auprès de <strong>MONETIZ</strong> par une réclamation écrite adressée par lettre recommandée avec accusé de réception à <strong>MONETIZ</strong> dans un délai de quinze (15) jours à compter de la fourniture du Service ;</li>
            <li>- la demande est manifestement infondée ou abusive ;</li>
            <li>- le litige a été précédemment examiné ou est en cours d'examen par un autre médiateur ou par un tribunal ;</li>
            <li>- l’<strong>Utilisateur</strong> a introduit la demande auprès du médiateur dans un délai supérieur à un (1) an à compter de sa réclamation écrite auprès de <strong>MONETIZ</strong> ;</li>
            <li>- le litige n'entre pas dans le champ de compétence du médiateur.</li>

        </ul>
        <br/>
        <p><strong>Article 12 – Droit Applicable et Litiges</strong></p>

        <p>12.1. Les présentes Conditions générales, ainsi que l’ensemble des rapports contractuels qui pourraient en découler sont soumis au droit français.</p>

        <p>12.2. Dans l’hypothèse où un litige surviendrait entre l’<strong>Utilisateur</strong> et <strong>MONETIZ</strong>, les Parties s’engagent à rechercher une solution amiable, prenant en compte les intérêts de chacune d’elles avant d’engager toute action judiciaire.</p>

        <p>12.3. A défaut, les Tribunaux du ressort de la Cour d’Appel de Bordeaux seront seuls compétents, sous réserve d’une attribution de compétence spécifique découlant d’un texte de loi ou réglementaire particulier.</p>

    </div>
</div>
@endsection
