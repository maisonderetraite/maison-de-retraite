require('./bootstrap');
require('./easing');
require('./modal');
require('./height');
require('./jqueryui');
/*-- chat --*/

$("#chat_button").click(function() {
    "undefined" != typeof window.$zopim.livechat ? window.$zopim.livechat.window.show() : $(this).tooltip({
        title: "Chat indisponible",
        content: "Veuillez réessayer plus tard",
        placement: "top"
    }).tooltip("show")
});


/*--- menu ---*/


$('#button-mobile').on('click',function(){
    if($('#button-mobile').hasClass('closed')) {
        $('#button-mobile').removeClass('closed');
        $('#menu').slideDown();
    } else {
        $('#button-mobile').addClass('closed');
        $('#menu').slideUp();
    }
});


/*--- formulaire --*/

function validAge(v) {
    var r = new RegExp("^[0-9]{1,5}$");
    return (v.match(r) == null) ? false : true;
}

function validEmail(v) {
    var r = new RegExp("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$");
    return (v.match(r) == null) ? false : true;
}
function validTelephone(v) {
    var r = new RegExp("^[0-9]{10}$");
    return (v.match(r) == null) ? false : true;
}


//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches



$(".next").click(function(){
    valid = true;
    var form = $('#msform');
    var age = $('#age-bloc input').val();
    var name = $('#name-firstname-bloc input').val();

    var secteur = $('#first-secteur input').val();
    var periode = form.find('input[name=periode_souhaite]:checked').val();

    if (name == "") {
        $('#name-firstname-bloc').addClass('error');
        $('#name-firstname-bloc .errortext').text('Veuillez saisir un nom');
        valid = false;
    }
    else {
        $('#name-firstname-bloc').removeClass('error');
        $('#name-firstname-bloc .errortext').text('');
    }
    if (secteur == "") {
        $('#first-secteur').addClass('error');
        $('#first-secteur .errortext').text('Veuillez saisir un nom');
        valid = false;
    } else {
        $('#first-secteur').removeClass('error');
        $('#first-secteur .errortext').text('');
    }

    if (name == "") {
        $('#name-firstname-bloc').addClass('error');
        $('#name-firstname-bloc .errortext').text('Veuillez saisir un nom');
        valid = false;
    }
    else {
        $('#name-firstname-bloc').removeClass('error');
        $('#name-firstname-bloc .errortext').text('');
    }

    if ( age == "") {
        $('#age-bloc').addClass('error');
        $('#age-bloc .errortext').text('Veuillez saisir votre age');
        valid = false;
    }
    else if (!validAge(age)) {
        $('#age-bloc').addClass('error');
        $('#age-bloc .errortext').text('Veuillez saisir un chiffre');
        valid = false;
    }
    else {
        $('#age-bloc').removeClass('error');
        $('#age-bloc .errortext').text('');
    };

    if (!periode) {
        $('#periode-bloc').addClass('error');
        $('#periode-bloc .errortext').text('Veuillez saisir une période');
        valid = false;
    }
    else {
        $('#periode-bloc').removeClass('error');
        $('#periode-bloc .errortext').text('');
    };

    if ($('.list-ville').is(':visible')) {
        valid = false;
    }
    if (valid) {
        if(animating) return false;

        animating = true;
        current_fs = $(this).parent();
        next_fs = $(this).parent().next();

        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 50)+"%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({
                    'transform': 'scale('+scale+')'
                });
                next_fs.css({'left': left, 'opacity': opacity});
            },
            duration: 0,
            complete: function(){
                current_fs.hide();
                animating = false;
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    }
});

$(function () {
    $( "#agenumber" ).change(function() {
        var max = parseInt($(this).attr('max'));
        var min = parseInt($(this).attr('min'));
        if ($(this).val() > max)
        {
            $(this).val(max);
        }
        else if ($(this).val() < min)
        {
            $(this).val(min);
        }
    });
});


$('#formulaire-button').on('click',function() {

    valid =  true;
    var form = $('#msform');
    var civilite = form.find('input[name=civilite]:checked').val();
    var nom = $('#nom-bloc input').val();
    var prenom = $('#prenom-bloc input').val();
    var telephone = $('#telephone-bloc input').val();
    var email = $('#email-bloc input').val();

    if (!civilite) {
        $('#civilite-bloc').addClass('error');
        $('#civilite-bloc .errortext').text('Veuillez saisir votre civilite');
        valid = false;
    }
    else {
        $('#civilite-bloc').removeClass('error');
        $('#civilite-bloc .errortext').text('');
    }
    if (nom == "") {
        $('#nom-bloc').addClass('error');
        $('#nom-bloc .errortext').text('Veuillez saisir un nom');
        valid = false;
    }
    else {
        $('#nom-bloc').removeClass('error');
        $('#nom-bloc .errortext').text('');
    }
    if (prenom == "") {
        $('#prenom-bloc').addClass('error');
        $('#prenom-bloc .errortext').text('Veuillez saisir un prénom');
        valid = false;
    }
    else {
        $('#prenom-bloc').removeClass('error');
        $('#prenom-bloc .errortext').text('');
    }

    if (email == "") {
        $('#email-bloc').addClass('error');
        $('#email-bloc .errortext').text('Veuillez saisir un email');
        valid = false;
    }
    else if (!validEmail(email)) {
        $('#email-bloc').addClass('error');
        $('#email-bloc .errortext').text('Veuillez saisir un email valide');
        valid = false;
    }
    else {
        $('#email-bloc').removeClass('error');
        $('#email-bloc .errortext').text('');
    }

    if ( telephone == "") {
        $('#telephone-bloc').addClass('error');
        $('#telephone-bloc .errortext').text('Veuillez saisir votre téléphone');
        valid = false;
    }
    else if (!validTelephone(telephone)) {
        $('#telephone-bloc').addClass('error');
        $('#telephone-bloc .errortext').text('Veuillez saisir un téléphone valide');
        valid = false;
    }
    else {
        $('#telephone-bloc').removeClass('error');
        $('#telephone-bloc .errortext').text('');
    };
    return valid;
});

$('#contact-email').on('click',function() {
    valid =  true;
    var nom = $('#nom-bloc-contact input').val();
    var prenom = $('#prenom-bloc-contact input').val();
    var telephone = $('#telephone-bloc input').val();
    var email = $('#email-bloc-contact input').val();
    var objet = $('#objet-bloc input').val();
    var message = $('#message-bloc textarea').val();
    if (nom == "") {
        $('#nom-bloc-contact').addClass('error');
        $('#nom-bloc-contact .errortext').text('Veuillez saisir un nom');
        valid = false;
    }
    else {
        $('#nom-bloc-contact').removeClass('error');
        $('#nom-bloc-contact .errortext').text('');
    }
    if (prenom == "") {
        $('#prenom-bloc-contact').addClass('error');
        $('#prenom-bloc-contact .errortext').text('Veuillez saisir un prénom');
        valid = false;
    }
    else {
        $('#prenom-bloc-contact').removeClass('error');
        $('#prenom-bloc-contact .errortext').text('');
    }

    if (email == "") {
        $('#email-bloc-contact').addClass('error');
        $('#email-bloc-contact .errortext').text('Veuillez saisir un email');
        valid = false;
    }
    else if (!validEmail(email)) {
        $('#email-bloc').addClass('error');
        $('#email-bloc .errortext').text('Veuillez saisir un email valide');
        valid = false;
    }
    else {
        $('#email-bloc-contact').removeClass('error');
        $('#email-bloc-contact .errortext').text('');
    }
    if (objet == "") {
        $('#objet-bloc').addClass('error');
        $('#objet-bloc .errortext').text('Veuillez saisir un objet');
        valid = false;
    }
    else {
        $('#objet-bloc').removeClass('error');
        $('#objet-bloc .errortext').text('');
    }
    if (message == "") {
        $('#message-bloc').addClass('error');
        $('#message-bloc .errortext').text('Veuillez saisir un message');
        valid = false;
    }
    else {
        $('#message-bloc').removeClass('error');
        $('#message-bloc .errortext').text('');
    }

    return valid;
});



$(".previous").click(function(){
    if(animating) return false;
    animating = true;
    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    //de-activate current step on progressbar
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1-now) * 50)+"%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({'left': left});
            previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
        },
        duration: 0,
        complete: function(){
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

    $(".submit").click(function(){
        return false;
    })

/*-- ajout d'un secteur supplémentaire --*/

    $(".r-btnAdd").click(function(){
       $('#other-proposition').show();
    })

    $(".r-btnRemove").click(function(){

        $('#other-proposition').hide();
        $('#other-proposition').find('.secteur').val('');
    })


    //var append = true;
    //
    //var url = "https://www.cp-ville.com/cpcom.php?jsoncallback=?";
    //
    //$('.proposition').find('.secteur').keyup(function() {
    //    var val = $(this).val();
    //    var parent = $(this).parent();
    //    var selDpt = parent.find('.list-ville');
    //    selDpt.children('li').remove();
    //    $('body').append
    //    if(val == '') {
    //        selDpt.children('li').remove();
    //        selDpt.hide();
    //        $('body').removeClass('lol');
    //        append = true;
    //    }
    //    if(val.length < 3) {
    //        append = true;
    //        selDpt.hide();
    //    } else {
    //        if(append) {
    //            selDpt.show();
    //            $('body').append('lol');
    //            append = false;
    //        }
    //    }
    //
    //    $.ajax({
    //        url: url,
    //        dataType: "jsonp",
    //        jsonpCallback: 'jsoncallback',
    //        data : {cpcommune :val },
    //        success: function(data) {
    //            var items = [];
    //
    //            $.each(data, function (i) {
    //
    //                if (data.count == 0)
    //                {
    //                    selDpt.hide();
    //                    parent.find('.secteur').val('');
    //                    if (needsUpdate) {
    //                        $('body').append('zzz');
    //                    }
    //                }
    //
    //                if (data.typeretour == 'ville') {
    //                    if (data[i].cp ==  null) {
    //
    //                    } else {
    //                        items.push('<li>' + data[i].cp + ' ('+data[i].ville+') '+ '</li>');
    //                    }
    //                }
    //                if ( data.typeretour == 'cp' ) {
    //                    if (data[i].ville == null) {
    //
    //                    } else {
    //                        items.push('<li>' + data[i].cp + ' (' + data[i].ville + ')' + '</li>');
    //                    }
    //                }
    //            });
    //
    //            selDpt.children('li').remove();
    //            selDpt.append( items.join('') );
    //
    //        }
    //    });
    //});
    //


$('#button-immediat').click(function() {
    $('#Modal-rappel #button').hide();
    $('#Modal-rappel #immediat').show();

});


$(function()
{
    $('.secteur').each(function() {
        $(this).autocomplete({
            source: "https://www.maisonderetraite.net/ville",
            minLength: 2,
            focus: function(event, ui) {

                $(this).val(ui.item.value);
            },
            change: function (event, ui) {
                if (!ui.item) {
                    $(this).val('');
                }
            }
        });
    })
});








