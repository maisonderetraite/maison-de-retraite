
require('./bootstrap');
require('./jqueryui');
require('./table');
require('./bootstrap-datepicker')
require('./repeateur');
require('./height');


/*--- table ---*/

$('.table-fill').DataTable({
    responsive: {
        details: false
    },
    "order": [[ 1, "DESC" ]],
    "language": {
        "sProcessing":     "Traitement en cours...",
        "sSearch":         "Rechercher&nbsp;:",
        "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
        "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
        "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
        "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
        "sInfoPostFix":    "",
        "sLoadingRecords": "Chargement en cours...",
        "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
        "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
        "oPaginate": {
            "sFirst":      "Premier",
            "sPrevious":   "Pr&eacute;c&eacute;dent",
            "sNext":       "Suivant",
            "sLast":       "Dernier"
        },
        "oAria": {
            "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
            "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
        }
    },
    "pageLength": 100,
});

$('.delete').on('click', function(e){
    e.preventDefault();
    swal({
            title: "Êtes vous sur ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            closeOnConfirm: false,
        },
        function(){
            e.delegateTarget.form.submit();
        });
});



// Datepicker (version fr)

$.fn.datepicker.dates['fr'] = {
    days: ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
    daysShort: ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."],
    daysMin: ["d", "l", "ma", "me", "j", "v", "s"],
    months: ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"],
    monthsShort: ["janv.", "févr.", "mars", "avril", "mai", "juin", "juil.", "août", "sept.", "oct.", "nov.", "déc."],
    today: "Aujourd'hui",
    clear: "Effacer",
    weekStart: 1,
    format: "dd/mm/yyyy"
};

$('.datedemande').datepicker({
    language: "fr",
    format: "dd/mm/yyyy"
});

$("#showprofil" ).click(function() {
    $( "#dropdown-menu" ).slideToggle( "slow", function() {
        // Animation complete.
    });
});

$('#commentaire-list').repeater({
    btnAddClass: 'r-btnAdd',
    btnRemoveClass: 'r-btnRemove',
    groupClass: 'r-group',
    minItems: 1,
    maxItems: 1,
    startingIndex: 0,
    reindexOnDelete: true,
    repeatMode: 'append',
    animation: null,
    animationSpeed: 400,
    animationEasing: 'swing',
    clearValues: true
});

/*-- gestion hauteur --*/
    /*-- admin ( onglet ) --*/
$(function() {
    $('#bloc-statut .col-sm-6 .box').matchHeight();
    $('#admin-onglet .col-10').matchHeight();
});



$('#faq .faq-box').on('click',function(){
    if($(this).hasClass('closed')) {
        $(this).find('.reponses').slideDown();
        $(this).removeClass('closed').addClass('opened');
    } else {
        $(this).find('.reponses').slideUp();
        $(this).removeClass('opened').addClass('closed');
    }
});



/*--- formulaire --*/

function validAge(v) {
    var r = new RegExp("^[0-9]{1,5}$");
    return (v.match(r) == null) ? false : true;
}

function validEmail(v) {
    var r = new RegExp("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$");
    return (v.match(r) == null) ? false : true;
}
function validTelephone(v) {
    var r = new RegExp("^[0-9]{10}$");
    return (v.match(r) == null) ? false : true;
}



$(function () {
    $( "#agenumber" ).change(function() {
        var max = parseInt($(this).attr('max'));
        var min = parseInt($(this).attr('min'));
        if ($(this).val() > max)
        {
            $(this).val(max);
        }
        else if ($(this).val() < min)
        {
            $(this).val(min);
        }
    });
});


$('#formulaire-fiche').on('click',function() {

    valid =  true;
    var form = $('#fiche-form');
    var age = $('#age-bloc input').val();
    var name = $('#name-firstname-bloc input').val();
    var periode = form.find('input[name=periode_souhaite]:checked').val();
    var civilite = form.find('input[name=civilite]:checked').val();
    var nom = $('#nom-bloc input').val();
    var prenom = $('#prenom-bloc input').val();
    var telephone = $('#telephone-bloc input').val();
    var email = $('#email-bloc input').val();

    if (name == "") {
        $('#name-firstname-bloc').addClass('error');
        $('#name-firstname-bloc .errortext').text('Veuillez saisir un nom');
        valid = false;
    }
    else {
        $('#name-firstname-bloc').removeClass('error');
        $('#name-firstname-bloc .errortext').text('');
    }
    $(".secteur").each(function() {
        if ($('#secteur-bloc').find('.secteur').val() == "") {
            $('#secteur-bloc').addClass('error');
            $('#secteur-bloc').find('.errortext').text('Veuillez saisir un secteur');
            valid = false;
        }
        else {
            $('#secteur-bloc').removeClass('error');
            $('#secteur-bloc .errortext').text('');
        }
    });
    if ( age == "") {
        $('#age-bloc').addClass('error');
        $('#age-bloc .errortext').text('Veuillez saisir votre age');
        valid = false;
    }
    else if (!validAge(age)) {
        $('#age-bloc').addClass('error');
        $('#age-bloc .errortext').text('Veuillez saisir un chiffre');
        valid = false;
    }
    else {
        $('#age-bloc').removeClass('error');
        $('#age-bloc .errortext').text('');
    };

    if (!periode) {
        $('#periode-bloc').addClass('error');
        $('#periode-bloc .errortext').text('Veuillez saisir une période');
        valid = false;
    }
    else {
        $('#periode-bloc').removeClass('error');
        $('#periode-bloc .errortext').text('');
    };

    if (!civilite) {
        $('#civilite-bloc').addClass('error');
        $('#civilite-bloc .errortext').text('Veuillez saisir votre civilite');
        valid = false;
    }
    else {
        $('#civilite-bloc').removeClass('error');
        $('#civilite-bloc .errortext').text('');
    }
    if (nom == "") {
        $('#nom-bloc').addClass('error');
        $('#nom-bloc .errortext').text('Veuillez saisir un nom');
        valid = false;
    }
    else {
        $('#nom-bloc').removeClass('error');
        $('#nom-bloc .errortext').text('');
    }
    if (prenom == "") {
        $('#prenom-bloc').addClass('error');
        $('#prenom-bloc .errortext').text('Veuillez saisir un prénom');
        valid = false;
    }
    else {
        $('#prenom-bloc').removeClass('error');
        $('#prenom-bloc .errortext').text('');
    }

    if (email == "") {
        $('#email-bloc').addClass('error');
        $('#email-bloc .errortext').text('Veuillez saisir un email');
        valid = false;
    }
    else if (!validEmail(email)) {
        $('#email-bloc').addClass('error');
        $('#email-bloc .errortext').text('Veuillez saisir un email valide');
        valid = false;
    }
    else {
        $('#email-bloc').removeClass('error');
        $('#email-bloc .errortext').text('');
    }

    if ( telephone == "") {
        $('#telephone-bloc').addClass('error');
        $('#telephone-bloc .errortext').text('Veuillez saisir votre téléphone');
        valid = false;
    }
    else if (!validTelephone(telephone)) {
        $('#telephone-bloc').addClass('error');
        $('#telephone-bloc .errortext').text('Veuillez saisir un téléphone valide');
        valid = false;
    }
    else {
        $('#telephone-bloc').removeClass('error');
        $('#telephone-bloc .errortext').text('');
    };

    return valid;
});


/*-- ajout d'un secteur supplémentaire --*/

$(".r-btnAdd").click(function(){
    $('#other-proposition').show();
})

$(".r-btnRemove").click(function(){

    $('#other-proposition').hide();
    $('#other-proposition').find('.secteur').val('');
})



$(function()
{
    $('.secteur').each(function() {
        $(this).autocomplete({
            source: "https://www.maisonderetraite.net/ville",
            minLength: 2,
            focus: function(event, ui) {

                $(this).val(ui.item.value);
            },
            change: function (event, ui) {
                if (!ui.item) {
                    $(this).val('');
                }
            }
        });
    })
});



$(function()
{
    $('#lien_demande').autocomplete({
        source: "https://www.maisonderetraite.net/admin/demandelistlien",
        minLength: 2,
        focus: function(event, ui) {
            $(this).val(ui.item.value);
        },
    });
});


