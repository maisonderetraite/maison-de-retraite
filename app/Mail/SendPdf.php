<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPdf extends Mailable
{
    use Queueable, SerializesModels;

    public $demandeur;


    public function __construct($demandeur, $data,$pdf,$user)
    {
        $this->demandeur = $demandeur;
        $this->data = $data;
        $this->pdf = $pdf;
        $this->user = $user;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('lol@est.fre', 'Your Application')
            ->subject('Votre demande de création de résidence')
            ->view('mails.pdf')
            ->attachData($this->pdf, 'name.pdf');
    }
}
