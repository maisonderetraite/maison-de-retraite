<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commentaires extends Model
{
    protected $fillable = [
        'commentaire','demandeur_id'
    ];

    public function demandeurs()
    {
        return $this->belongsTo('App\Demandeur');
    }
}
