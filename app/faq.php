<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class faq extends Model
{


    public $timestamps = false;
    protected $fillable = [
        'question','reponse','categorie_id'
    ];


    public function categories()
    {
        return $this->belongsTo('App\Categorie');
    }

}
