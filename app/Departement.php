<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departement extends Model
{



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'nom',
        'user_id'
    ];

    public function regions()
    {
        return $this->belongsTo('App\Region');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
