<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EhpadCommentaires extends Model
{

    protected $fillable = [
        'commentaire','ehpad_ehpad_id','users_id','etat'
    ];

    public function ehpad()
    {
        return $this->belongsTo('App\ehpad');
    }

    public function users()
    {
        return $this->belongsTo('App\User');
    }
}
