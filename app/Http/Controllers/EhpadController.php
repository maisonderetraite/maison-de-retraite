<?php

namespace App\Http\Controllers;

use App\ehpad;
use App\EhpadCommentaires;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class EhpadController extends Controller
{
    private $auth;

    /**
     * AdminController constructor.
     * @param Guard $auth
     */

    public function __construct(Guard $auth){
        $this->auth = $auth;
        $this->middleware('auth');
    }


    public function index(Guard $auth) {

        $user = $auth->user();
        $departements_array = array();
        foreach ( $user->regions as $region ) {
            foreach ( $region->departements as $departement) {
                $departements_array[] = $departement->nombre;
            }
        }
        $collection = DB::Table('ehpads')->select('*');
        foreach($departements_array as $key => $departement) {
            $collection->orWhere('postal_code', 'like', $departement.'%');
        }
        if ( $user->roles == 'administrateur') {
            $ehpads = ehpad::orderBy('ehpad_id', 'desc')->paginate(50);
        } else {
            $ehpads = $collection->orderBy('ehpad_id', 'desc')->paginate(50);
        }

        return view('admin.ehpad.index', compact('user','ehpads'));
    }

    public function search(Guard $auth, Request $request)
    {
        $user = $auth->user();
        $ehpad = $request->input('ehpad');
        $ehpad_name = $request->input('ehpad_name');
        $results = ehpad::Where(function ($query)  use ($ehpad_name,$ehpad) {
                if ( $ehpad_name ) {
                    $query->where('name', 'LIKE',  '%'.$ehpad_name.'%');
                }
                if ( $ehpad ) {
                    $query->where('postal_code', 'LIKE',$ehpad.'%' );
                    $query->orwhere('city', 'LIKE', '%'.$ehpad.'%' );

                }
            })
            ->paginate(50);
        return view('admin.ehpad.search', compact('user','results'));
    }


    public function create (Guard $auth) {
        $user = $auth->user();
        $ehpad = New ehpad();
        return view('admin.ehpad.create', compact('user','ehpad'));
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {

        $data = $request->all();
        $data['id'] = bcrypt(str_random(45));
        $ehpad = ehpad::create($data);
        return redirect('/admin/ehpad')->with('success', 'La fiche de l\'établissement a été mis a jour');
    }



    /**
     * @param Guard $auth
     * @param $key
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function show(Guard $auth, $key)
    {
        $user = $auth->user();
        $i = 0;
        $ehpad = ehpad::findOrFail($key);
        return view('admin.ehpad.show', compact('user','ehpad','i'));
    }

    /**
     * @param Guard $auth
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */

    public function updatecomment (Guard $auth,Request $request, $id) {


        $user = $auth->user();
        $ehpad = ehpad::findOrFail($id);
        $etat = Input::get('etat');
        $commentaires = Input::get('commentaire');
        EhpadCommentaires::create(['commentaire' =>  $commentaires , 'etat' => $etat , 'ehpad_ehpad_id' => $ehpad->ehpad_id , 'users_id' => $user->id] );
        return redirect()->back()->with('success', 'Le commentaire a bien été enregistré');
    }

    public function deletecomment (Guard $auth, $commentaire) {
        $commentaire_ehpad = EhpadCommentaires::findOrFail($commentaire);
        $commentaire_ehpad->delete();
        return redirect()->back()->with('success', 'Le commentaire a bien été supprimé');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request ,$id) {

        $data = $request->all();
        $ehpad = ehpad::findOrFail($id);
        $ehpad->update($data);
        return redirect()->back()->with('success', 'La fiche de l\'établissement a été mis a jour');
    }

}
