<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class ContactController extends Controller
{
    public function store(Request $request)
    {
        $data = [
            'nom' => $request->get('nom'),
            'prenom' => $request->get('prenom'),
            'mail' => $request->get('mail'),
            'telephone' => $request->get('telephone'),
            'objet' => $request->get('objet'),
            'texte' => $request->get('texte')
        ];

        Mail::send('mails.contact',$data , function ($message) use ($data) {
            $message->from( $data['mail'] , 'Maisonderetraite.net')
                ->to('contact@maisonderetraite.net', $data['nom'] )
                ->subject('Demande de contact par le site maison de retraite');

        });
        return redirect('/')->with('success', 'Votre message a bien été envoyé. Un conseiller vous contactera dans les plus brefs délais');

    }
}
