<?php

namespace App\Http\Controllers;

use App\Demandeur;
use App\ehpad;
use App\Secteur;
use App\User;
use App\Ville;
use Barryvdh\DomPDF\PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Maatwebsite\Excel\Excel;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function index()
    {
        $demandeurs = new Demandeur();

        return view('welcome', compact('demandeurs'));
    }

    /**
     * @param Request $request
     * @param Guard $auth
     * @return \Illuminate\Http\RedirectResponse
     */

    public function store(Request $request)
    {
        $data = $request->all();
        $antirobot = $data['remarque'];

        if ($antirobot != '') {

            die();
        } else {
            $demandeurs = Demandeur::create([
                    'lien_personne' => $data['lien_personne'],
                    'civilite' => $data['civilite'],
                    'name_firstname' => $data['name_firstname'],
                    'nom' => $data['nom'],
                    'statut' => 'a traiter',
                    'prenom' => $data['prenom'],
                    'email' => $data['email'],
                    'telephone' => $data['telephone'],
                    'periode_souhaite' => $data['periode_souhaite']

                ]
            );
            $insertedId = $demandeurs->id;
            $secteurs = Input::get('secteur');
            foreach ($secteurs as $key => $n) {
                if ($n != '') {
                    Secteur::create(['name' => $n, 'demandeur_id' => $insertedId]);
                }
            }
            return redirect('/confirmation');
        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function confirmation()
    {
        return view('confirmation');
    }


    public function testpdf()
    {

        return view('pdf');
        /*$pdf = App::make('dompdf.wrapper');
        $pdf->loadView('pdf');
        return $pdf->stream();*/
    }

    public function pdf()
    {
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('pdf');
        return $pdf->stream();
    }


    public function ville()
    {

        $term = Input::get('term');

        $results = array();

        $queries = DB::table('villes')
            ->where('CodePostal', 'LIKE', $term . '%')
            ->orWhere('NomVille', 'LIKE', $term . '%')
            ->take(20)->get();

        foreach ($queries as $query) {
            //$results[] = [ 'id' => $query->IdVille, 'value' => $query->IdDepartement.substr($query->CodePostal, - 3).' '.$query->NomVille ];
            $results[] = ['id' => $query->IdVille, 'value' => $query->CodePostal . ' ' . $query->NomVille];
        }
        return Response::json($results);
    }


    public function excel()
    {

        $users = ehpad::select('name','postal_code','email')

            ->where('postal_code','like','04%')
            ->Orwhere('postal_code','like','05%')
            ->Orwhere('postal_code','like','06%')
            ->Orwhere('postal_code','like','13%')
            ->Orwhere('postal_code','like','83%')
            ->Orwhere('postal_code','like','84%')
            ->get();
        \Maatwebsite\Excel\Facades\Excel::create('users', function($excel) use($users) {
            $excel->sheet('Sheet 1', function($sheet) use($users) {
                $sheet->fromArray($users);
            });
        })->export('csv');

    }

    public function zeroplace()
    {
        return view('email-place-zero');
    }
    public function oneplace()
    {
        return view('email-place');
    }
    public function twoplaces()
    {
        return view('email-place');
    }

}
