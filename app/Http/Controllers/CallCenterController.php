<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CallCenterController extends Controller
{
    public function contact_call_aswat(Request $request) {




       $telephone = $request->input('contact_call_aswat');

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://maison-de-retraite.aswtel.com/webservices/callback.php",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"key\"\r\n\r\nLQYYILXIweD7CunGV1KDwBoeRXWVGE1f\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"queue\"\r\n\r\nrappel\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"dst\"\r\n\r\n".$telephone."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                "key: LQYYILXIweD7CunGV1KDwBoeRXWVGE1f",
                "postman-token: c2630450-40d8-33d3-223f-94f5c7838e0a"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }

        return redirect()->back()->with('success', 'Vous allez être contacté immédiatement');
    }
}
