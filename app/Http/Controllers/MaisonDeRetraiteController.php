<?php

namespace App\Http\Controllers;

use App\Demandeur;
use App\MaisonDeRetraite;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class MaisonDeRetraiteController extends Controller
{

    private $auth;

    /**
     * AdminController constructor.
     * @param Guard $auth
     */

    public function __construct(Guard $auth){
        $this->auth = $auth;
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */


    public function store(Request $request, $id) {

        $demandeur = Demandeur::findOrFail($id);
        $data = $request->all();
        MaisonDeRetraite::create( [
            'nom' => $data['nom'],
            'adresse' => $data['adresse'],
            'cp' => $data['cp'],
            'ville' => $data['ville'],
            'email' => $data['email'],
            'demandeur_id' => $demandeur->id
        ]
        );
        return redirect()->back()->with('success', 'La maison de retraite a bien été ajouté');
    }

    /**
     * @param $faq
     * @return \Illuminate\Http\RedirectResponse
     */

    public function delete ($id, $mdr) {
        $maisonderetraite = MaisonDeRetraite::findOrFail($mdr);
        $maisonderetraite->delete($mdr);
        return redirect()->back()->with('success', 'La maison de retraite a bien été supprimé');
    }

    public function choix($id, $mdr) {
        $demandeur = Demandeur::FindOrFail($id);
        $maisonderetraite = $demandeur->maisonderetraite;
        foreach ( $maisonderetraite as $maisonderetrait ) {
            $maisonderetrait->update([
                'choix' => '0'
            ]);
        }
        $maisonderetraite->find($mdr)->update([
            'choix' => '1'
        ]);
        return redirect()->back()->with('success', 'Votre choix final a été effectué');

    }


}
