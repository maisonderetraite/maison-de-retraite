<?php

namespace App\Http\Controllers\Admin;

use App\Region;
use App\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Symfony\Component\Console\Input\Input;

class UserController extends Controller
{

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    private $auth;

    /**
     * AdminController constructor.
     * @param Guard $auth
     */

    public function __construct(Guard $auth){
        $this->auth = $auth;
        $this->middleware('auth');
        $this->middleware('administrator');
    }

    /**
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function utilisateurslist(Guard $auth) {
        $users = User::all();
        return view('admin.users.list', compact('users'));
    }

    /**
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function usercreate(Guard $auth) {
        $user = new User();
        return view('admin.users.create', compact('user'));
    }

    public function editrole(Guard $auth, Request $request ,$id) {

        $data = $request->all();
        $users = User::all();
        $user = User::find($id);
        $administrateurs = $users->where('roles', '=', 'administrateur')->count();

        if($administrateurs > 1) {
            $user->update($data);
            return redirect()->back()->with('success', 'Le membre a été mis à jour');
        } else {
            return redirect()->back()->with('error', 'Il doit y avoir au moins un administrateur pour cet espace');
        }
    }


    public function delete(Guard $auth, $id) {
        if ($auth->user() == User::findOrFail($id)) {
            return redirect()->back()->with('error', 'Vous ne pouvez vous supprimer en tant qu\'administrateur');
        }
        else {
            $user = User::findOrFail($id);
            $user->delete();
            return redirect()->back()->with('success', 'Vous avez bien supprimé l\'utilisateur');
        }
    }

    public function edit($id){
        $user = User::find($id);
        $allregions = Region::all();
        $departements = $user->regions;
        return view('admin.users.edit', compact('user','allregions','departements'));
    }


    public function update( Request $request , $id ){
        $user = User::findOrFail($id);
        $data = $request->all();
        $user->update($data);
        return redirect()->back()->with('success', 'L\'utilisateur a bien été mis à jour');
    }

    public function createregions (Request $request, $id) {
        $user = User::findOrFail($id);
        $region = $request->get('region');
        $region_user =  $user->regions()->where('region_id', $region)->count();
        if ( $region_user == 0  ) {
            $user->regions()->attach($region);
            return redirect()->back()->with('success', 'La région a été associé à cet utilisateur');
        } else {
            return redirect()->back()->with('error', 'La région est dèjà associé à cet utilisateur');
        }

    }

    public function deleteregions(Guard $auth, $id, $region) {
        $user = User::findOrFail($id);
        $region_user = Region::findOrFail($region);
        $user->regions()->detach($region_user);
        return redirect()->back()->with('success', 'Vous avez bien supprimé l\'utilisateur');
    }

}
