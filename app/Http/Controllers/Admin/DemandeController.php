<?php

namespace App\Http\Controllers\Admin;

use App\Demandeur;
use App\Secteur;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class DemandeController extends Controller
{



    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    private $auth;

    /**
     * AdminController constructor.
     * @param Guard $auth
     */

    public function __construct(Guard $auth){
        $this->auth = $auth;
        $this->middleware('auth');


    }

    public function create(Guard $auth) {
        $user = $auth->user();
        $demandeurs = new Demandeur();
        return view('admin.demandeurs.create', compact('demandeurs','user'));
    }

    /**
     * @param Request $request
     * @param Guard $auth
     * @return \Illuminate\Http\RedirectResponse
     */

    public function store (Request $request) {
        $data = $request->all();
        $demandeurs = Demandeur::create( [
                'lien_personne' => $data['lien_personne'],
                'civilite' => $data['civilite'],
                'name_firstname' => $data['name_firstname'],
                'nom' => $data['nom'],
                'statut' => 'a traiter',
                'prenom' => $data['prenom'],
                'email' => $data['email'],
                'telephone' => $data['telephone'],
                'periode_souhaite' => $data['periode_souhaite']

            ]
        );
        $insertedId = $demandeurs->id;
        $secteurs = Input::get('secteur');
        foreach($secteurs as $key => $n ) {
            if ($n != '') {
                Secteur::create(['name' =>  $n ,'demandeur_id' => $insertedId ] );
            }
        }
        return redirect()->route('ficheview', [ $insertedId , 0]);
    }

}
