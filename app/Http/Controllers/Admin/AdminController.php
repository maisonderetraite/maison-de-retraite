<?php

namespace App\Http\Controllers\Admin;

use App\Commentaires;
use App\Demandeur;
use App\Mail\SendPdf;
use App\MaisonDeRetraite;
use App\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Barryvdh\DomPDF\PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;

class AdminController extends Controller
{


    private $auth;

    /**
     * AdminController constructor.
     * @param Guard $auth
     */

    public function __construct(Guard $auth){
        $this->auth = $auth;
        $this->middleware('auth');
    }

    /**
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function index(Guard $auth)
    {
        /*
        $users = User::all();
        $demandeurscount = Demandeur::all();
        $user = $auth->user();
        $departements_array = array();
        foreach ( $user->regions as $region ) {
            foreach ( $region->departements as $departement) {
                $departements_array[] = $departement->nombre;
            }
        }

            $collection = DB::table('demandeurs')->select('*')
            ->join('secteurs', function ($join) use ($departements_array) {
                foreach( $departements_array as $key => $departement) {
                    $join->Orwhere('secteurs.name', 'like',  $departement.'%');
                }
            })->select('*','secteurs.*');
            if ($user->roles != 'administrateur') {
                $demandeurs = Demandeur::with('secteurs','users')
                    ->where('statut','=','a traiter')->orderBy('id', 'desc')->get();
            } else {
                $demandeurs = $collection->get();

            }
        return view('admin.index', compact('demandeurs','user','users','demandeurscount'));
        */

        $users = User::all();
        $demandeurscount = Demandeur::all();
        $user = $auth->user();
        $departements_array = array();
        foreach ( $user->regions as $region ) {
            foreach ( $region->departements as $departement) {
                $departements_array[] = $departement->nombre;
            }
        }

        if ($user->roles != 'administrateur') {

            $demandeurs = Demandeur::whereHas('secteurs', function ($query) use ($departements_array) {
                $query->where('id');
                foreach( $departements_array as $key => $departement) {
                    $query->Orwhere('name','like' ,$departement.'%');
                }
            })->where('statut','=','a traiter')->orderBy('id', 'desc')->get();

        } else {

            $demandeurs = Demandeur::with('secteurs','users')->where('statut','=','a traiter')->orderBy('id', 'desc')->get();

        }
        return view('admin.index', compact('demandeurs','user','users','demandeurscount','$departements_array '));




    }


    public function priseencharge(Guard $auth, $id) {
        $user = $auth->user();
        $demandeur = Demandeur::findOrFail($id);
        $demandeur->update([
            'users_id' => $user->id,
            'statut' => 'en cours'
        ]);

        return redirect()->back()->with('success', 'Cette demande est maintenant à votre charge');

    }

    /**
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function encours(Guard $auth) {

        $user = $auth->user();
        $departements_array = array();
        foreach ( $user->regions as $region ) {
            foreach ( $region->departements as $departement) {
                $departements_array[] = $departement->nombre;
            }
        }
        if ($user->roles == 'commercial') {

            $demandeurs = Demandeur::whereHas('secteurs', function ($query) use ($departements_array) {
                $query->where('id');
                foreach( $departements_array as $key => $departement) {
                    $query->Orwhere('name','like' ,$departement.'%');
                }
            })->where('statut','=','En cours')->orderBy('id', 'desc')->get();

        } else {
            $demandeurs = Demandeur::with('secteurs')->where(
                [
                    ['statut', '=', 'En cours'],
                ]
            )->orderBy('id', 'desc')->get();
        }
        return view('admin.encours', compact('demandeurs','user'));
    }

    /**
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function archive(Guard $auth)
    {
        $user = $auth->user();
        $departements_array = array();
        foreach ( $user->regions as $region ) {
            foreach ( $region->departements as $departement) {
                $departements_array[] = $departement->nombre;
            }
        }
        if ($user->roles == 'commercial') {

            $demandeurs = Demandeur::whereHas('secteurs', function ($query) use ($departements_array) {
                $query->where('id');
                foreach( $departements_array as $key => $departement) {
                    $query->Orwhere('name','like' ,$departement.'%');
                }
            })->where('statut','=','Archivé')->orderBy('id', 'desc')->get();

        } else {
            $demandeurs = Demandeur::with('secteurs')->where(
                [
                    ['statut', '=', 'Archivé'],
                ]
            )->orderBy('id', 'desc')->get();
        }
        return view('admin.encours', compact('demandeurs','user'));
    }

    /**
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function arelancer(Guard $auth)
    {
        $user = $auth->user();
        if ($user->roles == 'commercial') {
            $demandeurs = Demandeur::with('secteurs')
                ->where(
                    [
                        ['statut', '=', 'À relancer'],
                        ['users_id', '=', $user->id],
                    ]
                )
                ->orderBy('id', 'desc')->get();
        } else {
            $demandeurs = Demandeur::with('secteurs')->where(
                [
                    ['statut', '=', 'À relancer'],
                ]
            )->orderBy('id', 'desc')->get();
        }

        return view('admin.encours', compact('demandeurs','user'));
    }

    /**
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function dossierenvoyeauclient(Guard $auth)
    {
        $user = $auth->user();
        if ($user->roles == 'commercial') {
            $demandeurs = Demandeur::with('secteurs')
                ->where(
                    [
                        ['statut', '=', 'Dossier envoyé au client'],
                        ['users_id', '=', $user->id],
                    ]
                )
                ->orderBy('id', 'desc')->get();
        } else {
            $demandeurs = Demandeur::with('secteurs')->where(
                [
                    ['statut', '=', 'Dossier envoyé au client'],
                ]
            )->orderBy('id', 'desc')->get();
        }

        return view('admin.encours', compact('demandeurs','user'));
    }

    /**
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function dossiertransmisenehpad(Guard $auth)
    {
        $user = $auth->user();
        $departements_array = array();
        foreach ( $user->regions as $region ) {
            foreach ( $region->departements as $departement) {
                $departements_array[] = $departement->nombre;
            }
        }
        if ($user->roles == 'commercial') {



            $demandeurs = Demandeur::whereHas('secteurs', function ($query) use ($departements_array) {
                $query->where('id');
                foreach( $departements_array as $key => $departement) {
                    $query->Orwhere('name','like' ,$departement.'%');
                }
            })->where('statut','=','Dossier transmis en Ehpad')->orderBy('id', 'desc')->get();

        } else {
            $demandeurs = Demandeur::with('secteurs')->where(
                [
                    ['statut', '=', 'Dossier transmis en Ehpad'],
                ]
            )->orderBy('id', 'desc')->get();
        }
        return view('admin.encours', compact('demandeurs','user'));
    }
    public function updatecommentaire ($id) {

        $demandeur = Demandeur::findOrFail($id);
        $insertedId = $demandeur->id;
        $commentaires = Input::get('commentaire');
        Commentaires::create(['commentaire' =>  $commentaires ,'demandeur_id' => $insertedId ] );
        return redirect()->back()->with('success', 'Le commentaire a bien été enregistré');
    }

    /**
     * @param Guard $auth
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */

    public function deleteAsker(Guard $auth, $id)
    {
        $demandeur = Demandeur::find($id);
        $demandeur->delete();
        return redirect()->back()->with('success', 'Cette demande a bien été supprimé');
    }

    /**
     * @param Guard $auth
     * @param $id
     * @param $onglet
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */

    public function show(Guard $auth, $id, $onglet) {
        $user = $auth->user();
        $users = User::where('roles','=','commercial')->pluck('email','id');
        $demandeur = Demandeur::with('maisonderetraite')->findOrFail($id);
        $maisonderetraite = New MaisonDeRetraite();
        $next = $onglet + 1;

        return view('admin.page.'.$onglet, compact('demandeur','next','onglet','user','users','maisonderetraite'));
    }

    /**
     * @param Guard $auth
     * @param Request $request
     * @param $id
     * @param $onglet
     * @return \Illuminate\Http\RedirectResponse
     */

    public function update(Request $request ,$id,$onglet) {

        $data = $request->all();
        $demandeur = Demandeur::findOrFail($id);
        $demandeur->update($data);
        return redirect()->route('ficheview', [  $demandeur->id , $onglet+1]);
    }

    /**
     * @param Guard $auth
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */

    public function updatestatut(Guard $auth, Request $request ,$id) {

        $data = $request->all();
        $user = $auth->user();
        $demandeur = Demandeur::findOrFail($id);
        $demandeur->update([
            'statut' => $data['statut'],
            'users_id' => $user->id
        ]);
        return redirect()->back()->with('success', 'Le statut de cette demande a bien été mis a jour');
    }

    public function updateuser(Guard $auth, Request $request ,$id) {

        $data = $request->all();
        $demandeur = Demandeur::findOrFail($id);
        $demandeur->update([
            'users_id' => $data['users_id']
        ]);
        return redirect()->back()->with('success', 'Le statut de cette demande a bien été mis a jour');
    }


    /**
     * @param Guard $auth
     * @param $id
     * @return mixed
     */

    public function pdf(Guard $auth, $id) {
        $demandeur = Demandeur::findOrFail($id);
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('pdf',compact('demandeur') );
        return $pdf->stream();
    }

    /**
     * @param Guard $auth
     * @param $id
     * @return mixed
     */

    public function download($id) {
        $demandeur = Demandeur::findOrFail($id);
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('pdf',compact('demandeur') );
        return $pdf->download('CERFA.pdf');
    }



    public function sendemail(Request $request, $id, Guard $auth) {
        $user = $auth->user();
        $data = [
            'email' => $request->get('email')
        ];
        $demandeur = Demandeur::findOrFail($id);
        $test = App::make('dompdf.wrapper');
        $pdf = $test->loadView('pdf',compact('demandeur') );
        Mail::send('mails.pdf', ['data' => $data, 'demandeur' => $demandeur, 'user' => $user], function($message) use($pdf,$data)
        {
            $message->from('contact@mamaisonderetraite.org', 'Maison de retraite');
            $message->to($data['email'])->subject('Votre dossier d\'inscription');
            $message->attachData($pdf->output(), "Cerfa.pdf");
            $message->attach(base_path() . '/public/documents/dossiermedical.pdf');
            ;
        });

        return redirect()->back()->with('success', 'L\'email a bien été envoyé');
    }



    public function demandelist() {

        $term = Input::get('term');
        $results = array();
        $queries = DB::table('demandeurs')
            ->where('name_firstname', 'LIKE', '%'.$term.'%')
            ->take(20)->get();
        foreach ($queries as $query)
        {
            $results[] = [ 'id' => $query->id, 'value' => $query->name_firstname];
        }
        return Response::json($results);
    }
}
