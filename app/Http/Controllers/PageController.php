<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{


    public function quisommesnous()
    {
        return view('quisommesnous');
    }

    public function mission()
    {
        return view('mission');
    }

    public function contact()
    {
        return view('contact');
    }

    public function ml()
    {
        return view('ml');
    }

    public function pdc()
    {
        return view('pdc');
    }

    public function cgu()
    {
        return view('cgu');
    }
}
