<?php

namespace App\Http\Controllers;

use App\Categorie;
use App\faq;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class FaqController extends Controller
{

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    private $auth;

    /**
     * AdminController constructor.
     * @param Guard $auth
     */

    public function __construct(Guard $auth){
        $this->auth = $auth;
        $this->middleware('auth');
    }

    /**
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function liste(Guard $auth) {
        $categories = Categorie::all();
        $user = $auth->user();

        $faqs = faq::where('categorie_id' ,'=', 0)->get();

        return view('admin.faq.index', compact('user','categories','faqs'));
    }

    /**
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function suggestion(Guard $auth) {
        $user = $auth->user();
        $faq = new faq();
        return view('admin.faq.suggestion', compact('user','faq'));
    }


    /**
     * @param Guard $auth
     * @param $key
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function show(Guard $auth, $id)
    {
        $user = $auth->user();
        $faq = faq::findOrFail($id);
        $categories = Categorie::all();
        return view('admin.faq.show', compact('user','faq','categories'));
    }

    /**
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function create(Guard $auth) {
        $user = $auth->user();
        $faq = new faq();
        $categories = Categorie::all();
        return view('admin.faq.create', compact('faq','user','categories'));
    }

    /**
     * @param Guard $auth
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function search(Guard $auth, Request $request)
    {
        $user =$auth->user();
        $faq = $request->input('faq');
        $results = faq::where('question', 'LIKE', '%'.$faq.'%' )
            ->Orwhere('reponse', 'LIKE', '%'.$faq.'%' )
            ->get();
        return view('admin.faq.search', compact('user','results'));
    }

    public function store ( Guard $auth, Request $request) {
        $data = $request->all();
        if ( $data['reponse'] = '') {
            $data['categorie_id'] = null;
        }
        if ( $data['categorie_id'] = '') {
            $data['categorie_id'] = null;
        }


        faq::create([
            'question' => $data['question'],
            'reponse' => $data['reponse'],
            'categorie_id' => $data['categorie_id'],
            ]
        );
        return redirect()->back()->with('success', 'Faq a bien été enregistré');
    }

    public function update ( Guard $auth, Request $request, $id) {
        $data = $request->all();
        $faq = faq::findOrFail($id);
        $faq->update($data);
        return redirect()->back()->with('success', 'La question a été mis à jour');
    }

    public function delete ($faq) {
        $faqdelete = faq::findOrFail($faq);
        $faqdelete->delete($faq);
        return redirect()->back()->with('success', 'Faq a bien été supprimé');
    }




}
