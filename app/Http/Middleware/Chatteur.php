<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class Chatteur
{
    /**
     * Administrator constructor.
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $this->auth->user();
        if($user->roles != 'commercial') {
            return $next($request);
        } else {
            return redirect('/admin')->with('error', 'Vous ne pouvez pas accéder à cet espace');
        }
    }
}
