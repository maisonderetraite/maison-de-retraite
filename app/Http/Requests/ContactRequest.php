<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "nom" => "required",
            "prenom" => "required",
            "email" => "required",
            "objet" => "required",
            "texte" => "required",
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'nom.required' => 'Un nom est requis',
            'prenom.required' => 'Une prénom est requis',
            'email.required' => 'Un email est requis',
            'objet.required' => 'Une objet est requis',
            'texte.required' => 'Un message est requis',
        ];
    }
}
