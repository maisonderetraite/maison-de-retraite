<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestFormulaire extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        $rules = [
            "age" => "required|min:2",
            "civilite" => "required",
            "nom" => "required",
            "prenom" => "required",
            "lien_personne" => "required",
            "email" => "required",
            "telephone" => "required",
            "periode_souhaite" => "required",
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'age.required' => 'Un titre est requis',
            'content.required' => 'Une description est requis',
            'title.min' => 'Le titre est trop court',
            'content.min' => 'La description est trop courte',
            'time.required' => 'une date est requis',
            'place.required' => 'un lieu est requis',
        ];
    }
}
