<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Secteur extends Model
{
    protected $fillable = [
        'name','cp','demandeur_id'
    ];

    public function demandeurs()
    {
        return $this->belongsTo('App\Demandeur');
    }

}
