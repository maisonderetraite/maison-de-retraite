<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','roles'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function demandeurs()
    {
        return $this->hasMany('App\Demandeur');
    }

    public function commentaires()
    {
        return $this->hasMany('App\EhpadCommentaires');
    }

    public function regions()
    {
        return $this->belongsToMany('App\Region');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */

    public function departements()
    {
        return $this->belongsToMany('App\Departement');
    }

}
