<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Demandeur extends Model
{
    protected $fillable = [
        'statut','nom', 'prenom', 'lien_personne','telephone','email','periode_souhaite','civilite','commentaire_agent','commentaire_annexe','name_firstname','patologies','dependances','link_other_fiche',
        'users_id',
        'demande_type_hebergement',
        'demande_accompagnement',
        'demande_duree_sejour_temporaire',
        'demande_situation',
        'demande_autres',
        'demande_nom_etablissement_service',
        'demande_personne_informe',
        'demande_personne_consentante',
        'demande_consentement_recueilli',

        'etat_civil_civilite',
        'etat_civil_nom_de_naissance',
        'etat_civil_prenoms',
        'etat_civil_date_de_naissance',
        'etat_civil_lieu_de_naissance',
        'etat_civil_pays_ou_departement',
        'etat_civil_immatriculation',
        'etat_civil_adresse_rue',
        'etat_civil_adresse_cp',
        'etat_civil_adresse_commune_ville',
        'etat_civil_telephone_fixe',
        'etat_civil_telephone_portable',
        'etat_civil_email',
        'etat_civil_situation_familiale',
        'etat_civil_nb_enfants',
        'etat_civil_protection_juridique',
        'etat_civil_protection_juridique_laquelle',
        'etat_civil_protection_contexte',

        'etat_civil_representant_civilite',
        'etat_civil_representant_nom_de_naissance',
        'etat_civil_representant_prenoms',
        'etat_civil_representant_date_de_naissance',
        'etat_civil_representant_lieu_de_naissance',
        'etat_civil_representant_pays_ou_departement',
        'etat_civil_representant_adresse_rue',
        'etat_civil_representant_adresse_cp',
        'etat_civil_representant_adresse_commune_ville',
        'etat_civil_representant_telephone_fixe',
        'etat_civil_representant_telephone_portable',
        'etat_civil_representant_email',

        'personne_confiance_designation',
        'personne_confiance_nom_de_naissance',
        'personne_confiance_prenoms',
        'personne_confiance_adresse_rue',
        'personne_confiance_adresse_cp',
        'personne_confiance_adresse_commune_ville',
        'personne_confiance_telephone_fixe',
        'personne_confiance_telephone_portable',
        'personne_confiance_email',
        'personne_confiance_lien_parente',

        'personne_contact_personne_concernee',
        'personne_contact_nom_de_naissance',
        'personne_contact_prenoms',
        'personne_contact_adresse_rue',
        'personne_contact_adresse_cp',
        'personne_contact_adresse_commune_ville',
        'personne_contact_telephone_fixe',
        'personne_contact_telephone_portable',
        'personne_contact_email',
        'personne_contact_lien_parente',

        'personne_contact2_nom_de_naissance',
        'personne_contact2_prenoms',
        'personne_contact2_adresse_rue',
        'personne_contact2_adresse_cp',
        'personne_contact2_adresse_commune_ville',
        'personne_contact2_telephone_fixe',
        'personne_contact2_telephone_portable',
        'personne_contact2_email',
        'personne_contact2_lien_parente',

        'aspects_financiers_comment_frais',
        'aspects_financiers_aide_social',
        'aspects_financiers_allocation_logement',
        'aspects_financiers_allocation_personnalise',
        'aspects_financiers_prestation_compensation',
        'aspects_financiers_commentaire',
        'estimation_budget_commentaire',

        'date_entree_souhaitee',
        'date_entree_souhaitee_hergemenent_temporaire',
        'date_entree_date_demande',

        'maison_de_retraite_nom',
        'maison_de_retraite_adresse',
        'maison_de_retraite_cp',
        'maison_de_retraite_ville',
        'maison_de_retraite_email',

    ];


    public function secteurs()
    {
        return $this->hasMany('App\Secteur');
    }

    public function commentaires()
    {
        return $this->hasMany('App\Commentaires');
    }

    public function getUserIdAttribute($id){
        if ($id == false) {
            return "Non attribué";
        }
    }

    public function users()
    {
        return $this->belongsTo('App\User', 'users_id');
    }


    public function maisonderetraite()
    {
        return $this->hasMany('App\MaisonDeRetraite');
    }



}
