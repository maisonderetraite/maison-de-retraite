<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ehpad extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'ehpad_id';



    protected $fillable = [
        'id',
        'address',
        'capacity',
        'city',
        'email',
        'housing_type',
        'latitude',
        'longitude',
        'miscellaenous',
        'name',
        'other_pricing',
        'phone',
        'postal_code',
        'pricing',
        'public_grant',
        'specific_accompaniment',
        'website',
        'type_hebergement',
        'qualification_du_decisionnaire',
        'civilite',
        'nom',
        'prenom',
        'poste',
        'on_off',
        'disponibilites'
    ];

    public function commentaires()
    {
        return $this->hasMany('App\EhpadCommentaires');
    }
}
