<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaisonDeRetraite extends Model
{
    protected $fillable = [
        'nom',
        'adresse',
        'cp',
        'ville',
        'email',
        'demandeur_id',
        'choix',
    ];


    public function demandeurs()
    {
        return $this->belongsTo('App\Demandeur');
    }
}
