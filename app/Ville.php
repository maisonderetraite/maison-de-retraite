<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ville extends Model
{
    public function getCodePostalAttribute($value)
    {
        $test = str_pad($value, 5, '0', STR_PAD_LEFT);
        return $test;
    }
}
